#Drinki Blog

The Drinki blog is a wordpress instance hosted in a GAE flex environment.

It has very little maintenance except adding new plugins. To do this just add the
desired plugin folder into wordpress/wp-content/plugins and push a new version
using `gcloud app deploy` to the `drinkiapplication` project.