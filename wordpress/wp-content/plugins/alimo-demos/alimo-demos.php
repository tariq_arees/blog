<?php
/*
Plugin Name: Alimo Demos
Plugin URI:
Description: This plugin contains all the available demo data for one click import.
Version: 1.0
Author: LayerBlend
Author URI: http://www.layerblend.com
*/


/* Extensions */
if(!function_exists('alimo_register_custom_extension_loader')) :
	function alimo_register_custom_extension_loader($ReduxFramework) {
		$path = dirname( __FILE__ ) . '/extensions/';
		$folders = scandir( $path, 1 );
		foreach($folders as $folder) {
			if ($folder === '.' or $folder === '..' or !is_dir($path . $folder) ) {
				continue;
			}
			$extension_class = 'ReduxFramework_Extension_' . $folder;
			if( !class_exists( $extension_class ) ) {
				$class_file = $path . $folder . '/extension_' . $folder . '.php';
				$class_file = apply_filters( 'redux/extension/'.$ReduxFramework->args['opt_name'].'/'.$folder, $class_file );
				if( $class_file ) {
					require_once( $class_file );
					$extension = new $extension_class( $ReduxFramework );
				}
			}
		}
	}
	add_action("redux/extensions/alimo_data/before", 'alimo_register_custom_extension_loader', 0);
endif;

if ( !function_exists( 'wbc_filter_title' ) ) {
	/**
	 * Filter for changing demo title in options panel so it's not folder name.
	 *
	 * @param [string] $title name of demo data folder
	 *
	 * @return [string] return title for demo name.
	 */
	function wbc_filter_title( $title ) {
		$demo_title = '';
		switch ($title){
			case 'alimo-deco':
				$demo_title = 'Fashion & Home Deco';
				break;
			case 'alimo-standard':
				$demo_title = 'Standard';
				break;
			default:
				$demo_title = 'Alimo';
		}
		return $demo_title;
	}
	// Uncomment the below
	add_filter( 'wbc_importer_directory_title', 'wbc_filter_title', 10 );
}
