<?php
/*
Plugin Name: Alimo Shortcodes
Plugin URI:
Description: This plugin contains all shortcodes for Alimo.
Version: 1.0
Author: LayerBlend
Author URI: http://www.layerblend.com
*/

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }


class Alimo_Shortcodes {

    protected static $instance = null;

    private function __construct() {

        require_once( 'shortcodes/Alimo_Shortcode_Social_Profile.php' );
		
    }

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function get_instance() {

        // If the single instance hasn't been set, set it now.
        if ( null == self::$instance ) {
            self::$instance = new self;
        }

        return self::$instance;
    }


}
Alimo_Shortcodes::get_instance();