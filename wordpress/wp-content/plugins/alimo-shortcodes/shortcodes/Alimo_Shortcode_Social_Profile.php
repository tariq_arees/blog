<?php

if ( ! defined( 'ABSPATH' ) ) { exit; }


class Alimo_Shortcode_Social_Profile {
    protected static $instance = null;

    public static function get_instance() {

        // If the single instance hasn't been set, set it now.
        if ( null == self::$instance ) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __construct() {
        add_shortcode( 'alimo_social_profile', array( &$this, 'shortcode' ) );
    }

    public function shortcode( $atts, $content = null ) {
        $output = $network = $text = $profile_url = $target = $tooltip = $color = $style = '';

        $alimo_data = get_option('alimo_data');

        extract( shortcode_atts( array(
            'network'         => 'facebook',
            'text'          => '',
            'href'         => '#',
            'target'       => '_blank',
            'tooltip'       => '',
	        'color' => ''
        ), $atts ) );

        $show_tooltip = '';
        switch ($tooltip){
            case 'top':
            case 'bottom':
            case 'left':
            case 'right':
                $show_tooltip = 'data-toggle="tooltip" data-placement="' . esc_attr($tooltip) . '" title="' . esc_attr($network) . '"';
                break;
            default:
                break;
        }
        if(!empty($color)){
        	$is_color = preg_match('/#([a-f0-9]{3}){1,2}\b/i',$color);
        	if($is_color){
		        $style = " style='color:".esc_attr($color)."' ";
	        }
        }

        $profile_url = $alimo_data[$network.'-url'];
        if($profile_url){
            $output .= '<a class="alimo_social_profile '.esc_attr($network).'" href="'.esc_attr($profile_url).'" target="'.esc_attr($target).'" '.$show_tooltip.''.$style.'><i class="fa fa-'.esc_attr($network).'"></i>'.esc_html($text).'</a>';
        }

        return $output;
    }



}
Alimo_Shortcode_Social_Profile::get_instance();