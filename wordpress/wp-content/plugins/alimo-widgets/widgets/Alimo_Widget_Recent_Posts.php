<?php
/**
 * Alimo Recent Posts Widget
 */

class Alimo_Widget_Recent_Posts extends WP_Widget {

    /**
     * Sets up a new Recent Posts widget instance.
     *
     * @since 2.8.0
     * @access public
     */
    public function __construct() {
        $widget_ops = array(
            'classname' => 'alimo_widget_recent_entries',
            'description' => esc_html__( 'Your site&#8217;s most recent Posts.', 'alimo' ),
            'customize_selective_refresh' => true,
        );
        parent::__construct( 'alimo-recent-posts', esc_html__( 'Alimo Recent Posts', 'alimo' ), $widget_ops );
        $this->alt_option_name = 'alimo_widget_recent_entries';

	    add_action( 'save_post', [$this, 'flush_widget_cache'] );
	    add_action( 'deleted_post', [$this, 'flush_widget_cache'] );
	    add_action( 'switch_theme', [$this, 'flush_widget_cache'] );
	    add_action( 'init' , [$this, 'alimo_image_size']);
    }

    /**
     * Outputs the content for the current Recent Posts widget instance.
     *
     * @since 2.8.0
     * @access public
     *
     * @param array $args     Display arguments including 'before_title', 'after_title',
     *                        'before_widget', and 'after_widget'.
     * @param array $instance Settings for the current Recent Posts widget instance.
     */
    public function widget( $args, $instance ) {
	    $cache = array();
	    if ( ! $this->is_preview() ) {
		    $cache = wp_cache_get( 'alimo_recent_posts_widget', 'widget' );
	    }

	    if ( ! is_array( $cache ) ) {
		    $cache = array();
	    }

	    if ( ! isset( $args['widget_id'] ) ) {
            $args['widget_id'] = $this->id;
        }
	    if ( isset( $cache[ $args['widget_id'] ] ) ) {
		    echo $cache[ $args['widget_id'] ];
		    return;
	    }
	    ob_start();
        $title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : esc_html__( 'Recent Posts', 'alimo' );

        /** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
        $title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
	    $cat_id = $instance['cat_id'];
        $number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
        if ( ! $number )
            $number = 5;
        $show_only_featured = isset( $instance['show_only_featured'] ) ? $instance['show_only_featured'] : false;
        $show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : false;
        $show_thumbnail = isset( $instance['show_thumbnail'] ) ? $instance['show_thumbnail'] : false;
        $show_reading_time = isset( $instance['show_reading_time'] ) ? (bool) $instance['show_reading_time'] : false;
        $show_likes = isset( $instance['show_likes'] ) ? (bool) $instance['show_likes'] : false;
        $show_comments = isset( $instance['show_comments'] ) ? (bool) $instance['show_comments'] : false;
        $show_share = isset( $instance['show_share'] ) ? (bool) $instance['show_share'] : false;
        $alimo_data = get_option('alimo_data');
        if(!function_exists('alimo_reading_time')){
	        function alimo_reading_time( $post ) {
		        $avg_wpm      = 275;
		        $content      = get_post_field( 'post_content', $post->ID );
		        $word_count   = str_word_count( strip_tags( $content ) );
		        $reading_time = floor( $word_count / $avg_wpm );
		        if ( $reading_time <= 0 ) {
			        return '1';
		        } else {
			        return $reading_time;
		        }

	        }
        }
        /**
         * Filters the arguments for the Recent Posts widget.
         *
         * @since 3.4.0
         *
         * @see WP_Query::get_posts()
         *
         * @param array $args An array of arguments used to retrieve the recent posts.
         */

        $arguments = array(
	        'posts_per_page'      => $number,
	        'cat'               => $cat_id,
	        'meta_query'    => array(
		        array(
			        'key' => 'alimo_meta_featured_post',
			        'value' => 1
		        ),
	        ),
	        'no_found_rows'       => true,
	        'post_status'         => 'publish',
	        'ignore_sticky_posts' => true
        );

        if($cat_id == 0){
            unset($arguments['cat']);
        };
        if(!$show_only_featured){
            unset($arguments['meta_query']);
        };

        $r = new WP_Query( apply_filters( 'widget_posts_args', $arguments ) );
        if ($r->have_posts()) :
            ?>
            <?php echo $args['before_widget']; ?>
            <?php if ( $title ) {
            echo $args['before_title'] . $title . $args['after_title'];
        } ?>
            <ul>
                <?php while ( $r->have_posts() ) : $r->the_post(); ?>
                    <li>
                        <a class="post-title" href="<?php the_permalink(); ?>">
                            <?php if ($show_thumbnail) {
                                global $post;
	                            $prefix              = (class_exists('Alimo_Meta_Boxes'))? Alimo_Meta_Boxes::get_instance()->prefix : 'alimo_meta_';
	                            $post_video_provider = (function_exists('rwmb_meta')) ? rwmb_meta( "{$prefix}video_provider" ): null;
	                            $post_video_id       = (function_exists('rwmb_meta')) ? rwmb_meta( "{$prefix}video_id" ) : null;

	                            $featured_image_url             = get_the_post_thumbnail_url(get_the_ID(),'alimo-recent-post-image');
	                            $featured_image_replacement     = (function_exists('rwmb_meta')) ? is_array(rwmb_meta( "{$prefix}featured_image_replacement" )) ? current( rwmb_meta( "{$prefix}featured_image_replacement", array('size'=>'alimo-recent-post-image') ) ) : null : null;
	                            $featured_image_replacement_url = (!empty($featured_image_replacement))?$featured_image_replacement['full_url'] : null;

	                            $featured_image = ( ! empty( $featured_image_replacement_url ) ) ? $featured_image_replacement_url : $featured_image_url;
	                            if ( empty( $featured_image ) && $post_video_id ) {
		                            $featured_image = alimo_get_video_thumbnail( $post_video_id, $post_video_provider );
	                            }
	                            if($featured_image){
	                                echo "<img src='{$featured_image}' alt=''/>";
                                }

                            }  ?>
                            <?php get_the_title() ? the_title() : the_ID(); ?>
                        </a>
                        <?php if ( $show_date ) : ?>
                            <span class="post-date"><?php echo date_i18n( get_option( 'date_format' )); ?></span>
                        <?php endif; ?>
                        <footer class="row">
                        <?php if ( $show_reading_time ) : ?>
                            <div class="read-time col-xs-6">
                                <i class="fa fa-clock-o"></i>
                                <span><?php echo alimo_reading_time($r->post).' ' ; _e('Min read', 'alimo');  ?></span>
                            </div>
                        <?php endif; ?>
                            <div class="col-xs-6 article-meta">
                            <?php if ( $show_likes && function_exists('zilla_likes') ) : ?>
                                <div class="like"><?php zilla_likes();?></div>
                            <?php endif; ?>
                            <?php if ( $show_comments ) : ?>
                                <?php $comments_number = get_comments_number(); ?>
                                <?php if($comments_number > 0){ ?>
                                    <div class="comments">
                                        <a href="<?php echo get_the_permalink(); ?>#comments">
                                            <i class="fa fa-comment-o"></i>
                                            <span><?php echo $comments_number;?></span>
                                        </a>
                                    </div>
                                <?php } ?>
                            <?php endif; ?>
                            <?php
                            $post_sharing_networks  = array_filter($alimo_data['post_sharing_networks']);
                            if ( $show_share && !empty($post_sharing_networks) ) : ?>
                                <div class="share">
                                    <i class="fa fa-share"></i>
                                    <?php get_template_part('template-parts/sharing-options','tooltip'); ?>
                                </div>
                            <?php endif; ?>
                            </div>
                        </footer>
                    </li>
                <?php endwhile; ?>
            </ul>
            <?php echo $args['after_widget']; ?>
            <?php
            // Reset the global $the_post as this query will have stomped on it
            wp_reset_postdata();
	        if ( ! $this->is_preview() ) {
		        $cache[ $args['widget_id'] ] = ob_get_flush();
		        wp_cache_set( 'alimo_recent_posts_widget', $cache, 'widget' );
	        } else {
		        ob_end_flush();
	        }
        endif;
    }

    /**
     * Handles updating the settings for the current Recent Posts widget instance.
     *
     * @since 2.8.0
     * @access public
     *
     * @param array $new_instance New settings for this instance as input by the user via
     *                            WP_Widget::form().
     * @param array $old_instance Old settings for this instance.
     * @return array Updated settings to save.
     */
    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title']              = sanitize_text_field( $new_instance['title'] );
	    $instance['cat_id']             = (int) $new_instance['cat_id'];
	    $instance['number']             = (int) $new_instance['number'];
	    $instance['show_only_featured'] = isset( $new_instance['show_only_featured'] ) ? (bool) $new_instance['show_only_featured'] : false;
        $instance['show_date']          = isset( $new_instance['show_date'] ) ? (bool) $new_instance['show_date'] : false;
        $instance['show_thumbnail']     = isset( $new_instance['show_thumbnail'] ) ? (bool) $new_instance['show_thumbnail'] : false;
        $instance['show_reading_time']  = isset( $new_instance['show_reading_time'] ) ? (bool) $new_instance['show_reading_time'] : false;
        $instance['show_likes']         = isset( $new_instance['show_likes'] ) ? (bool) $new_instance['show_likes'] : false;
        $instance['show_comments']      = isset( $new_instance['show_comments'] ) ? (bool) $new_instance['show_comments'] : false;
        $instance['show_share']         = isset( $new_instance['show_share'] ) ? (bool) $new_instance['show_share'] : false;
	    $this->flush_widget_cache();

	    $all_options = wp_cache_get( 'alloptions', 'options' );
	    if ( isset($all_options['alimo-recent-posts']) )
		    delete_option('alimo-recent-posts');

	    return $instance;
    }


    public function alimo_image_size(){
	    add_image_size( 'alimo-recent-post-image', 300, 0 );
    }
	public function flush_widget_cache()
	{
		wp_cache_delete('alimo_recent_posts_widget', 'widget');
	}
    /**
     * Outputs the settings form for the Recent Posts widget.
     *
     * @since 2.8.0
     * @access public
     *
     * @param array $instance Current settings.
     */
    public function form( $instance ) {
        $title              = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
	    $cat_id             = isset( $instance['cat_id'] ) ? absint( $instance['cat_id'] ) : 0;
        $number             = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
        $show_only_featured = isset( $instance['show_only_featured'] ) ? (bool) $instance['show_only_featured'] : false;
        $show_date          = isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : true;
        $show_thumbnail     = isset( $instance['show_thumbnail'] ) ? (bool) $instance['show_thumbnail'] : true;
        $show_reading_time  = isset( $instance['show_reading_time'] ) ? (bool) $instance['show_reading_time'] : true;
        $show_likes         = isset( $instance['show_likes'] ) ? (bool) $instance['show_likes'] : true;
        $show_comments      = isset( $instance['show_comments'] ) ? (bool) $instance['show_comments'] : true;
        $show_share         = isset( $instance['show_share'] ) ? (bool) $instance['show_share'] : true;
        ?>
        <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'alimo' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>

        <p>
            <label for="<?php echo $this->get_field_id('cat_id'); ?>"><?php _e( 'Show posts from:', 'alimo' )?></label>
            <select id="<?php echo $this->get_field_id('cat_id'); ?>" name="<?php echo $this->get_field_name('cat_id'); ?>">
                <?php
                $all_cats_selected = ($cat_id == 0) ? 'selected ="selected"': '';
                echo "<option {$all_cats_selected} value='0'>All Categories</option>";

			    $categories = get_categories();
			    foreach ( $categories as $cat ) {
				    $selected = ( $cat->term_id == esc_attr( $cat_id ) ) ? ' selected = "selected" ' : '';
				    $option = '<option '.$selected .'value="' . $cat->term_id;
				    $option = $option .'">';
				    $option = $option .$cat->name;
				    $option = $option .'</option>';
				    echo $option;
			    }
			    ?>
            </select>
        </p>

        <p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:', 'alimo' ); ?></label>
            <input class="tiny-text" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="number" step="1" min="1" value="<?php echo $number; ?>" size="3" /></p>
        <p><input class="checkbox" type="checkbox"<?php checked( $show_only_featured ); ?> id="<?php echo $this->get_field_id( 'show_only_featured' ); ?>" name="<?php echo $this->get_field_name( 'show_only_featured' ); ?>" />
            <label for="<?php echo $this->get_field_id( 'show_only_featured' ); ?>"><?php _e( 'Show only featured posts', 'alimo' ); ?></label><br>
            <i>If this option is checked, only posts that have the <a href="http://help.layerblend.com/alimo#featured-post">featured</a> flag enabled will be shown.</i>
        </p>

        <hr>
        <h4>Post information to display:</h4>
        <?php /* Post date */ ?>
        <p><input class="checkbox" type="checkbox"<?php checked( $show_date ); ?> id="<?php echo $this->get_field_id( 'show_date' ); ?>" name="<?php echo $this->get_field_name( 'show_date' ); ?>" />
            <label for="<?php echo $this->get_field_id( 'show_date' ); ?>"><?php _e( 'Post date', 'alimo' ); ?></label></p>
        <?php /* Post featured image */ ?>
        <p><input class="checkbox" type="checkbox"<?php checked( $show_thumbnail ); ?> id="<?php echo $this->get_field_id( 'show_thumbnail' ); ?>" name="<?php echo $this->get_field_name( 'show_thumbnail' ); ?>" />
            <label for="<?php echo $this->get_field_id( 'show_thumbnail' ); ?>"><?php _e( 'Featured image', 'alimo' ); ?></label></p>
        <?php /* Post reading time */ ?>
        <p><input class="checkbox" type="checkbox"<?php checked( $show_reading_time ); ?> id="<?php echo $this->get_field_id( 'show_reading_time' ); ?>" name="<?php echo $this->get_field_name( 'show_reading_time' ); ?>" />
            <label for="<?php echo $this->get_field_id( 'show_reading_time' ); ?>"><?php _e( 'Reading time', 'alimo' ); ?></label></p>
        <?php /* Post likes */ ?>
        <p><input class="checkbox" type="checkbox"<?php checked( $show_likes ); ?> id="<?php echo $this->get_field_id( 'show_likes' ); ?>" name="<?php echo $this->get_field_name( 'show_likes' ); ?>" />
            <label for="<?php echo $this->get_field_id( 'show_likes' ); ?>"><?php _e( 'Likes', 'alimo' ); ?></label></p>
        <?php /* Post comments */ ?>
        <p><input class="checkbox" type="checkbox"<?php checked( $show_comments ); ?> id="<?php echo $this->get_field_id( 'show_comments' ); ?>" name="<?php echo $this->get_field_name( 'show_comments' ); ?>" />
            <label for="<?php echo $this->get_field_id( 'show_comments' ); ?>"><?php _e( 'Comments', 'alimo' ); ?></label></p>
        <?php /* Post share */ ?>
        <p><input class="checkbox" type="checkbox"<?php checked( $show_share ); ?> id="<?php echo $this->get_field_id( 'show_share' ); ?>" name="<?php echo $this->get_field_name( 'show_share' ); ?>" />
            <label for="<?php echo $this->get_field_id( 'show_share' ); ?>"><?php _e( 'Sharing options', 'alimo' ); ?></label></p>
        <?php
    }
}

