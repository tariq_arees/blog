<?php

/**
 * Alimo Author widget.
 *
 * @since 1.1.0
 *
 * @see WP_Widget
 */
class Alimo_Widget_Author extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		// Add Widget scripts
		add_action( 'admin_enqueue_scripts', array( $this, 'scripts' ) );
		add_filter( 'user_contactmethods', array(
			$this,
			'alimo_modify_contact_methods',
		) );

		parent::__construct(
			'Alimo_Author', // Base ID
			esc_html__( 'Alimo Author', 'alimo' ), // Name
			array( 'description' => esc_html__( 'Adds an author widget.', 'alimo' ), )
		);

	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {

		$author            = ! empty( $instance['author'] ) ? $instance['author'] : '';
		$image             = ! empty( $instance['image'] ) ? $instance['image'] : '';
		$available_authors = get_users( array( 'who' => 'authors' ) );
		echo $args['before_widget'];

		$author_exists      = FALSE;
		foreach ( (array)($available_authors) as $available_author ) {
			if( $author == $available_author->ID ){
				$author_exists =  TRUE;
			}
		}
		if ( ! empty( $author ) && $author_exists ) :

			$user_info = get_userdata( $author );
			$user_name      = $user_info->user_nicename;
			$user_ninckname = $user_info->display_name;
			$author_url     = get_site_url() . '/author/' . $user_name; ?>

            <aside class="widget alimo_author_widget">
                <figure>
                    <a href="<?php echo esc_url( $author_url ); ?>">
						<?php if ( $image ): ?>
                            <img src="<?php echo esc_url( $image ); ?>" alt="">
						<?php else: ?>
							<?php echo get_avatar( $author, 600 ); ?>
						<?php endif; ?>
                    </a>
                </figure>
                <h3><?php echo "<a href='" . esc_url( $author_url ) . "'>" . esc_html( $user_ninckname ) . "</a>" ?></h3>
				<?php $description = get_user_meta( $author, 'description', TRUE );
				if ( $description ) { ?>
                    <div class="author-description text-center">
                        <p>
							<?php echo wp_kses_post( $description ); /*echo ( strlen( $description ) > 100 ) ? substr( $description, 0, 100 ) . ' ...' : $description;*/ ?>
                        </p>
                    </div>
				<?php }
				$facebook = esc_url( get_user_meta( $author, 'facebook', TRUE ) );
				$twitter  = esc_url( get_user_meta( $author, 'twitter', TRUE ) );
				$linkedin = esc_url( get_user_meta( $author, 'linkedin', TRUE ) );
				$gplus    = esc_url( get_user_meta( $author, 'google-plus', TRUE ) );
				if ( ! empty( $facebook ) || ! empty( $twitter ) || ! empty( $gplus ) || ! empty( $linkedin ) ):
					echo "<div class='social-profiles'>";
					echo ( $facebook ) ? "<a href='{$facebook}' target='_blank'><i class='fa fa-facebook'></i></a>" : '';
					echo ( $twitter ) ? "<a href='{$twitter}' target='_blank'><i class='fa fa-twitter'></i></a>" : '';
					echo ( $linkedin ) ? "<a href='{$linkedin}' target='_blank'><i class='fa fa-linkedin'></i></a>" : '';
					echo ( $gplus ) ? "<a href='{$gplus}' target='_blank'><i class='fa fa-google-plus'></i></a>" : '';
					echo "</div>";
				endif; ?>
            </aside>

		<?php endif; ?>

		<?php

		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 * @param array $instance
	 *
	 * @return string|void
	 */
	public function form( $instance ) {

		if ( isset( $instance['author'] ) ) {
			$selected_author = $instance['author'];
		} else {
			$selected_author = '';
		}

		$available_authors = get_users( array( 'who' => 'authors' ) );
		$image             = ! empty( $instance['image'] ) ? $instance['image'] : '';
		$author_exists     = FALSE;
		foreach ( (array)($available_authors) as $available_author ) {
			if( $selected_author == $available_author->ID ){
				$author_exists =  TRUE;
            }
		}
		?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'text' ) ); ?>">Author:
                <select class='widefat' id="<?php echo esc_attr( $this->get_field_id( 'author' ) ); ?>"
                        name="<?php echo esc_attr( $this->get_field_name( 'author' ) ); ?>" type="text">
					<?php if ( ! empty( $selected_author ) && !$author_exists ): ?>
                        <option selected value=""><?php esc_html_e( 'Please select', 'alimo' ) ?></option>
					<?php endif; ?>
					<?php foreach ( $available_authors as $author ) {
						$option_selected = ($selected_author == $author->ID) ? 'selected' : null;
                    ?>
                        <option <?php echo  esc_attr($option_selected)?> value="<?php echo esc_attr( $author->ID ); ?>"><?php echo esc_html( $author->user_login ); ?></option>
					<?php } ?>
                </select>
            </label>
        </p>
        <p class="info">If you want to use a custom image, upload it below:</p>
        <p>
            <label style="display: block;"
                   for="<?php echo esc_attr( $this->get_field_id( 'image' ) ); ?>"><?php _e( 'Image:', 'alimo' ); ?></label>
            <img class="wrap" style="max-width: 100%" src="<?php echo esc_url( $image ); ?>" alt="">
            <input class="widefat hidden" id="<?php echo esc_attr( $this->get_field_id( 'image' ) ); ?>"
                   name="<?php echo esc_attr( $this->get_field_name( 'image' ) ); ?>" type="text"
                   value="<?php echo esc_url( $image ); ?>"/>
            <button class="upload_image_button button button-primary">Upload
                Image
            </button>
			<?php if ( $image ): ?>
                <button class="remove_image_button button button-cancel">Remove
                    Image
                </button>
			<?php endif; ?>
        </p>
        <p class="info">You can display your personal social profiles by adding
            them to your Contact Info section in your user profile page
            <a href="<?php echo get_edit_profile_url( $author->ID ) ?>">here</a>.
        </p>
		<?php
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();

		$instance['author'] = ( ! empty( $new_instance['author'] ) ) ? strip_tags( $new_instance['author'] ) : '';
		$instance['image']  = ( ! empty( $new_instance['image'] ) ) ? $new_instance['image'] : '';

		return $instance;
	}

	/**
	 *
	 */
	public function scripts() {
		wp_enqueue_script( 'media-upload' );
		wp_enqueue_media();
		wp_enqueue_script( 'alimo-author-widget', plugins_url() . '/alimo-widgets/js/author-widget.js', array( 'jquery' ) );
	}


	/**
	 * Add user profile social fields
	 *
	 * @param $profile_fields
	 *
	 * @return mixed
	 */
	function alimo_modify_contact_methods( $profile_fields ) {

		// Add new fields
		$profile_fields['facebook']    = esc_html__( 'Facebook URL', 'alimo' );
		$profile_fields['twitter']     = esc_html__( 'Twitter URL', 'alimo' );
		$profile_fields['linkedin']    = esc_html__( 'LinkedIn URL', 'alimo' );
		$profile_fields['google-plus'] = esc_html__( 'Google+ URL', 'alimo' );

		return $profile_fields;
	}


}