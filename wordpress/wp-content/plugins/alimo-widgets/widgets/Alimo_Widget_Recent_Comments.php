<?php
/**
 * Widget API: WP_Widget_Recent_Comments class
 *
 * @package WordPress
 * @subpackage Widgets
 * @since 4.4.0
 */

/**
 * Alimo version of Wordpress Recent Comments widget.
 *
 * @since 1.0.0
 *
 * @see WP_Widget
 */
class Alimo_Widget_Recent_Comments extends WP_Widget {

    /**
     * Sets up a new Recent Comments widget instance.
     *
     * @since 2.8.0
     * @access public
     */
    public function __construct() {
        $widget_ops = array(
            'classname' => 'alimo_widget_recent_comments',
            'description' => esc_html__( 'Your site&#8217;s most recent comments.', 'alimo' ),
            'customize_selective_refresh' => true,
        );
        parent::__construct( 'alimo-recent-comments', esc_html__( 'Alimo Recent Comments', 'alimo' ), $widget_ops );
        $this->alt_option_name = 'alimo_widget_recent_comments';

        if ( is_active_widget( false, false, $this->id_base ) || is_customize_preview() ) {
            add_action( 'wp_head', array( $this, 'recent_comments_style' ) );
        }
    }

    /**
     * Outputs the default styles for the Recent Comments widget.
     *
     * @since 2.8.0
     * @access public
     */
    public function recent_comments_style() {
        /**
         * Filters the Recent Comments default widget styles.
         *
         * @since 3.1.0
         *
         * @param bool   $active  Whether the widget is active. Default true.
         * @param string $id_base The widget ID.
         */
        if ( ! current_theme_supports( 'widgets' ) // Temp hack #14876
            || ! apply_filters( 'show_recent_comments_widget_style', true, $this->id_base ) )
            return;
        ?>
        <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
        <?php
    }

    /**
     * Outputs the content for the current Recent Comments widget instance.
     *
     * @since 2.8.0
     * @access public
     *
     * @param array $args     Display arguments including 'before_title', 'after_title',
     *                        'before_widget', and 'after_widget'.
     * @param array $instance Settings for the current Recent Comments widget instance.
     */
    public function widget( $args, $instance ) {
        if ( ! isset( $args['widget_id'] ) )
            $args['widget_id'] = $this->id;

        $output = '';

        $title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : esc_html__( 'Recent Comments', 'alimo' );

        /** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
        $title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

        $number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
        if ( ! $number )
            $number = 5;

        if(!function_exists('alimo_relative_comment_time')){
	        function alimo_relative_comment_time( $comment_ID ) {
		        $post_date = get_comment_date( 'U', $comment_ID );
		        $delta     = time() - $post_date;
		        if ( $delta < 60 ) {
			        $formatted_time = esc_html__('Less than a minute ago','alimo');
		        } elseif ( $delta > 60 && $delta < 120 ) {
			        $formatted_time = esc_html__('About a minute ago','alimo');
		        } elseif ( $delta > 120 && $delta < ( 60 * 60 ) ) {
			        $formatted_time = sprintf(esc_html__('%s minutes ago','alimo'),strval( round( ( $delta / 60 ), 0 ) ) );
		        } elseif ( $delta > ( 60 * 60 ) && $delta < ( 120 * 60 ) ) {
			        $formatted_time = esc_html__('About an hour ago','alimo');
		        } elseif ( $delta > ( 120 * 60 ) && $delta < ( 24 * 60 * 60 ) ) {
			        $formatted_time = sprintf(esc_html__('%s hours ago','alimo'),strval( round( ( $delta / 3600 ), 0 ) ));
		        } else {
			        $formatted_time = date_i18n( 'j\<\s\u\p\>S\<\/\s\u\p\> M y g:i a' );
		        }

		        return $formatted_time;
	        }
        }

        /**
         * Filters the arguments for the Recent Comments widget.
         *
         * @since 3.4.0
         *
         * @see WP_Comment_Query::query() for information on accepted arguments.
         *
         * @param array $comment_args An array of arguments used to retrieve the recent comments.
         */
        $comments = get_comments( apply_filters( 'widget_comments_args', array(
            'number'      => $number,
            'status'      => 'approve',
            'post_status' => 'publish'
        ) ) );

        $output .= $args['before_widget'];
        if ( $title ) {
            $output .= $args['before_title'] . $title . $args['after_title'];
        }

        $output .= '<ul id="recentcomments">';
        if ( is_array( $comments ) && $comments ) {
            // Prime cache for associated posts. (Prime post term cache if we need it for permalinks.)
            $post_ids = array_unique( wp_list_pluck( $comments, 'comment_post_ID' ) );
            _prime_post_caches( $post_ids, strpos( get_option( 'permalink_structure' ), '%category%' ), false );

            foreach ( (array) $comments as $comment ) {
                $output .= '<li class="recentcomments">';
                $output .= '<div class="user-avatar">'.get_avatar($comment).'</div>';
                $output .= '<div class="comment-content">';
                /* translators: comments widget: 1: comment author, 2: post link */
                $output .= sprintf( _x( '%1$s %2$s %3$s', 'widgets', 'alimo' ),
                    '<p class="comment-author-link">' . get_comment_author_link( $comment ) . ' on </p>',
                    '<a class="rc-post-title" href="' . esc_url( get_comment_link( $comment ) ) . '">' . get_the_title( $comment->comment_post_ID ) . '</a>',
                    '<p class="comment-date"> '.alimo_relative_comment_time($comment).'</p>'
                );
                $output .= '</div>';
                $output .= '</li>';
            }
        }
        $output .= '</ul>';
        $output .= $args['after_widget'];

        echo $output;
    }

    /**
     * Handles updating settings for the current Recent Comments widget instance.
     *
     * @since 2.8.0
     * @access public
     *
     * @param array $new_instance New settings for this instance as input by the user via
     *                            WP_Widget::form().
     * @param array $old_instance Old settings for this instance.
     * @return array Updated settings to save.
     */
    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = sanitize_text_field( $new_instance['title'] );
        $instance['number'] = absint( $new_instance['number'] );
        return $instance;
    }

    /**
     * Outputs the settings form for the Recent Comments widget.
     *
     * @since 2.8.0
     * @access public
     *
     * @param array $instance Current settings.
     */
    public function form( $instance ) {
        $title = isset( $instance['title'] ) ? $instance['title'] : '';
        $number = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
        ?>
        <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'alimo' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></p>

        <p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of comments to show:', 'alimo'  ); ?></label>
            <input class="tiny-text" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="number" step="1" min="1" value="<?php echo $number; ?>" size="3" /></p>
        <?php
    }

    /**
     * Flushes the Recent Comments widget cache.
     *
     * @since 2.8.0
     * @access public
     *
     * @deprecated 4.4.0 Fragment caching was removed in favor of split queries.
     */
    public function flush_widget_cache() {
        _deprecated_function( __METHOD__, '4.4.0' );
    }
}
