<?php
/*
Plugin Name: Alimo Widgets
Plugin URI:
Description: This plugin contains all widgets for Alimo.
Version: 1.1.0
Author: LayerBlend
Author URI: http://www.layerblend.com
*/
// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }


class Alimo_Widgets {

    protected static $instance = null;

    private function __construct() {

        // Require the widgets files
		require_once('widgets/Alimo_Widget_Author.php');
		require_once('widgets/Alimo_Widget_Recent_Posts.php');
		require_once('widgets/Alimo_Widget_Recent_Comments.php');

        // Init the widgets
        add_action( 'widgets_init', array( &$this, 'register_widgets' ));

    }

    // register widgets
    public function register_widgets() {
		register_widget( 'Alimo_Widget_Author' );
		register_widget( 'Alimo_Widget_Recent_Posts' );
		register_widget( 'Alimo_Widget_Recent_Comments' );
    }

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object  A single instance of this class.
     */
    public static function get_instance() {

        // If the single instance hasn't been set, set it now.
        if ( null == self::$instance ) {
            self::$instance = new self;
        }

        return self::$instance;
    }

}

Alimo_Widgets::get_instance();

