// initialise some required variables for reading mode and quote sharing.
var reading_mode_inherited,social_quote_sharer_facebook_id;

jQuery(document).ready(function () {

  "use strict";

  // @INCLUDE BACKGROUND AUDIO - view source in /assets/js_modules/
  // This component enables the background audio feature for blog posts
  window.alimo_background_audio.init();

  // @INCLUDE READING MODE - view source in /assets/js_modules/
  // This component enables the reading mode feature for blog posts
  window.alimo_reading_mode.init();

  // @INCLUDE MOBILE NAV - view source in /assets/js_modules/
  // This component contains the mobile navigation interaction
  window.alimo_mobile_nav.init();

  // @INCLUDE SIDEBAR - view source in /assets/js_modules/
  // This component controls the sidebar functionality
  window.alimo_sidebar.init();

  // @INCLUDE WOOCOMMERCE - view source in /assets/js_modules/
  // This component controls the WooCommerce classes added to the header minicart
  window.alimo_woocommerce.init();

  // @INCLUDE INIT PLUGINS - view source in /assets/js_modules/
  // This component is used for third party plugin initialisation
  window.alimo_init_plugins.init();


  jQuery(window).load(function () {
    // @INCLUDE SHARE WIDGET - view source in /assets/js_modules/
    // This component controls the behaviour of the share widget on the single post
    window.alimo_share_widget.init();
    // @INCLUDE READING PROGRESS - view source in /assets/js_modules/
    // This component enables the reading progress bar feature for posts
    window.alimo_reading_progress.init();

  });


  // triggers for window resize event
  jQuery(window).resize(function () {
    // reinitialise reading progress
    window.alimo_reading_progress.init();
    // reinitialise masonry

    var $grid = jQuery('.articles-container-masonry #content');
    $grid.masonry('destroy');
    $grid.masonry({
      columnWidth: 'article',
      itemSelector: 'article',
      percentPosition: true
    });
    // layout Masonry after each image loads
    $grid.imagesLoaded().progress( function() {
      $grid.masonry('layout');
    });

  });


});