/**
 * PLUGIN INIT
 *
 * This component is used for third party plugin initialisation
 *
 * @package Alimo
 * @version 1.0.0
 */


window.alimo_init_plugins = (function (window, document, $) {
  function init() {

    // FitVids Options
    $('html').fitVids();

    var afw_insta = $('.above-footer-widget ul.instagram-pics');
    if (afw_insta.length > 0) {
      afw_insta.slick({
        infinite: true,
        slidesToShow: 5,
        slidesToScroll: 1,
        arrows: true,
        prevArrow: '<div class="prevArrow"></div>',
        nextArrow: '<div class="nextArrow"></div>'
      });
    }

    var sc = $('.search-container');

    sc.bind('touchstart', function () {
      $(this).addClass('hovered');
    });


    $("[data-fancybox]").fancybox({

      loop : true,
      // Open/close animation type possible values:
      //   false            - disable
      //   "zoom"           - zoom images from/to thumbnail
      //   "fade", "zoom-in-out"
      animationEffect : "zoom",
      // Duration in ms for open/close animation
      animationDuration : 300,
      // Transition effect between slides possible values:
      //   false            - disable
      //   "fade',"slide',"circular',"tube',"zoom-in-out',"rotate'
      transitionEffect : "slide",

      // Duration in ms for transition animation
      transitionDuration : 366,
      touch : {
        vertical : true,  // Allow to drag content vertically
        momentum : true   // Continuous movement when panning
      },
    });



    // "single-post-content" is the class of blog-single content container
    $(".gallery-item .gallery-icon a").attr('data-fancybox', 'gallery').fancybox({
      loop: true,
      animationEffect : "zoom",
      animationDuration : 300,
      transitionEffect : "slide",
      transitionDuration : 366,
      touch : {
        vertical : true,  // Allow to drag content vertically
        momentum : true   // Continuous movement when panning
      }
    });

    // init Masonry
    var $grid = $('.articles-container-masonry #content').masonry({
      columnWidth: 'article',
      itemSelector: 'article',
      percentPosition: true
    });
    // layout Masonry after each image loads
    $grid.imagesLoaded().progress( function() {
      $grid.masonry('layout');
    });



    // Tooltip
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    });

    // Smooth Scroll
    $('a[data-scroll]').smoothScroll();

    // Headroom
    var myElement = document.querySelector("header.main-header");
    var headroom = new Headroom(myElement, {
      offset: 72,
      tolerance: {
        up: 10,
        down: 0
      }
    });
    headroom.init();


    //Card animation
    var itemQueue = [];
    var queueTimer;
    var delay = 50;
    var ai = '.animate-in';
    var class_name = 'fade-in-up';

    function animateArticlesOnScroll(selector) {
      if (selector.length > 0) {
        inView(selector).on('enter', function (el) {
          itemQueue.push($(el));
          if ($(el).hasClass('interlaced-article')) {
            class_name = 'fade-in';
          }
          processItemQueue(class_name)
        });

      }
    }
    animateArticlesOnScroll(ai);

    function processItemQueue(class_name) {
      if (queueTimer) {
        return;
      }
      queueTimer = window.setInterval(function () {
        if (itemQueue.length) {
          $(itemQueue.shift()).addClass(class_name);
          firstItem = false;
          animationDelay = delay;
          processItemQueue()
        }
        else {
          window.clearInterval(queueTimer);
          queueTimer = null
        }
      }, delay)
    }

    $( document.body ).on( 'post-load', function () {
      $('span.infinite-loader').remove();
      setInterval( function() {
        $grid.imagesLoaded().progress( function() {
          $grid.masonry('layout');
        });
      }, 300 );
      animateArticlesOnScroll(ai);
    } );



    // FB SDK
    (function (d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {
        return;
      }
      js = d.createElement(s);
      js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  }

  return {
    init: init
  }
}(window, document, jQuery));