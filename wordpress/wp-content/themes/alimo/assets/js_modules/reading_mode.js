/**
 * READING MODE
 *
 * This component enables the reading mode feature for blog posts
 *
 * @package Alimo
 * @version 1.0.0
 */

window.alimo_reading_mode = (function (window, document, $) {
    function init() {

        var Cookie = window.alimo_cookie;
        var colorSchemeClasses = 'light dark sepia';
        var fontTypeClasses = 'serif sans-serif';
        var readingModeTrigger = $('.reading-mode');
        var readingModeContainer = $('.reading-mode-container');
        var readingModeContentContainer = readingModeContainer.find('.reading-mode-content-container');
        var fontSize = (Cookie.getCookie('fontSize')) ? Cookie.getCookie('fontSize') : readingModeContentContainer.attr('data-size');
        var lineHeight = (Cookie.getCookie('lineHeight')) ? Cookie.getCookie('lineHeight') : readingModeContentContainer.attr('data-line-height');
        var increaseFontSize = $('.font-size .increase');
        var decreaseFontSize = $('.font-size .decrease');
        var increaseLineHeight = $('.line-height .increase');
        var decreaseLineHeight = $('.line-height .decrease');

        function setReadingModeFontType(type) {
            readingModeContentContainer.removeClass(fontTypeClasses).addClass(type);
            Cookie.setCookie('fontType', type, '1');
        }

        function setReadingModeFontSize(size) {
            readingModeContentContainer.attr('data-size', size).removeClass(function (index, css) {
                return (css.match(/(^|\s)size-\S+/g) || []).join(' ');
            }).addClass('size-' + size);
            Cookie.setCookie('fontSize', size, '1');
        }

        function setReadingModeLineHeight(lineHeight) {
            readingModeContentContainer.attr('data-line-height', lineHeight).removeClass(function (index, css) {
                return (css.match(/(^|\s)lineHeight-\S+/g) || []).join(' ');
            }).addClass('lineHeight-' + lineHeight);
            Cookie.setCookie('lineHeight', lineHeight, '1');
        }

        function setReadingModeColorScheme(scheme) {
            readingModeContainer.removeClass(colorSchemeClasses).addClass(scheme);
            readingModeTrigger.removeClass(colorSchemeClasses).addClass(scheme);
            Cookie.setCookie('colorScheme', scheme, '1');
        }

        /* Check existing reading mode cookies and set them if they exist */
        function getReadingMode() {
            (Cookie.getCookie('fontType')) ? setReadingModeFontType(Cookie.getCookie('fontType')) : null;
            (Cookie.getCookie('fontSize')) ? setReadingModeFontSize(Cookie.getCookie('fontSize')) : null;
            (Cookie.getCookie('lineHeight')) ? setReadingModeLineHeight(Cookie.getCookie('lineHeight')) : null;
            (Cookie.getCookie('colorScheme')) ? setReadingModeColorScheme(Cookie.getCookie('colorScheme')) : null;
        }

        getReadingMode();


        /* Set font type controls */
        $('.font-box.serif').click(function () {
            $(this).addClass('active').siblings().removeClass('active');
            setReadingModeFontType('serif');
        });

        $('.font-box.sans-serif').click(function () {
            $(this).addClass('active').siblings().removeClass('active');
            setReadingModeFontType('sans-serif');
        });

        /* Set font size controls */
        $(increaseFontSize).click(function () {
            if (fontSize < 4) {
                fontSize++;
                setReadingModeFontSize(fontSize);
            }
        });

        $(decreaseFontSize).click(function () {
            if (fontSize > 1) {
                fontSize--;
                setReadingModeFontSize(fontSize);
            }
        });

        /* Set line height controls */
        $(increaseLineHeight).click(function () {
            if (lineHeight < 4) {
                lineHeight++;
                setReadingModeLineHeight(lineHeight);
            }
        });

        $(decreaseLineHeight).click(function () {
            if (lineHeight > 1) {
                lineHeight--;
                setReadingModeLineHeight(lineHeight);
            }
        });

        /* Set color scheme */
        $('.cs-option.light').click(function () {
            $(this).addClass('active').siblings().removeClass('active');
            setReadingModeColorScheme('light');
        });

        $('.cs-option.dark').click(function () {
            $(this).addClass('active').siblings().removeClass('active');
            setReadingModeColorScheme('dark');
        });

        $('.cs-option.sepia').click(function () {
            $(this).addClass('active').siblings().removeClass('active');
            setReadingModeColorScheme('sepia');
        });


        var getReadingModeContent = 0;
        var readingOptionsContainer = readingModeContainer.find('.reading-options-container');
        var readingOptionsTrigger = readingModeContainer.find('.reading-options-trigger');

        var htmlSelector = $('html');
        htmlSelector.click(function () {
            readingOptionsContainer.removeClass('active');
            readingOptionsTrigger.removeClass('active');
        });

        readingOptionsTrigger.click(function (e) {
            e.stopPropagation();
            $(this).toggleClass('active');
            readingOptionsContainer.toggleClass('active');
        });

        readingOptionsContainer.click(function (e) {
            e.stopPropagation();
        });

        function addReadingModeContent() {
            var regex = /\[.+\]/g;
            var title = $('.single-post-title h1').text();
            var content = $('.post-content').html();
            var content_without_shortcodes = content.replace(regex,'');
            readingModeContainer.find('.the-title').html(title);
            readingModeContainer.find('.the-content').html(content_without_shortcodes);

        }

        function enableReadingMode() {
            readingModeTrigger.toggleClass('active');
            readingModeTrigger.find('i').toggleClass('fa-times fa-book');
            htmlSelector.toggleClass('reading-mode-active');
            if (getReadingModeContent === 0) {
                addReadingModeContent();
            }
            getReadingModeContent = 1;
        }

        readingModeTrigger.click(function () {
            enableReadingMode();
            window.dispatchEvent(new Event('resize'));
        });

        if (reading_mode_inherited === 'enabled') {
            enableReadingMode();
        }
    }

    return {
        init: init
    }
}(window, document, jQuery));