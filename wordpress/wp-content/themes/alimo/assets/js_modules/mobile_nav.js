/**
 * MOBILE NAVIGATION
 *
 * This component contains the mobile navigation interaction
 *
 * @package: Alimo
 * @version: 1.0.0
 */

window.alimo_mobile_nav = (function (window, document, $) {
    function init() {
        /* Mobile navigation toggle */

        var body = $('body');
        var header = $('header.main-header');
        var mainMenu = header.find('.main-nav');
        var phoneMenuTrigger = mainMenu.find('.phone-menu-trigger');
        var toggleSubmenu =  mainMenu.find('li.menu-item-has-children .toggle-submenu');

        var searchContainer = $('.search-container');
        var searchOpen = searchContainer.find('.search-open');
        var searchClose = searchContainer.find('.search-close');
        var searchInput = searchContainer.find('.main-search input');

        phoneMenuTrigger.on('click', function(e){
            e.stopPropagation();
            mainMenu.toggleClass('menu-is-opened');
            header.toggleClass('menu-on');
        });
        toggleSubmenu.on('click', function() {
          $(this).siblings('ul.sub-menu').toggleClass('open');
          //$(this).toggleClass('fa-angle-down').toggleClass('fa-angle-up');
        });

        body.on('click touchend', function(){
            if(mainMenu.hasClass('menu-is-opened')){
                mainMenu.removeClass('menu-is-opened');
            }
            if(header.hasClass('search-active')){
                body.removeClass('search-active');
                header.removeClass('search-active');
                searchContainer.removeClass('hovered');
            }
        });
        mainMenu.on('click touchend', function(e){
            e.stopPropagation();
            if(header.hasClass('search-active')){
                body.removeClass('search-active');
                header.removeClass('search-active');
                searchContainer.removeClass('hovered');
            }
        });
        searchContainer.on('click touchend', function(e){
            e.stopPropagation();
        });
        searchOpen.on('click', function(){
            body.addClass('search-active');
            header.addClass('search-active');
            if(mainMenu.hasClass('menu-is-opened')){
                mainMenu.removeClass('menu-is-opened');
            }
            //wait for the css animation to finish, otherwise focus triggers immediately and breaks the layout
            searchContainer.find('.search-trigger-bg').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(e){
                searchInput.focus();
            });
        });
        searchClose.click( function(){
            body.removeClass('search-active');
            header.removeClass('search-active');
            searchContainer.removeClass('hovered');
        });
    }
    return{
        init: init
    }
}(window, document, jQuery));