/**
 * SIDEBAR
 *
 * This component controls the sidebar functionality
 *
 * @package: Alimo
 * @version: 1.0.0
 */

window.alimo_sidebar = (function (window, document, $) {

    var lastScrollTop = 0;
    var stickySidebar = $('.sticky-sidebar');
    var fixedSidebar = $('.fixed-sidebar');
    var sidebarClosed = $('.sticky-sidebar:not(.is-opened)');
    var header = $('header.main-header');

    function init() {
        if(fixedSidebar.length > 0 && header.hasClass('visible-sidebar')){
            header.removeClass('visible-sidebar');
        }
          if(fixedSidebar.length === 0 && stickySidebar.length === 0 ){
            header.removeClass('visible-sidebar');
          }
        stickySidebar.find('.sidebar-open-trigger').click(function () {
            stickySidebar.toggleClass('is-opened');
        });

        stickySidebar.find('.clearfix').click(function () {
            stickySidebar.removeClass('is-opened');
        });

        $(window).scroll(function (event) {
            if(stickySidebar.length > 0 && !stickySidebar.hasClass('is-opened')){
                var st = $(this).scrollTop();

                if (st > lastScrollTop) {
                    sidebarClosed.addClass('hidden-sidebar');
                    sidebarClosed.removeClass('visible-sidebar');
                    header.removeClass('visible-sidebar');
                } else {
                    sidebarClosed.addClass('visible-sidebar');
                    sidebarClosed.removeClass('hidden-sidebar');
                    header.addClass('visible-sidebar');
                }
                lastScrollTop = st;
            }
            window.alimo_reading_progress.init();
            window.alimo_share_widget.init();
        });

    }

    return{
        init: init
    }
}(window, document, jQuery));