This folder contains all the components in js/main.min.js
These files together with main.js are used to build the js/main.min.js file.

If you need to make any changes to any of these modules, you have 2 options:
1) Recommended : After you have made the changes, use the gulp task in mainjs-gulptask.js file to rebuild the file
OR
2) load all the modules indivitualy and use in conjunction with main.js - see functions.php line ~:280 (search for `Alimo Main JS Components`)

The first method allows you to have one single file (main.min.js) and one single request instead of 9 (all modules + main.js).
