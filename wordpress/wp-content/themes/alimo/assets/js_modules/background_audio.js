/**
 * BACKGROUND AUDIO
 *
 * This component enables the background audio feature for blog posts
 *
 * @package: Alimo
 * @version: 1.0.0
 */
window.alimo_background_audio = (function (window, document, $) {
    function init() {
        if ($('.single-article-audio-bg').length > 0) {
            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                window.alimo_cookie.setCookie('auto-play-audio', 'pause', '1');
            }
            var $this = $('.single-article-audio-bg');
            var audioTag = $('.single-article-audio-bg audio')[0],
                iconTag = $this.find('i');

            if ((window.alimo_cookie.getCookie('auto-play-audio') === 'play') || (true === audioTag.hasAttribute('autoplay'))) {
                setInterval(function () {
                    $("#count_num").html(function (i, html) {
                        if (parseInt(html) > 1) {
                            return parseInt(html) - 1;
                        } else {
                            $(this).parent().addClass('count-done');
                        }
                    });
                }, 1000);

            } else {
                $("#count_num").parent().addClass('count-done');
                $('.single-article-audio-bg i').toggleClass('fa-volume-up fa-volume-off');
            }
            if (window.alimo_cookie.getCookie('auto-play-audio') === 'play') {
                setTimeout(function () {
                    audioTag.play();
                }, 2000);
            }
            $this.click(function () {
                if (window.alimo_cookie.getCookie('auto-play-audio') === 'play') {
                    iconTag.toggleClass('fa-volume-up fa-volume-off');
                    audioTag.pause();
                    iconTag.parent().addClass('count-done');
                    window.alimo_cookie.setCookie('auto-play-audio', 'pause', '1');
                } else {
                    iconTag.toggleClass('fa-volume-up fa-volume-off');
                    audioTag.play();
                    window.alimo_cookie.setCookie('auto-play-audio', 'play', '1');
                }
            });
        }
    }
    return{
        init: init
    }
}(window, document, jQuery));