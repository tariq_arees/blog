/**
 * This component controls the WooCommerce classes added to the header minicart
 */
window.alimo_woocommerce = (function (window, document, $) {

    var cart = $('.cart-outer');
    var header = $('header.main-header');


    function init() {
        $('body').on('added_to_cart', function (event, rest_fragments, resp_cart_hash, add_btn) {
            var product = add_btn.closest('.product');
            var product_name = product.find('h2').html();
            cart.find('.cart-contents').addClass('cart-full');
            cart.find('.cart-notification').find('.item-name').html(product_name);
            cart.find('.cart-notification').addClass('visible').delay(2500).queue(function(next){
                $(this).removeClass('visible');
                next();
            });
            header.addClass('product-added').delay(2500).queue(function(next){
                $(this).removeClass('product-added');
                next();
            });
        });
        cart.hover(function () {
            header.toggleClass('minicart-open');
        });

    }

    return{
        init: init
    }
}(window, document, jQuery));