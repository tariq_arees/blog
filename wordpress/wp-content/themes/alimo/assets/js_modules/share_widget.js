/**
 * SHARE WIDGET
 *
 * This component controls the behaviour of the share widget on the single post
 *
 * @package: Alimo
 * @version: 1.0.0
 */

window.alimo_share_widget = (function (window, document, $) {

    var shareWidget = $('.share-widget');
    var articleTitle = $('.single-post-title');
    var singlePostFooter = $('.article-footer');

    function init() {
        if(articleTitle.length > 0)
        {
            var actualScroll = $(window).scrollTop(),
                titleOffset = articleTitle.offset().top,
                footerOffset = singlePostFooter.offset().top - window.innerHeight;

            if( titleOffset < actualScroll && actualScroll < footerOffset){
                shareWidget.addClass('fade-in');
            }
            else{
                shareWidget.removeClass('fade-in');
            }
        }
    }
    return {
        init:init
    }
}(window, document, jQuery));