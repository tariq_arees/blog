/**
 * READING PROGRESS
 *
 * This component enables the reading progress bar feature for posts
 *
 * @package: Alimo
 * @version: 1.0.0
 */

window.alimo_reading_progress = (function (window, document, $) {
    var readingProgress = $('.reading-position');
    var singlePostFooter = $('.article-footer');
    var mainHeader = jQuery('.main-header');
    var singlePostWrapper = jQuery('.single-article-featured-image');
    var adminON = jQuery('.customize-support').length > 0;


    function init() {

        if ( readingProgress.length > 0 ) {

            var actualScroll = $(window).scrollTop(),
                headerH = mainHeader.outerHeight(),
                windowH = singlePostWrapper.height() + headerH,
                singleArticleFooterOffset = singlePostFooter.offset(),
                singleArticleFooterOffsetTop = singleArticleFooterOffset.top,
                finalReadingBar = singleArticleFooterOffsetTop - windowH - headerH;

            if ( adminON ) {
                headerH = mainHeader.outerHeight() + 32;
            }

            if ( actualScroll >= finalReadingBar ) {
                readingProgress.css('width','100%');

            } else if ( actualScroll < finalReadingBar ) {

                if ( actualScroll >= windowH ) {

                    var totalRadingDistance = finalReadingBar - windowH,
                        totalRadingDistancePercent = totalRadingDistance / 100,
                        getBarPercent = (actualScroll -windowH ) / totalRadingDistancePercent;

                    readingProgress.css('width',getBarPercent + '%');

                } else {
                    readingProgress.css('width','0');
                }
            }
        }
    }
    return{
        init: init
    }
}(window, document, jQuery));