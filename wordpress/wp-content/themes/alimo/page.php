<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package alimo
 */

get_header();

$alimo_data = get_option('alimo_data');
$sidebar_enabled     = ( $alimo_data['enable-sidebar'] == true &&  is_active_sidebar('sidebar-1')  ) ? true : false;
$sidebar_always_open = ( $alimo_data['sidebar-always-open'] == true ) ? true : false;
$sidebar_position = ($alimo_data['sidebar-position'] == 'left') ? 'left-sidebar':'right-sidebar';

$content_class =  ( $sidebar_enabled ) ? ( $sidebar_always_open ) ? "col-sm-9 {$sidebar_position}" : "col-sm-12 col-md-9 center-block" : "col-sm-12 col-md-9 center-block";
?>
<div class="main-content">
	<div class="container">
		<div class="row">
			<?php while ( have_posts() ) : the_post(); ?>
            <div class="default-page page-content-wrapper <?php echo esc_attr($content_class);?>">
                <h1 class="page-title"><?php the_title() ?></h1>
                <?php the_content(); ?>
	            <?php // If comments are open or we have at least one comment, load up the comment template.
	            if ( comments_open() || get_comments_number() ) : ?>
                    <div class="row">
                        <div class="center-block col-md-8">
		                    <?php comments_template(); ?>
                        </div>
                    </div>
	            <?php endif; ?>
			</div>
			<?php endwhile; // End of the loop. ?>
            <?php ( $sidebar_enabled && $sidebar_always_open ) ? get_sidebar() : null; ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
