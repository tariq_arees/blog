<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package alimo
 */

$alimo_data          = get_option('alimo_data');
$sidebar_enabled     = (!empty($alimo_data['enable-sidebar'])) ? $alimo_data['enable-sidebar'] : FALSE;
$sidebar_always_open = (!empty($alimo_data['sidebar-always-open'])) ? $alimo_data['sidebar-always-open'] : FALSE;

$header_layout =  ($alimo_data['header_format']) ? $alimo_data['header_format'] : 'default_header';

function alimo_grayscale_class( $classes ) {
  $prefix = Alimo_Meta_Boxes::get_instance()->prefix;
  $post_grayscale = (function_exists('rwmb_meta')) ? rwmb_meta( "{$prefix}post_grayscale") : null;
  if( is_single() && $post_grayscale == '1'){
    $classes[] = 'black-white';
  }
  return $classes;
}
add_filter( 'body_class','alimo_grayscale_class' );

?>

<!DOCTYPE html>
<html <?php language_attributes(); if($alimo_data['smooth-transition']) echo ' class="smooth-transition"';?>>
<head>
	
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="HandheldFriendly" content="true" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) :
    $favicon = ( !empty($alimo_data['favicon']['url']) ) ? esc_url($alimo_data['favicon']['url']) : ALIMO_THEMEROOT.'/assets/img/header/favicon.png'; ?>
    <link rel="shortcut icon" href="<?php echo esc_url($favicon); ?>" />
<?php endif; ?>

<?php if( isset( $alimo_data['smooth-transition'] ) && $alimo_data['smooth-transition'] == true ) get_template_part('template-parts/smooth-transition'); ?>

<?php get_template_part('template-parts/typography'); ?>
<?php get_template_part('template-parts/color-palettes'); ?>
<?php is_single() ? get_template_part('template-parts/post-scripts') : null; ?>
<?php wp_head(); ?>
</head>
<body <?php body_class();?>>

<?php if( isset( $alimo_data['smooth-transition'] ) && $alimo_data['smooth-transition'] == true ) : ?>
    <div class="smooth-transition">
        <div class="spinner"></div>
        <div class="message"><?php echo (!empty($alimo_data['transition-message'])) ?  esc_attr($alimo_data["transition-message"]) : null;?></div>
    </div>
<?php endif; ?>
<?php get_template_part( 'template-parts/headers/'.$header_layout ); ?>

<?php /* if Sidebar always open is disabled in the backend load it here. Otherwise it gets loaded inside index.php */ ?>
<?php ($sidebar_enabled && $sidebar_always_open) ? null : get_sidebar(); ?>