/*
* Use this task with Gulp to rebuild main.min.js if you edit any of the JS Components from /assets/js_modules/
* */

var gulp    = require('gulp'),
    concat  = require('gulp-concat'),
    uglify  = require('gulp-uglify'),
    pump    = require('pump');

var config = {
  jsDist: './assets/js/',
  jsModules: './assets/js_modules/**/*.js'
};

gulp.task('js_modules', function () {
  pump([
        gulp.src([config.jsModules, config.jsDist + 'main.js']),
        concat('main.min.js'),
        uglify(),
        gulp.dest(config.jsDist)
      ]
  );
});