<?php
/**
 * Template for displaying search form
 */

$alimo_data = get_option('alimo_data');
$admin_sph = sanitize_text_field($alimo_data['search-placeholder']);
$search_placeholder = !empty($admin_sph) ? $admin_sph : esc_html__('Search and press Enter', 'alimo');

$form = '<form role="search" method="get" class="search-form" action="' . esc_url( home_url( '/' ) ) . '">
            <div class="input-group">
                <input type="search" class="search-field" placeholder="' . esc_attr($search_placeholder) . '" value="' . get_search_query() . '" name="s" />
                <span class="input-group-btn">
                    <button class="btn btn-default search-submit" type="button"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>';

echo $form;