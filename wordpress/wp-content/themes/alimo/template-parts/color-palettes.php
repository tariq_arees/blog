<?php
/**
 * Template part for color palettes selected or defined in backend
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package alimo
 */



function alimo_custom_accent_styles() {
	$alimo_data = get_option( 'alimo_data' );

	$custom_accent         = ( ! empty( $alimo_data['custom_accent_color'] ) ) ? $alimo_data['custom_accent_color'] : '#F0C';
	$custom_accent_escaped = esc_attr( $custom_accent );

	$menu_color_escaped = ( ! empty( $alimo_data['menu-font']['color'] ) ) ? esc_attr( $alimo_data['menu-font']['color'] ) : NULL;
	$output ="";
	if ( $alimo_data['accent_color'] == 'accent_custom' ):
        $output = "
      
        ::selection {
        background-color: ". $custom_accent_escaped ."; }

        .form-control:hover, .form-control:active, .form-control:focus,
        select:hover, select:active, select:focus,
        input[type=text]:hover, input[type=text]:active, input[type=text]:focus,
        input[type=email]:hover, input[type=email]:active, input[type=email]:focus,
        input[type=search]:hover, input[type=search]:active, input[type=search]:focus,
        input[type=tel]:hover, input[type=tel]:active, input[type=tel]:focus,
        textarea:hover, textarea:active, textarea:focus,
        .post-password-form input[type=password]:hover,
        .post-password-form input[type=password]:active,
        .post-password-form input[type=password]:focus,
        .cart-collaterals .input-text:hover,
        .cart-collaterals .input-text:active,
        .cart-collaterals .input-text:focus,
        .create-account .input-text:hover,
        .create-account .input-text:active,
        .create-account .input-text:focus,
        .login .input-text:hover,
        .login .input-text:active,
        .login .input-text:focus {
        border-color: ". $custom_accent_escaped."; }
        
        .btn,
		input[type=submit],
		button[type=submit] {
		background: ". hex2rgba( $custom_accent_escaped, 0.8 )."}

        .btn:hover,
		input[type=submit]:hover,
		button[type=submit]:hover,
		.btn:active,
		input[type=submit]:active,
		button[type=submit]:active {
        background: ". $custom_accent_escaped."; }
        
        .button.add_to_cart_button[data-product_id],
		.woocommerce input.button[type=submit]{}
		
		.button.add_to_cart_button[data-product_id]:hover,
		.woocommerce input.button[type=submit]:hover{
		color: ". $custom_accent_escaped."; 
		border-color: ". $custom_accent_escaped."; }

        a {
        color: ". $custom_accent_escaped."; }

        .widget .social-profiles a:hover {
        color: ". $custom_accent_escaped."; }

        .widget.widget_recent_entries ul li a .centred-article span span {
        color: ". $custom_accent_escaped."; }

        .widget ul li a:hover {
        color: ". $custom_accent_escaped."; }
        .widget .post_count{
        color: ". $custom_accent_escaped.";
        }
        .instagram-follow .instagram-carousel a:after {
        background: ". $custom_accent_escaped."; }

        .instagram-follow .follow-instagram-button {
        background: ". $custom_accent_escaped."; }

        .main-footer .footer-share ul li a {
        background: ". $custom_accent_escaped."; }

        .contact-container .contact-info ul li a {
        background: ". $custom_accent_escaped."; }


        .comment-form-theme textarea:hover,
        .comments .comment-respond textarea:hover,
        .comment-form-theme textarea:active,
        .comments .comment-respond textarea:active,
        .comment-form-theme textarea:focus,
        .comments .comment-respond textarea:focus {
        border-color: ". $custom_accent_escaped."; }

        .comment-container ul.comments li .comment .right-section a.comment-reply-link:hover, .comment-container ul.comments li .pingback .right-section a.comment-reply-link:hover {
        border-color: ". $custom_accent_escaped.";
        color: ". $custom_accent_escaped."; }
        .comment-container ul.comments li .comment .right-section a.comment-reply-link:hover i, .comment-container ul.comments li .pingback .right-section a.comment-reply-link:hover i {
        color: ". $custom_accent_escaped."; }
        .comment-container ul.comments li .comment .right-section .comment-author a:hover, .comment-container ul.comments li .pingback .right-section .comment-author a:hover {
        color:  ". $custom_accent_escaped."; }

        .articles-container article .left-article-side .bottom-side .read-more a {
        color: ". $custom_accent_escaped."; }

        .articles-container article .left-article-side .social-article-share li a:hover {
        color: ". $custom_accent_escaped."; }

        .share-widget a:hover {
        color: ". $custom_accent_escaped."; }

        .share-widget .like:hover i, .share-widget .like:hover a {
        color: ". $custom_accent_escaped."; }

        .post-content li:before {
        color: ". $custom_accent_escaped."; }

        @media screen and (max-width: 768px) {
        .articles-container article .left-article-side .bottom-side .read-more a {
        color: white;
        border-color: ". $custom_accent_escaped.";
        background-color: ". $custom_accent_escaped."; } }

        .articles-container article .left-article-side a:hover {
        color: ". $custom_accent_escaped."; }

        .articles-container article .left-article-side .bottom-side .like i{
        color: ". $custom_accent_escaped."; }

        .articles-container article .featured i, .articles-container-masonry article .featured i,
        .articles-container-interlaced article .featured i, .articles-container-grid article .featured i,
        .post-content-wrapper .featured i{
        color: ". $custom_accent_escaped."; }

        .articles-container .sticky-post i, .articles-container-masonry .sticky-post i,
        .articles-container-interlaced .sticky-post i, .articles-container-grid .sticky-post i,
        .post-content-wrapper .sticky-post i{
        color: ". $custom_accent_escaped."; }

        .articles-container article .left-article-side .bottom-side .like i,
        article .featured i {
        color: ". $custom_accent_escaped."; }

        .author-profile .contact-methods .social-icons li a:hover {
        color: ". $custom_accent_escaped."; }

        article .author-details a:hover {
        color: ". $custom_accent_escaped."; }
        article .author-name:hover {
		color: ". $custom_accent_escaped."; }

        .articles-container article header h2 a:hover,
        .articles-container-masonry article header h2 a:hover,
        .articles-container-interlaced article header h2 a:hover,
        .articles-container-grid article header h2 a:hover {
        color: ". $custom_accent_escaped."; }

        .articles-container article .article-entries .read-more a,
        .articles-container-masonry article .article-entries .read-more a,
        .articles-container-interlaced article .article-entries .read-more a,
        .articles-container-grid article .article-entries .read-more a {
        color: ". $custom_accent_escaped."; }

        .articles-container article footer .post-meta .featured:hover, .articles-container article footer .post-meta .comments:hover, .articles-container article footer .post-meta .like:hover, .articles-container article footer .post-meta .share:hover,
        .articles-container-masonry article footer .post-meta .featured:hover,
        .articles-container-masonry article footer .post-meta .comments:hover,
        .articles-container-masonry article footer .post-meta .like:hover,
        .articles-container-masonry article footer .post-meta .share:hover,
        .articles-container-interlaced article footer .post-meta .featured:hover,
        .articles-container-interlaced article footer .post-meta .comments:hover,
        .articles-container-interlaced article footer .post-meta .like:hover,
        .articles-container-interlaced article footer .post-meta .share:hover,
        .articles-container-grid article footer .post-meta .featured:hover,
        .articles-container-grid article footer .post-meta .comments:hover,
        .articles-container-grid article footer .post-meta .like:hover,
        .articles-container-grid article footer .post-meta .share:hover {
        color: ". $custom_accent_escaped."; }

        .footer-share .like a:hover, .footer-share .like > i:hover {
        color: ". $custom_accent_escaped."; }

        .footer-share .social-article-share li a:hover {
        color: ". $custom_accent_escaped."; }
        .tooltip.top .tooltip-arrow{
        border-top-color: ". $custom_accent_escaped.";}
        .tooltip .tooltip-inner{
        background-color: ". $custom_accent_escaped.";}

        article .like:hover > a, article .featured:hover > a, article .comments:hover > a, article .share:hover > a {
        color: ". $custom_accent_escaped."; }

        ul.share-dd li a:hover {
        color: ". $custom_accent_escaped."; }

        .posts-navigation .prev-post:hover .post-title, .posts-navigation .next-post:hover .post-title {
        color: ". $custom_accent_escaped."; }

        .single-article-featured-image .single-post-top-author-info figure figcaption a:hover {
        color: ". $custom_accent_escaped."; }

        .single-article-featured-image .contact-media ul li a {
        background: ". $custom_accent_escaped."; }

        .single-post .main-article .post-meta .comment-count-link:hover{
        color: ". $custom_accent_escaped.";}

        .drop-cap:first-letter {
        border-color: ". $custom_accent_escaped."; }

        blockquote:before,
        q:before {
        background-color: ". $custom_accent_escaped."; }

        .author-social-top ul li a {
        color: ". $custom_accent_escaped."; }

        .reading-mode:hover i {
        color: ". $custom_accent_escaped."; }

        .reading-mode-container .reading-options .reading-options-container .ff-options .font-box.active,
        .reading-mode-container .reading-options .reading-options-container .ff-options .font-box:active,
        .reading-mode-container .reading-options .reading-options-container .cs-options .cs-option.active,
        .reading-mode-container .reading-options .reading-options-container .cs-options .cs-option:active {
        box-shadow: inset 0 -3px 0 ". $custom_accent_escaped.";
        color: ". $custom_accent_escaped."; }

        .reading-position {
        background: ". $custom_accent_escaped."; }
		a.alimo_social_profile:hover{
	    color: ". $custom_accent_escaped."; }
        .main-header .social-profiles ul li a:hover {
        color: ". $custom_accent_escaped."; }

        .main-header .main-search button {
        background: ". $custom_accent_escaped."; }

        .main-header .search-container.hovered .search-trigger-bg, .main-header .search-container:hover .search-trigger-bg {
        background: ". $custom_accent_escaped."; }

        .main-nav .header-menu .main-menu li a:before {
        background: ". $custom_accent_escaped."; }

        .main-nav .header-menu .main-menu li.current-menu-item > a {
        color: ". $custom_accent_escaped."; }
        .main-nav .header-menu .main-menu li:hover > a {
        color: ". $custom_accent_escaped."; }

        .main-nav .header-menu > li:hover > a {
        color: ". $custom_accent_escaped."; }

        .main-nav .header-menu > li ul > li:hover > a {
        color: ". $custom_accent_escaped."; }
		.main-nav .header-menu .main-menu li.current-menu-item > .toggle-submenu {
		  color: ". $custom_accent_escaped."; }
		.main-nav .header-menu .main-menu li:hover > .toggle-submenu {
		  color: ". $custom_accent_escaped."; }
        .posts-pagination .page-numbers.current,
        .posts-pagination .page-numbers:focus,
        .posts-pagination .page-numbers:active {
        border-color: ". $custom_accent_escaped.";
        color: ". $custom_accent_escaped."; }

        .posts-pagination .page-numbers:hover {
        border-color: ". hex2rgba( $custom_accent_escaped, 0.3 ).";
        color: ". $custom_accent_escaped.";}

        .posts-pagination .page-numbers.current:hover {
        border-color: ". $custom_accent_escaped."; }

        .glitch-fof {
        color: ". $custom_accent_escaped."; }
        .glitch-fof:before {
        text-shadow: 2px 0 #ff008b; }
        .glitch-fof:after {
        text-shadow: -1px 0 #f8dd00; }

        .woocommerce a.remove:hover {
        background-color: ". $custom_accent_escaped."; }

        .woocommerce span.onsale {
        background-color: ". $custom_accent_escaped."; }

        .woocommerce .product .entry-summary .price ins{
        color: ". $custom_accent_escaped.";
        }
        .woocommerce .star-rating span{
        color: ". $custom_accent_escaped.";
        }

        .woocommerce button.button.alt,
        .woocommerce input.button.alt {
        border-color: ". $custom_accent_escaped."; 
        background-color: ". $custom_accent_escaped."; }
        .woocommerce button.button.alt:hover,
        .woocommerce input.button.alt:hover {
        color: white;
        background-color: ". $custom_accent_escaped."; }

		.woocommerce .widget_price_filter .ui-slider .ui-slider-handle {
		background-color: ". $custom_accent_escaped."; }
		
		.woocommerce .widget_price_filter .ui-slider .ui-slider-range {
		background-color: ". $custom_accent_escaped."; }

        .cart-outer .cart-contents .product-count-badge {
        background-color: ". $custom_accent_escaped."; }

        .cart-outer .mini-cart .buttons .button:hover {
        color: ". $custom_accent_escaped."; }
        
        .cart-outer .mini-cart .buttons .checkout{
        background-color: ". hex2rgba( $custom_accent_escaped, 0.8 )." ; }
        
        .cart-outer .mini-cart .buttons .checkout:hover {
        color: white;
        background-color: ". $custom_accent_escaped."; }
        
        .cart-outer:hover .cart-icon-wrap i svg {
		fill: ". $custom_accent_escaped."; }
        
        .mini-cart .product-name:hover, .widget_shopping_cart .product-name:hover {
		color: ". $custom_accent_escaped."; }
		
		#add_payment_method table.cart td.product-name a:hover,
		.woocommerce-cart table.cart td.product-name a:hover,
		.woocommerce-checkout table.cart td.product-name a:hover {
		color: ". $custom_accent_escaped."; }
		
		.woocommerce .woocommerce-breadcrumb a:hover {
		color: ". $custom_accent_escaped."; }
		
		.woocommerce .product .entry-summary .cart button[type=submit]:hover i svg {
		fill: ". $custom_accent_escaped."; }
		
		.woocommerce span.onsale {
		background-color: ". $custom_accent_escaped."; }
		
		.woocommerce .products .product a:hover {
		color: ". $custom_accent_escaped."; }
		
		.woocommerce .woocommerce-pagination li .page-numbers.current, .woocommerce .woocommerce-pagination li .page-numbers:focus, .woocommerce .woocommerce-pagination li .page-numbers:active {
		  border-color: ". $custom_accent_escaped.";
		  color: ". $custom_accent_escaped."; }
		
		.woocommerce .woocommerce-pagination li .page-numbers.current:hover {
		  border-color: ". $custom_accent_escaped."; }
		
		.woocommerce .woocommerce-pagination li .page-numbers:hover {
		  border-color: ". hex2rgba( $custom_accent_escaped, 0.3 ).";
		  color: ". $custom_accent_escaped."; }
		.woocommerce .product .woocommerce-tabs.wc-tabs-wrapper ul.tabs li.active{
		    border-color:". $custom_accent_escaped."; }
		.woocommerce #respond input#submit.alt,
		.woocommerce a.button.alt,
		.woocommerce a.button.checkout,
		.woocommerce button.button.alt,
		.woocommerce input.button.alt {
		  background-color: ". hex2rgba( $custom_accent_escaped, 0.8 )."; }
		  .woocommerce #respond input#submit.alt:hover,
		  .woocommerce a.button.alt:hover,
		  .woocommerce a.button.checkout:hover,
		  .woocommerce button.button.alt:hover,
		  .woocommerce input.button.alt:hover {
		    background-color: ". $custom_accent_escaped.";}
		  .woocommerce #respond input#submit.alt.disabled, .woocommerce #respond input#submit.alt.disabled:hover,
		  .woocommerce a.button.alt.disabled,
		  .woocommerce a.button.alt.disabled:hover,
		  .woocommerce button.button.alt.disabled,
		  .woocommerce button.button.alt.disabled:hover,
		  .woocommerce input.button.alt.disabled,
		  .woocommerce input.button.alt.disabled:hover {
		    background-color: ". hex2rgba( $custom_accent_escaped, 0.3 )."; }
		  
		.woocommerce #respond input#submit:hover,
		.woocommerce a.button:hover,
		.woocommerce button.button:hover,
		.woocommerce input.button:hover {
		  color: ". $custom_accent_escaped."; }

        @media (max-width: 768px) {
        .main-nav .phone-menu-trigger:hover .menu-bar,
        .main-nav .phone-menu-trigger:focus .menu-bar,
        .main-nav .phone-menu-trigger:active .menu-bar {
        border-color: ". $custom_accent_escaped."; } }";

	endif;

	if ( ! empty( $menu_color_escaped ) ) :
        $output .="
        .main-header .social-profiles ul li a,
        .main-header .site-title a,
        .main-header .search-container .search-open{
        color : ". $menu_color_escaped.";
        }
        .cart-icon-wrap svg{
        fill :". $menu_color_escaped.";
        }
        .main-nav .menu-bar{
        border-color: ". $menu_color_escaped.";
        }";
	 endif;

	wp_add_inline_style( 'style', $output );

}

add_action( 'wp_enqueue_scripts', 'alimo_custom_accent_styles' );