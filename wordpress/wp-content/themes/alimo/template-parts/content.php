<?php
/**
 * Template part for displaying standard posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package alimo
 */
global $post;
$alimo_data = get_option('alimo_data');
/**
 * Get post custom settings like featured posts flag, if it has a video set or featured image replacement
 */
$prefix              = Alimo_Meta_Boxes::get_instance()->prefix;
$featured_post       = (function_exists('rwmb_meta')) ? rwmb_meta( "{$prefix}featured_post" ) : null;
$post_video_provider = (function_exists('rwmb_meta')) ? rwmb_meta( "{$prefix}video_provider" ) : null;
$post_video_id       = (function_exists('rwmb_meta')) ? rwmb_meta( "{$prefix}video_id" ) : null;

$featured_image_replacement = null;
$is_gif = FALSE;
$uploaded_image = array();
if(function_exists('rwmb_meta')){
    if(is_array(rwmb_meta( "{$prefix}featured_image_replacement" ))){
        $uploaded_image = current( rwmb_meta( "{$prefix}featured_image_replacement", array(),$post->ID) );
	    $uploaded_image_url = $uploaded_image['full_url'];
        $uploaded_image_ext = wp_check_filetype($uploaded_image_url);
        if('gif' == $uploaded_image_ext['ext'] ){
            $is_gif = TRUE;
	        $featured_image_replacement = current( rwmb_meta( "{$prefix}featured_image_replacement", array('size'=>'full') ) );
        }else{
	        $is_gif = FALSE;
	        $featured_image_replacement = current( rwmb_meta( "{$prefix}featured_image_replacement", array('size'=>'alimo-featured-image') ) );
        }
    }
}
$featured_image_replacement_url = (!empty($featured_image_replacement['url'])) ? $featured_image_replacement['url'] : null;
/**
 * Check if we have a featured image replacement set.
 * also, check if there's a video id set
 * if no image or image replacement is found, use the thumbnail from Vimeo or Youtube
 */
$featured_image = ( ! empty( $featured_image_replacement_url ) ) ? $featured_image_replacement_url : null;
if ( empty( $featured_image ) && $post_video_id ) {
	$featured_image = alimo_get_video_thumbnail( $post_video_id, $post_video_provider );
}
$content_class = '';
/**
 * Check if the fade in animation is set to true
 * Alimo Options > Design > Listing layouts > Animation
 */
if(!empty($alimo_data['list-animation']) && true == $alimo_data['list-animation']){
  $content_class = ' animate-in';
}
?>

<article <?php post_class($content_class); ?>>
    <div class="row">
        <div class="col-md-9 col-sm-7 col-xs-12">
            <header>
				<?php if ( $alimo_data['list-categories'] ) : ?>
                    <figure>
                        <figcaption>
                            <div class="categories-list clearfix">
                              <?php if (is_sticky()): ?>
                                  <span class="sticky-post" data-toggle="tooltip" data-placement="top" title="Sticky">
                                      <i class="fa fa-thumb-tack" aria-hidden="true"></i>
                                  </span>
                              <?php endif; ?>
                              <?php if ($featured_post == '1'): ?>
                                  <span class="featured" data-toggle="tooltip" data-placement="top" title="Featured">
                                        <i class="fa fa-bookmark"></i>
                                    </span>
                              <?php endif; ?>
                              <?php echo get_the_category_list(); ?>
                            </div>
                        </figcaption>
                    </figure>
				<?php endif; ?>
                <h2>
                  <?php if (is_sticky() && !$alimo_data['list-categories']) : ?>
                      <span class="sticky-post" data-toggle="tooltip" data-placement="top" title="Sticky">
                          <i class="fa fa-thumb-tack" aria-hidden="true"></i>
                      </span>
                  <?php endif; ?>
                  <?php if ($featured_post == '1' && !$alimo_data['list-categories']) : ?>
                      <span class="featured" data-toggle="tooltip" data-placement="top" title="Featured">
                          <i class="fa fa-bookmark"></i>
                      </span>
                  <?php endif; ?>
                    <a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a>
                </h2>
				<?php
				if ( $alimo_data['list-author'] OR $alimo_data['list-date'] ): ?>
                    <div class="author-details visible-xs">
						<?php if ( $alimo_data['list-author'] ):
							$author_url = get_site_url() . '/author/' . get_the_author_meta( 'user_login' ); ?>
                            <span><?php esc_html_e( 'Posted by ', 'alimo' ); ?></span>
                            <a class="author-photo" href="<?php echo esc_url($author_url); ?>">
								<?php echo get_avatar( get_the_author_meta( 'ID' ), 100 ); ?>
                            </a>
                            <a class="author-name" href="<?php echo esc_url($author_url); ?>">
								<?php the_author(); ?>
                            </a>
						<?php endif;
                        // Post date
						if ( $alimo_data['list-date'] ) : ?>
                            <span><?php esc_html_e( 'on ', 'alimo' ); ?></span>
                            <a class="post-date" href="<?php echo get_site_url().'/'.get_the_date( 'Y/m/d' ); ?>/"><?php echo alimo_relative_time(); ?></a>
						<?php endif; ?>
                    </div>
				<?php endif; ?>
            </header>
			<?php if ( $featured_image OR has_post_thumbnail() ): ?>
                <div class="featured-image">
                    <a href="<?php echo get_the_permalink(); ?>">
	                    <?php   if (has_post_thumbnail() && empty($featured_image_replacement_url) )
	                    {
		                    $default_featured_image_url = wp_get_attachment_url( get_post_thumbnail_id() );
		                    $attachment_filetype = wp_check_filetype($default_featured_image_url);
                            ('gif' == $attachment_filetype['ext']) ? the_post_thumbnail( 'full' ) : the_post_thumbnail( 'alimo-featured-image' );
                        }
                        else
                        { if($is_gif)
	                        {?>
		                        <img src="<?php echo esc_url($featured_image);?>" alt="">
                        <?php }
                            else
                                { ?>
                                    <img src="<?php echo esc_url($featured_image);?>" alt=""
                                         srcset="<?php echo esc_attr($featured_image_replacement['srcset']); ?>"
                                         sizes="(max-width: 767px) 100vw, (max-width: 1920px) 50vw, 2000px" >
                        <?php   }
                        }  ?>
                    </a>
                </div>
			<?php endif; ?>
            <div class="article-entries">
                <div class="article-content">
                    <?php the_excerpt(); ?>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-5 col-xs-12">
            <div class="left-article-side clearfix">
				<?php
				if ( $alimo_data['list-author'] OR $alimo_data['list-date'] ): ?>
                    <div class="author-details hidden-xs">
						<?php
						if ( $alimo_data['list-author'] ):
							$author_url = get_site_url() . '/author/' . get_the_author_meta( 'user_login' ); ?>
                            <a class="author-photo" href="<?php echo esc_url($author_url); ?>">
								<?php echo get_avatar( get_the_author_meta( 'ID' ), 100 ); ?>
                            </a>
                            <a class="author-name" href="<?php echo esc_url($author_url); ?>">
								<?php the_author(); ?>
                            </a>
						<?php endif;
                        // Post date
                        if ( $alimo_data['list-date'] ): ?>
                            <a class="post-date hidden-xs" href="<?php echo get_site_url() . '/'. get_the_date( 'Y/m/d' ); ?>/"> <?php echo alimo_relative_time(); ?></a>
						<?php endif; ?>
                    </div>
				<?php endif; ?>
                <?php $post_sharing_networks  = array_filter($alimo_data['post_sharing_networks']); ?>
				<?php ( $alimo_data['list-social'] && !empty($post_sharing_networks) ) ? get_template_part( 'template-parts/sharing-options', 'list' ) : null; ?>

				<?php // Post tags ?>
				<?php if ( get_the_tag_list() && $alimo_data['list-tags'] ): ?>
                    <div class="tags">
						<?php echo get_the_tag_list(); ?>
                    </div>
				<?php endif; ?>

				<?php // Likes ?>
				<?php if ( function_exists( 'zilla_likes' ) && $alimo_data['list-likes'] && $alimo_data['enable_post_likes'] ): ?>
                    <div class="like col-sm-12 col-xs-6"><?php zilla_likes(); ?></div>
				<?php endif; ?>

				<?php // Post comments ?>
				<?php $comments_number = get_comments_number(); ?>
				<?php if ( $comments_number > 0 && $alimo_data['list-comments'] ): ?>
                    <div class="comments col-sm-12 col-xs-6">
                        <a href="<?php echo get_the_permalink(); ?>#the-comment">
                            <i class="fa fa-comment-o"></i>
                            <span><?php comments_number(esc_html__('No Comments', 'alimo'), esc_html__('1 Comment', 'alimo'), esc_html__('% Comments', 'alimo')); ?></span>
                        </a>
                    </div>
				<?php endif; ?>

                <div class="bottom-side col-sm-12 col-xs-6 no-padding">
					<?php if ( $alimo_data['list-reading-time'] ): ?>
                        <div class="read-time">
                            <i class="fa fa-clock-o"></i>
                            <span><?php echo alimo_reading_time( $post ) . ' ';
								esc_html_e( 'Min read', 'alimo' ); ?></span>
                        </div>
					<?php endif; ?>
                    <div class="read-more hidden-xs">
                        <a href="<?php echo get_the_permalink(); ?>">
							<?php esc_html_e( 'read now', 'alimo' ); ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</article>

