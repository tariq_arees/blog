<?php
/**
 * Template part for displaying social media icons tooltip on share button.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package alimo
 */
$alimo_data = get_option('alimo_data');
?>
<ul class="share-dd">
<?php if($alimo_data['post_sharing_networks']['facebook'] == 1) { ?>
    <li>
    	<a href="http://www.facebook.com/sharer.php?u=<?php echo get_permalink($post->ID); ?>" target="_blank">
    		<i class="fa fa-facebook-f"></i>
    	</a>
    </li>
<?php } ?>
<?php if($alimo_data['post_sharing_networks']['twitter'] == 1) { ?>
    <li>
    	<a href="https://twitter.com/share?url=<?php echo get_permalink($post->ID); ?>&amp;text=<?php echo htmlspecialchars(urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8')), ENT_COMPAT, 'UTF-8');;?>&amp;hashtags=<?php echo htmlspecialchars(urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8')), ENT_COMPAT, 'UTF-8');;?>" target="_blank">
    		<i class="fa fa-twitter"></i>
    	</a>
    </li>
<?php } ?>
<?php if($alimo_data['post_sharing_networks']['gplus'] == 1) { ?>
    <li>
    	<a href="https://plus.google.com/share?url=<?php echo get_permalink($post->ID); ?>" target="_blank">
    		<i class="fa fa-google"></i>
    	</a>
    </li>
<?php } ?>
<?php if($alimo_data['post_sharing_networks']['linkedin'] == 1) { ?>
    <li>
    	<a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo get_permalink($post->ID); ?>" target="_blank">
    		<i class="fa fa-linkedin"></i>
    	</a>
    </li>
<?php } ?>
<?php if($alimo_data['post_sharing_networks']['pinterest'] == 1) { ?>
    <li>
    	<a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());" target="_blank">
    		<i class="fa fa-pinterest"></i>
    	</a>
    </li>
<?php } ?>
<?php if($alimo_data['post_sharing_networks']['reddit'] == 1) { ?>
    <li>
    	<a href="http://reddit.com/submit?url=<?php echo get_permalink($post->ID); ?>&amp;title=<?php echo htmlspecialchars(urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8')), ENT_COMPAT, 'UTF-8');;?>" target="_blank">
    		<i class="fa fa-reddit"></i>
    	</a>
    </li>
<?php } ?>
<?php if($alimo_data['post_sharing_networks']['stumble'] == 1) { ?>
    <li>
    	<a href="http://www.stumbleupon.com/submit?url=<?php echo get_permalink($post->ID); ?>&amp;title=<?php echo htmlspecialchars(urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8')), ENT_COMPAT, 'UTF-8');;?>" target="_blank">
    		<i class="fa fa-stumbleupon"></i>
    	</a>
    </li>
<?php } ?>
<?php if($alimo_data['post_sharing_networks']['tumblr'] == 1) { ?>
    <li>
    	<a href="http://www.tumblr.com/share/link?url=<?php echo get_permalink($post->ID); ?>&amp;title=<?php echo htmlspecialchars(urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8')), ENT_COMPAT, 'UTF-8');;?>" target="_blank">
    		<i class="fa fa-tumblr"></i>
    	</a>
    </li>
<?php } ?>
<?php if($alimo_data['post_sharing_networks']['vk'] == 1) { ?>
    <li>
    	<a href="http://vkontakte.ru/share.php?url=<?php echo get_permalink($post->ID); ?>" target="_blank">
    		<i class="fa fa-vk"></i>
    	</a>
    </li>
<?php } ?>
</ul>