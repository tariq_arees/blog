<?php
/**
 * Template part for Header type 3 selected in backend
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package alimo
 */

$alimo_data = get_option('alimo_data');

$display_search      = (!empty($alimo_data['display_search']) && $alimo_data['display_search'] == 1) ? TRUE : FALSE;
$admin_sph           = sanitize_text_field($alimo_data['search-placeholder']);
$search_placeholder  = (!empty($admin_sph)) ? $admin_sph : esc_html__('Search and press Enter', 'alimo');
$site_title          = esc_html(get_bloginfo('name'));
$site_description    = esc_html(get_bloginfo('description'));
$logo_alimo          = (!empty($alimo_data['logo-alimo'])) ? $alimo_data['logo-alimo']['url'] : FALSE;
$logo_text           = (!empty($alimo_data['logo-text'])) ? $alimo_data['logo-text'] : $site_description;
$logo_size           = (!empty($alimo_data['logo-size'])) ? $alimo_data['logo-size'] : NULL;
$header_social_icons = (!empty($alimo_data['header-social'])) ? array_filter($alimo_data['header-social']) : NULL;
$header_gradient     = (!empty($alimo_data['header_gradient'])) ? $alimo_data['header_gradient'] : NULL;
$boxed_header        = (!empty($alimo_data['header_boxed']) && TRUE == $alimo_data['header_boxed']) ? 'boxed-header' : NULL;
$gradient_from       = $gradient_to = $header_gradient_angle = NULL;
if (!empty($header_gradient)) {
  $gradient_from         = (!empty($header_gradient['from'])) ? $header_gradient['from'] : NULL;
  $gradient_to           = (!empty($header_gradient['to'])) ? $header_gradient['to'] : NULL;
  $header_gradient_angle = (!empty($alimo_data['header_gradient_angle']) OR 0 == $alimo_data['header_gradient_angle']) ? $alimo_data['header_gradient_angle'] : NULL;

  if (!empty($gradient_from) OR !empty($gradient_to)) {
    if (!empty($gradient_from) AND !empty($gradient_to)) {
      if (!empty($header_gradient_angle) OR $header_gradient_angle == 0) {
        $header_style = "background-image:linear-gradient({$header_gradient_angle}deg, {$gradient_from}, {$gradient_to} )";
      }
      else {
        $header_style = "background-image:linear-gradient(to right, {$gradient_from},{$gradient_to})";
      }
    }
    if (!empty($gradient_from) && empty($gradient_to)) {
      $header_style = "background-color:{$gradient_from}";
    }
    if (!empty($gradient_to) && empty($gradient_from)) {
      $header_style = "background-color:{$gradient_to}";
    };
  }
}
?>
<header class="main-header visible-sidebar <?php echo esc_html($boxed_header)?>" <?php if(!empty($header_style)) echo 'style="' .esc_attr($header_style).'"'; ?> >
    <div <?php if(!empty($boxed_header)) echo 'class="container"'; ?> >
        <div class="main-logo <?php echo esc_attr($logo_size); ?>">
            <?php if( is_front_page() && is_home()): ?>
                <?php if($logo_alimo): ?>
                    <h1 class="site-title">
                        <a href="<?php echo get_site_url(); ?>">
                            <img src="<?php echo esc_url($logo_alimo); ?>" alt="<?php echo wp_strip_all_tags($logo_text); ?>">
                        </a>
                    </h1>
                <?php else: ?>
                    <h1 class="site-title no-logo">
                        <a href="<?php echo get_site_url(); ?>"><?php echo esc_html($site_title); ?></a>
                    </h1>
                <?php endif; ?>
            <?php else: ?>
                <?php if($logo_alimo): ?>
                    <div class="site-title">
                        <a href="<?php echo get_site_url(); ?>">
                            <img src="<?php echo esc_url($logo_alimo); ?>" alt="<?php echo wp_strip_all_tags($logo_text); ?>">
                        </a>
                    </div>
                <?php else: ?>
                    <div class="site-title no-logo">
                        <a href="<?php echo get_site_url(); ?>"><?php echo esc_html($site_title); ?></a>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <nav class="main-nav centered">
            <div class="phone-menu-trigger">
                <span class="menu-bar"></span>
                <span class="menu-bar"></span>
                <span class="menu-bar"></span>
            </div>
            <div class="header-menu">
                <?php
                if(has_nav_menu('header_nav')){
                    wp_nav_menu(
                        array(
                            'theme_location'    => 'header_nav',
                            'menu_class'             => 'main-menu',
                            'container'         => '',
                            'fallback_cb'       => false,
                            'walker' => new Alimo_Arrow_Walker_Nav_Menu()
                        ));
                }
                else {
                  wp_nav_menu(array(
	                  'menu_class'     => 'main-menu',
	                  'container'      => '',
	                  'fallback_cb'    => false,
	                  'walker' => new Alimo_Arrow_Walker_Nav_Menu()
                  ));
                }
                ?>
                <?php if(isset($header_social_icons) && class_exists('Alimo_Shortcode_Social_Profile') ): ?>
                    <div class="social-profiles">
                        <ul class="social-icons">
                            <?php
                            foreach($header_social_icons as $icon => $value) {
                                echo '<li>';
                                $icon_shortcode = '[alimo_social_profile network="'.$icon.'" /]';
                                echo do_shortcode($icon_shortcode);
                                echo '</li>';
                            }?>
                        </ul>
                    </div>
                <?php endif; ?>
            </div>
        </nav>
        <?php
        global $woocommerce;
        if($woocommerce):
            $products_in_cart = $woocommerce->cart->cart_contents_count;
            ?>
            <div class="cart-outer">
                <div class="cart-menu-wrap">
                    <div class="cart-menu">
                        <a class="cart-contents <?php echo ($products_in_cart > 0)? 'cart-full': 'cart-empty'; ?>" href="<?php echo esc_url(wc_get_cart_url()); ?>">
                            <div class="cart-icon-wrap">
                                <i class="lbli lbli-cart-o">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16.782 16.34" width="16.34">
                                        <path id="cart" d="M12.581 16.34H4.202a1.254 1.254 0 0 1-.8-.509C1.96 14.246-.171 9.125.009 7.123c.208-2.266 1.1-2.514 2.955-2.514a4.876 4.876 0 0 1 1.733-3.271 5.775 5.775 0 0 1 7.392 0 4.872 4.872 0 0 1 1.73 3.271c1.852 0 2.747.248 2.953 2.514.182 2-1.948 7.123-3.394 8.707a1.255 1.255 0 0 1-.797.51zM4.192 6.289c-1.434 0-2.127.177-2.286 1.8a7.783 7.783 0 0 0 .786 3.084A11.784 11.784 0 0 0 4.437 14.2c.28.309.519.465.712.465h6.486c.192 0 .432-.156.711-.465a11.777 11.777 0 0 0 1.746-3.032 7.786 7.786 0 0 0 .785-3.085c-.158-1.618-.85-1.8-2.285-1.8zm4.2-4.613a3.821 3.821 0 0 0-3.824 2.933h7.649a3.817 3.817 0 0 0-3.824-2.933z"/>
                                    </svg>
                                </i>
                                <div class="product-count-badge"><span><?php echo esc_html($products_in_cart); ?> </span></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="cart-notification">
                    <span class="item-name"></span> <?php echo esc_html__('was successfully added to your cart.', 'alimo'); ?>
                </div>

                <?php
                $args = array(
                    'before_widget' => '<div class="mini-cart widget %s">',
                    'after_widget' => '</div>',
                );
                the_widget( 'WC_Widget_Cart', array(), $args);?>

            </div>
        <?php endif; ?>
    </div>
    <?php if($display_search): ?>
        <div class="search-container">
            <div class="search-trigger-bg"></div>
            <i class="search-open fa fa-search"></i>
            <i class="search-close fa fa-close"></i>

            <form role="search" action="<?php echo esc_html(home_url()); ?>" method="get" class="main-search">
                <input id="main-search-input" autocomplete="off" name="s" type="search" placeholder="<?php echo esc_attr($search_placeholder); ?>">
            </form>
        </div>
    <?php endif; ?>

</header>