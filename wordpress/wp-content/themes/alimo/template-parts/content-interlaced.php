<?php
/**
 * Template part for displaying interlaced posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package alimo
 */

$alimo_data = get_option('alimo_data');

$prefix              = Alimo_Meta_Boxes::get_instance()->prefix;
$featured_post       = (function_exists('rwmb_meta')) ? rwmb_meta( "{$prefix}featured_post" ) : null;
$post_video_provider = (function_exists('rwmb_meta')) ? rwmb_meta( "{$prefix}video_provider" ) : null;
$post_video_id       = (function_exists('rwmb_meta')) ? rwmb_meta( "{$prefix}video_id" ) : null;

$featured_image_url             = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
$featured_image_replacement     = (function_exists('rwmb_meta')) ? is_array(rwmb_meta( "{$prefix}featured_image_replacement" )) ? current( rwmb_meta( "{$prefix}featured_image_replacement" ) ) : null : null;
$featured_image_replacement_url = $featured_image_replacement['full_url'];

$featured_image = ( ! empty( $featured_image_replacement_url ) ) ? $featured_image_replacement_url : $featured_image_url;
if ( empty( $featured_image ) && $post_video_id ) {
	$featured_image = alimo_get_video_thumbnail( $post_video_id, $post_video_provider );
}
$post_classes = (!empty($alimo_data['list-animation']) && true == $alimo_data['list-animation']) ? 'clearfix interlaced-article animate-in' : 'clearfix interlaced-article';
?>

<article <?php post_class($post_classes);?> >
    <a class="featured-image" href="<?php echo get_the_permalink(); ?>"
	    <?php if ($featured_image) { echo 'style="background-image: url('. esc_url($featured_image).')"'; } ?>></a>
    <div class="article-wrapper">
        <header>
            <figure>
				<?php if ( $alimo_data['list-categories'] ): ?>
                    <figcaption>
                        <div class="categories-list clearfix">
                          <?php if (is_sticky()): ?>
                              <span class="sticky-post" data-toggle="tooltip" data-placement="top" title="Sticky">
                                      <i class="fa fa-thumb-tack" aria-hidden="true"></i>
                                  </span>
                          <?php endif; ?>
							<?php if ( $featured_post == '1' ) : ?>

                                <span class="featured" data-toggle="tooltip" data-placement="top" title="Featured">
                                    <i class="fa fa-bookmark"></i>
                                </span>
							<?php endif; ?>
							<?php echo get_the_category_list(); ?>
                        </div>
                    </figcaption>
				<?php endif; ?>
                <h2>
                  <?php if (is_sticky() && !$alimo_data['list-categories'] ): ?>
                      <span class="sticky-post" data-toggle="tooltip" data-placement="top" title="Sticky">
                              <i class="fa fa-thumb-tack" aria-hidden="true"></i>
                          </span>
                  <?php endif; ?>
					<?php if ( $featured_post == '1' && !$alimo_data['list-categories'] ) : ?>

                        <span class="featured" data-toggle="tooltip" data-placement="top" title="Featured">
                            <i class="fa fa-bookmark"></i>
                        </span>
					<?php endif; ?>
                    <a href="<?php echo get_the_permalink(); ?>">
						<?php echo get_the_title(); ?>
                    </a>
                </h2>
				<?php // Post tags ?>
				<?php if ( get_the_tag_list() && $alimo_data['list-tags'] ): ?>
                    <div class="tags">
						<?php echo get_the_tag_list(); ?>
                    </div>
				<?php endif; ?>
            </figure>
        </header>
        <div class="article-entries">
            <div class="article-content">
                <?php the_excerpt(); ?>
            </div>
            <div class="read-more">
                <a href="<?php echo get_the_permalink(); ?>">
					<?php esc_html_e( 'read on', 'alimo' ); ?>
                    <i class="fa fa-angle-right"></i>
                </a>
				<?php if ( $alimo_data['list-reading-time'] ): ?>
                    <span class="read-time">
							<?php echo alimo_reading_time( $post ) . ' ';
                            esc_html_e( 'Min read', 'alimo' ); ?>
						</span>
				<?php endif; ?>
            </div>
        </div>
		<?php if ( $alimo_data['list-author'] OR $alimo_data['list-date'] OR $alimo_data['list-comments'] OR ( $alimo_data['list-likes'] && $alimo_data['enable_post_likes'] && function_exists( 'zilla_likes' ) ) OR $alimo_data['list-social'] ): ?>
            <footer>
                <div class="footer-container clearfix">
					<?php
					// Author details and post date
					if ( $alimo_data['list-author'] OR $alimo_data['list-date'] ): ?>
                        <div class="author-details">
							<?php
							// If author details enabled, display
							if ( $alimo_data['list-author'] ):
								$author_url = get_site_url() . '/author/' . get_the_author_meta( 'user_login' ); ?>
                                <a class="author-photo" href="<?php echo esc_url($author_url); ?>">
									<?php echo get_avatar( get_the_author_meta( 'ID' ), 100 ); ?>
                                </a>
                                <a class="author-name" href="<?php echo esc_url($author_url); ?>"><?php the_author(); ?></a>
							<?php endif;
							// Post date
							if ( $alimo_data['list-date'] ): ?>
                                <a class="post-date" href="<?php echo get_site_url().'/'.get_the_date( 'Y/m/d' ); ?>/"><?php echo alimo_relative_time(); ?></a>
							<?php endif; ?>
                        </div>
					<?php endif;
					// Post meta
					if ( $alimo_data['list-comments'] OR $alimo_data['list-social'] OR ( $alimo_data['list-likes'] && $alimo_data['enable_post_likes'] && function_exists( 'zilla_likes' ) ) ): ?>
                        <div class="post-meta clearfix">
							<?php $comments_number = get_comments_number(); ?>
							<?php if ( $comments_number > 0 && $alimo_data['list-comments'] ): ?>
                                <div class="comments">
                                    <a href="<?php echo get_the_permalink(); ?>#the-comment">
                                        <i class="fa fa-comment"></i>
										<?php echo esc_html($comments_number); ?>
                                    </a>
                                </div>
							<?php endif; ?>

							<?php if ( function_exists( 'zilla_likes' ) && $alimo_data['list-likes'] && $alimo_data['enable_post_likes'] ): ?>
                                <div class="like">
									<?php zilla_likes(); ?>
                                </div>
							<?php endif;
							// Social share
                            $post_sharing_networks  = array_filter($alimo_data['post_sharing_networks']);
							if ( $alimo_data['list-social'] && !empty($post_sharing_networks) ): ?>
                                <div class="share">
                                    <i class="fa fa-share"></i>
									<?php esc_html_e( 'share', 'alimo' ); ?>
									<?php get_template_part( 'template-parts/sharing-options', 'tooltip' ); ?>
                                </div>
							<?php endif; ?>
                        </div>
					<?php endif; ?>
                </div>
            </footer>
		<?php endif; ?>
    </div>
</article>