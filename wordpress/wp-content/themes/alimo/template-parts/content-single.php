<?php
/**
 * Template part for displaying single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package alimo
 */

$alimo_data          = get_option('alimo_data');
$sidebar_enabled     = ($alimo_data['enable-sidebar'] == true &&  is_active_sidebar('sidebar-1') ) ? true : false;
$sidebar_always_open = ($alimo_data['sidebar-always-open'] == true) ? true : false;
$sidebar_position    = ($alimo_data['sidebar-position'] == 'left') ? 'left-sidebar' : 'right-sidebar';

$prefix        = Alimo_Meta_Boxes::get_instance()->prefix;
$content_class = "post-content-wrapper col-xs-12 {$sidebar_position} ";
if ($sidebar_enabled) {
  if ($sidebar_always_open) {
    $content_class .= " col-md-8 center-block";
    /*if ($sidebar_position == "right-sidebar") {
      $content_class .= " col-md-offset-1";
    }*/
  }
  else {
    $content_class .= " center-block col-md-9";
  }
}
else {
  $content_class .= " center-block col-md-9";
}
/**
 * Header overlay settings
 * check if it's enabled globally in the theme options panel or at the post level
 */
$header_overlay_enabled           = (function_exists('rwmb_meta')) ? (rwmb_meta("{$prefix}enable_overlay") == 'enable') ? TRUE : FALSE : FALSE;
$post_header_overlay_color        = (function_exists('rwmb_meta')) ? rwmb_meta("{$prefix}post_header_overlay_color") : NULL;
$post_header_overlay_alpha        = (function_exists('rwmb_meta')) ? rwmb_meta("{$prefix}post_header_overlay_alpha") : NULL;
$global_post_header_overlay_color = (!empty($alimo_data['post_header_overlay']['color'])) ? $alimo_data['post_header_overlay']['color'] : '';
$global_post_header_overlay_alpha = (!empty($alimo_data['post_header_overlay']['alpha'])) ? $alimo_data['post_header_overlay']['alpha'] : '';

/**
 * If Post Custom Options > Custom Header Overlay is set to 'Enabled' and a color and/or transparency is set, use them
 * Otherwise, use the options from Alimo > Posts > Header > Header overlay color
 */
$post_overlay = array(
  'color' => ( $header_overlay_enabled && !empty($post_header_overlay_color) ) ? $post_header_overlay_color : $global_post_header_overlay_color,
  'alpha' => ( $header_overlay_enabled && !empty($post_header_overlay_alpha) ) ? $post_header_overlay_alpha / 100 : $global_post_header_overlay_alpha,
);
/**
 * If any values were set for post overlay, create the inline style
 */
if (!empty($post_overlay)) {
  $overlay_style = '';
  if (!empty($post_overlay['color'])) {
    $overlay_style .= "background-color: {$post_overlay['color']}; ";
  }
  if (!empty($post_overlay['alpha'])) {
    $overlay_style .= "opacity:{$post_overlay['alpha']}; ";
  }
}

/**
 * If we have a post video ID set, test the embed URL and set it
 * Used below when checking if we have a valid video url
 */
$post_video_id       = (function_exists('rwmb_meta')) ? rwmb_meta("{$prefix}video_id") : NULL;
$post_video_provider = (function_exists('rwmb_meta')) ? rwmb_meta("{$prefix}video_provider") : NULL;
if ($post_video_id) {
  $request_url = '';
  $embed_url   = '';
  if ($post_video_provider && $post_video_provider == "youtube") {
    $request_url = "https://www.youtube.com/embed/{$post_video_id}";
  }
  elseif ($post_video_provider && $post_video_provider == "vimeo") {
    $request_url = "https://player.vimeo.com/video/{$post_video_id}";
  }
  $response = wp_remote_get($request_url);
  if (is_wp_error($response)) {
    //request can't be performed
    $embed_url = NULL;
    return FALSE;
  }
  if ('200' == wp_remote_retrieve_response_code($response)) {
    //request succeeded
    $embed_url = $request_url;
  }
}

$featured_image = get_the_post_thumbnail_url($post->ID,'full');
?>
<div <?php post_class('article-wrapper') ?> >
  <?php if ($post_video_id OR has_post_thumbnail()): ?>
      <div class="container-fluid">
          <div class="row">
            <?php if (!empty($embed_url)): /* Check if we have a valid video url */ ?>
                <div class="single-article-featured-image video-format clearfix">
                    <div class="video-format-container">
                        <iframe src="<?php echo esc_url($embed_url); ?>" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            <?php elseif (has_post_thumbnail()): /* If no video is available, check if we have a featured image set and header overlay settings*/ ?>
                <div class="single-article-featured-image clearfix" style="background-image: url('<?php echo esc_url($featured_image); ?>')">
                  <?php if (!empty($overlay_style)): ?>
                      <div class="overlay" style="<?php echo esc_attr($overlay_style) ?>"></div>
                  <?php endif; ?>
                </div>
            <?php endif; ?>
          </div>
      </div>
  <?php endif; ?>
    <div class="container">
        <div class="row">
            <?php $post_sharing_networks  = array_filter($alimo_data['post_sharing_networks']); ?>
          <?php if ((function_exists('zilla_likes') && $alimo_data['enable_post_likes']) OR ( !empty($post_sharing_networks) ) ): ?>
              <div class="share-widget sidebar-<?php echo esc_attr($alimo_data['sidebar-position']); ?>">
                  <span class="title"><?php esc_html_e('Share','alimo'); ?></span>
                  <div class="share-buttons">
                    <?php if (function_exists('zilla_likes') && $alimo_data['enable_post_likes']): ?>
                        <div class="like">
                          <?php zilla_likes(); ?>
                        </div>
                    <?php endif; ?>
                    <?php get_template_part('template-parts/sharing-options', 'list'); ?>
                  </div>
              </div>
          <?php endif; ?>

            <article class="main-article">
                <div class="<?php echo esc_attr($content_class); ?>">
                  <?php if ($alimo_data['category-display'] == 1): ?>
                    <div class="clearfix">
	                    <?php echo get_the_category_list(); ?>
                    </div>
                  <?php endif; ?>
                    <div class="single-post-title">
                        <h1><?php the_title(); ?></h1>
                    </div>
                    <div class="post-meta">
	                    <?php if(1 == $alimo_data['author-display']):?>
                        <div class="author-meta">
                            <span class="author-details">
                              <?php esc_html_e('A story by', 'alimo');
                              $author_url = get_site_url() . '/author/' . get_the_author_meta('user_login'); ?>
                                <a class="author-photo" href="<?php echo esc_url($author_url); ?>"><?php echo get_avatar(get_the_author_meta('ID'), 36); ?></a>
                                <a class="author-name" href="<?php echo esc_url($author_url); ?>"><?php the_author(); ?></a>
                            </span>
                            <span class="post-date"><?php echo alimo_relative_time(); ?></span>
                        </div>
	                    <?php endif; ?>
                        <?php if(1 == $alimo_data['comments-number-display']):?>
                        <div class="comment-count-wrapper">
                            <a href="#the-comment" class="comment-count-link" data-easing="easeInOutQuint" data-scroll="" data-speed="600" data-url="false">
                                <i class="fa fa-comment"></i> <?php comments_number(esc_html__('No Comments', 'alimo'), esc_html__('1 Comment', 'alimo'), esc_html__('% Comments', 'alimo')); ?>
                            </a>
                        </div>
                        <?php endif; ?>
                    </div>
                  <?php if (current_user_can('edit_post', $post->ID)): ?>
                      <div class="edit-post-link">
                        <?php edit_post_link(); ?>
                      </div>
                  <?php endif; ?>
                    <div class="post-content">
                      <?php the_content();
                      wp_link_pages(array(
                        'before'      => '<div class="page-links">' . esc_html__('Pages:', 'alimo'),
                        'after'       => '</div>',
                        'link_before' => '<span class="page-number">',
                        'link_after'  => '</span>',
                      ));
                      ?>
                    </div>
                    <?php $post_sharing_networks  = array_filter($alimo_data['post_sharing_networks']); ?>
                  <?php if ( (function_exists('zilla_likes') && $alimo_data['enable_post_likes']) OR ( !empty($post_sharing_networks) ) ): ?>
                      <div class="footer-share">
                          <span class="title"><?php esc_html_e('Share','alimo'); ?></span>
                        <?php if (function_exists('zilla_likes') && $alimo_data['enable_post_likes']): ?>
                            <div class="like">
                              <?php zilla_likes(); ?>
                            </div>
                        <?php endif; ?>
                        <?php get_template_part('template-parts/sharing-options', 'list'); ?>
                      </div>
                  <?php endif; ?>
                </div>
              <?php ($sidebar_always_open) ? get_sidebar() : NULL; ?>
            </article>
        </div>
    </div>
</div>

