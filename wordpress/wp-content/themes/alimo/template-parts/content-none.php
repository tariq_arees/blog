<?php
/**
 * Template part for displaying a message that posts cannot be found.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package alimo
 */
$alimo_data = get_option('alimo_data');
$sidebar_enabled     = ( $alimo_data['enable-sidebar'] == true &&  is_active_sidebar('sidebar-1')  ) ? true : false;
$sidebar_always_open = ( $alimo_data['sidebar-always-open'] == true ) ? true : false;
$sidebar_position = ($alimo_data['sidebar-position'] == 'left') ? 'left-sidebar':'right-sidebar';

$content_class = ($sidebar_enabled) ? ($sidebar_always_open) ? "articles-container col-md-9 {$sidebar_position}": "articles-container col-md-9 {$sidebar_position}":"articles-container col-md-9 {$sidebar_position}";
?>
<div class="<?php echo esc_attr($content_class)?>">
		<h3>
			<?php esc_html_e('Nothing Found', 'alimo'); ?>
		</h3>

		<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<p><?php printf( wp_kses( _e('Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'alimo'), array( 'a' => array( 'href' => array() ) ) ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

		<?php elseif ( is_search() ) : ?>

			<p><?php esc_html_e('Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'alimo'); ?></p>
			<?php get_search_form(); ?>

		<?php else : ?>

			<p><?php esc_html_e('It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'alimo'); ?></p>
			<?php get_search_form(); ?>

		<?php endif; ?>
</div>
