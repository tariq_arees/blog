<?php
/**
 * Template part for typography selected or defined in backend
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package alimo
 */

function alimo_typography_styles() {

	$alimo_data = get_option( 'alimo_data' );

	$heading_font_family_escaped = ( ! empty( $alimo_data['heading-font']['font-family'] ) ) ? esc_attr( $alimo_data['heading-font']['font-family'] ) : NULL;
	$heading_font_color_escaped  = ( ! empty( $alimo_data['heading-font']['color'] ) ) ? esc_attr( $alimo_data['heading-font']['color'] ) : NULL;
	$content_font_family_escaped = ( ! empty( $alimo_data['content-font']['font-family'] ) ) ? esc_attr( $alimo_data['content-font']['font-family'] ) : NULL;
	$content_color_escaped       = ( ! empty( $alimo_data['content-font']['color'] ) ) ? esc_attr( $alimo_data['content-font']['color'] ) : NULL;
	$rm_serif_font_escaped       = ( ! empty( $alimo_data['reading-mode-serif']['font-family'] ) ) ? esc_attr( $alimo_data['reading-mode-serif']['font-family'] ) : NULL;
	$rm_sans_serif_font_escaped  = ( ! empty( $alimo_data['reading-mode-sans-serif']['font-family'] ) ) ? esc_attr( $alimo_data['reading-mode-sans-serif']['font-family'] ) : NULL;
	$logo_font_escaped           = ( ! empty( $alimo_data['logo-font']['font-family'] ) ) ? esc_attr( $alimo_data['logo-font']['font-family'] ) : NULL;
	$output ="";
	if ( ! empty( $heading_font_family_escaped ) OR ! empty( $content_font_family_escaped ) OR ! empty( $rm_serif_font_escaped ) OR ! empty( $rm_sans_serif_font_escaped ) OR ! empty( $content_color_escaped ) ) :
		if ( ! empty( $content_font_family_escaped ) ):
            $output .= "
                html body {
                    font-family: ".$content_font_family_escaped .";
                }";
        endif;

		if ( ! empty( $heading_font_family_escaped ) ):
			$output .= "
            h1, .h1,
            h2, .h2,
            h3, .h3,
            h4, .h4,
            h5, .h5,
            h6, .h6,
            h1, .h1,
            h2, .h2,
            h3, .h3,
            h4, .h4,
            h5, .h5,
            h6, .h6{
                font-family: ". $heading_font_family_escaped .";
                ".($heading_font_color_escaped ? 'color: '.$heading_font_color_escaped.';': null ).";
            }
            
            cite{
             font-family: ". $heading_font_family_escaped .";
            }
            ";
        endif;

		if ( ! empty( $logo_font_escaped ) ):
			$output .=" .site-title{font-family:".$logo_font_escaped .";}";
		endif;

		if ( ! empty( $content_color_escaped ) ):
			$output .= "
            .post-content, .page-content-wrapper, .article-wrapper article{
                color: ". hex2rgba( $content_color_escaped, 0.7 ) .";
            }";
		endif;

		if ( ! empty( $rm_serif_font_escaped ) ):
			$output .= "
            .reading-mode-container .reading-options .reading-options-container .ff-options .font-box.serif .typeface {
                font-family: ". $rm_serif_font_escaped."
            }
            .reading-mode-container .reading-mode-content-container.serif * {
                font-family: ". $rm_serif_font_escaped."
            }
            ";

		endif;

		if ( ! empty( $rm_sans_serif_font_escaped ) ) :
			$output .= "
            .reading-mode-container .reading-options .reading-options-container .ff-options .font-box.sans-serif .typeface {
                font-family: ". $rm_sans_serif_font_escaped.";
            }
            .reading-mode-container .reading-mode-content-container.sans-serif * {
                font-family: ". $rm_sans_serif_font_escaped.";
            }
            ";
		endif;

	endif;

	if(!empty($output)){
	    wp_add_inline_style('style',$output);
    }
}
add_action('wp_enqueue_scripts','alimo_typography_styles');