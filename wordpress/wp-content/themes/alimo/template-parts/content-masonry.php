<?php
/**
 * Template part for displaying masonry posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package alimo
 */

$alimo_data = get_option('alimo_data');
$sidebar_enabled     = ( $alimo_data['enable-sidebar'] == true &&  is_active_sidebar('sidebar-1')  ) ? true : false;
$sidebar_always_open = ( $alimo_data['sidebar-always-open'] == true ) ? true : false;
$sidebar_position = ($alimo_data['sidebar-position'] == 'left') ? 'left-sidebar':'right-sidebar';


$prefix              = Alimo_Meta_Boxes::get_instance()->prefix;
$featured_post       = (function_exists('rwmb_meta')) ? rwmb_meta( "{$prefix}featured_post" ): null;
$post_video_provider = (function_exists('rwmb_meta')) ? rwmb_meta( "{$prefix}video_provider" ): null;
$post_video_id       = (function_exists('rwmb_meta')) ? rwmb_meta( "{$prefix}video_id" ): null;

$featured_image_url             = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
$featured_image_replacement     = (function_exists('rwmb_meta')) ? is_array(rwmb_meta( "{$prefix}featured_image_replacement" )) ? current( rwmb_meta( "{$prefix}featured_image_replacement" ) ) : null : null;
$featured_image_replacement_url = $featured_image_replacement['full_url'];

$featured_image = ( ! empty( $featured_image_replacement_url ) ) ? $featured_image_replacement_url : $featured_image_url;
if ( empty( $featured_image ) && $post_video_id ) {
	$featured_image = alimo_get_video_thumbnail( $post_video_id, $post_video_provider );
}

$content_class = ( $sidebar_enabled ) ? ( $sidebar_always_open ) ? 'col-md-6' : 'col-md-4' : 'col-md-4';
if(!empty($alimo_data['list-animation']) && true == $alimo_data['list-animation']){
  $content_class .= ' animate-in';
}
?>
<article <?php post_class($content_class); ?> >
    <div class="article-wrapper">
        <header>
            <figure <?php if(!$featured_image) echo 'class="no-featured-image"'?>>
				<?php if ( $featured_image ): ?>
                    <a href="<?php echo get_the_permalink(); ?>">
                        <img src="<?php echo esc_url($featured_image); ?>" alt="">
                    </a>
				<?php endif; ?>
				<?php if ( $alimo_data['list-categories'] ): ?>
                    <figcaption>
                        <div class="categories-list clearfix">
                          <?php if (is_sticky()): ?>
                              <span class="sticky-post" data-toggle="tooltip" data-placement="top" title="Sticky">
                                  <i class="fa fa-thumb-tack" aria-hidden="true"></i>
                              </span>
                          <?php endif; ?>
                          <?php if ($featured_post == '1') : ?>
                              <span class="featured" data-toggle="tooltip" data-placement="top" title="Featured">
                                  <i class="fa fa-bookmark"></i>
                              </span>
                          <?php endif; ?>
                          <?php echo get_the_category_list(); ?>
                        </div>
                    </figcaption>
				<?php endif; ?>
            </figure>
            <h2>
              <?php if (is_sticky() && !$alimo_data['list-categories']): ?>
                  <span class="sticky-post" data-toggle="tooltip" data-placement="top" title="Sticky">
                        <i class="fa fa-thumb-tack" aria-hidden="true"></i>
                    </span>
              <?php endif; ?>
              <?php if ($featured_post == '1' && !$alimo_data['list-categories']) : ?>
                  <span class="featured" data-toggle="tooltip" data-placement="top" title="Featured">
                      <i class="fa fa-bookmark"></i>
                  </span>
              <?php endif; ?>
                <a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a>
            </h2>
			<?php if ( get_the_tag_list() && $alimo_data['list-tags'] ): ?>
                <div class="tags">
					<?php echo get_the_tag_list(); ?>
                </div>
			<?php endif; ?>
        </header>
        <div class="article-entries">
            <div class="article-content">
                <?php the_excerpt(); ?>
            </div>
            <div class="read-more">
                <a href="<?php echo get_the_permalink(); ?>">
					<?php esc_html_e( 'read on', 'alimo' ); ?>
                    <i class="fa fa-angle-right"></i>
                </a>
				<?php if ( $alimo_data['list-reading-time'] ): ?>
                    <span class="read-time">
                        <?php echo alimo_reading_time( $post ) . ' ';
                        esc_html_e( 'Min read', 'alimo' ); ?>
                    </span>
				<?php endif; ?>
            </div>
        </div>
		<?php if ( $alimo_data['list-author'] OR $alimo_data['list-date'] OR $alimo_data['list-comments'] OR ( $alimo_data['list-likes'] && $alimo_data['enable_post_likes'] && function_exists( 'zilla_likes' ) ) OR $alimo_data['list-social'] ): ?>
            <footer>
                <div class="footer-container clearfix">
					<?php
					// Author details and post date
					if ( $alimo_data['list-author'] OR $alimo_data['list-date'] ): ?>
                        <div class="author-details">
							<?php
							// If author details enabled, display
							if ( $alimo_data['list-author'] ):
								$author_url = get_site_url() . '/author/' . get_the_author_meta( 'user_login' ); ?>
                                <a class="author-photo" href="<?php echo esc_url($author_url); ?>">
									<?php echo get_avatar( get_the_author_meta( 'ID' ), 100 ); ?>
                                </a>
                                <a class="author-name" href="<?php echo esc_url($author_url); ?>"><?php the_author(); ?></a>
							<?php endif;
							// Post date
							if ( $alimo_data['list-date'] ): ?>
                                <a class="post-date" href="<?php echo get_site_url().'/'.get_the_date( 'Y/m/d' ); ?>/"><?php echo alimo_relative_time(); ?></a>
							<?php endif; ?>
                        </div>
					<?php endif;
					// Post meta
					if ( $alimo_data['list-comments'] OR $alimo_data['list-social'] OR ( $alimo_data['list-likes'] && $alimo_data['enable_post_likes'] && function_exists( 'zilla_likes' ) ) ): ?>
                        <div class="post-meta clearfix">
							<?php $comments_number = get_comments_number(); ?>
							<?php if ( $comments_number > 0 && $alimo_data['list-comments'] ): ?>
                                <div class="comments">
                                    <a href="<?php echo get_the_permalink(); ?>#the-comment">
                                        <i class="fa fa-comment"></i>
										<?php echo esc_html($comments_number); ?>
                                    </a>
                                </div>
							<?php endif; ?>

							<?php if ( function_exists( 'zilla_likes' ) && $alimo_data['list-likes'] && $alimo_data['enable_post_likes'] ): ?>
                                <div class="like">
									<?php zilla_likes(); ?>
                                </div>
							<?php endif;
							// Social share
                            $post_sharing_networks  = array_filter($alimo_data['post_sharing_networks']);
							if ( $alimo_data['list-social'] && !empty($post_sharing_networks) ): ?>
                                <div class="share">
                                    <i class="fa fa-share"></i>
									<span><?php esc_html_e( 'share', 'alimo' ); ?></span>
									<?php get_template_part( 'template-parts/sharing-options', 'tooltip' ); ?>
                                </div>
							<?php endif; ?>
                        </div>
					<?php endif; ?>
                </div>
            </footer>
		<?php endif; ?>
    </div>
</article>