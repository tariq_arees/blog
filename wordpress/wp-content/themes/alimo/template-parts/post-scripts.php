<?php

function alimo_post_inline_styles_and_scripts(){
	$alimo_data = get_option('alimo_data');
	$sqs_enabled = (!empty($alimo_data['social-quote-sharer']) && (1 == $alimo_data['social-quote-sharer']) ) ?  TRUE : FALSE;
	$sqs_fb_appid = $sqs_enabled ? !empty($alimo_data['social-quote-sharer-facebook-id']) ? $alimo_data['social-quote-sharer-facebook-id'] : "not defined" : "" ;
	$js_output = 'var social_quote_sharer_facebook_id = "'.esc_attr($sqs_fb_appid).'";';

	$reading_mode_inherited = get_query_var( 'reading_mode', 'not defined' );
	if ( $reading_mode_inherited == 'enabled' ) {
		$rm_script = 'var reading_mode_inherited = "enabled";';
		$js_output .= $rm_script;

	} else {
		$rm_script = 'var reading_mode_inherited = "'.esc_attr($reading_mode_inherited).'";';
		$js_output .= $rm_script;
	}

	$prefix = Alimo_Meta_Boxes::get_instance()->prefix;
	$background_music_autoplay = (function_exists('rwmb_meta')) ? rwmb_meta( "{$prefix}background_music_autoplay", $args = array(), get_the_ID() ) : null;
	$background_music_arr          = (function_exists('rwmb_meta')) ? is_array(current(rwmb_meta( "{$prefix}background_music", "type=file" ))) ? current(rwmb_meta( "{$prefix}background_music", "type=file" )) : null : null;
	$background_music = (!empty($background_music_arr)) ? $background_music_arr['url'] : null;
	$file_mime_type   = wp_check_filetype( $background_music );

	if ( !empty($background_music) && 'audio/mpeg' == $file_mime_type['type'] &&  'enable' == $background_music_autoplay  && isset($_COOKIE['auto-play-audio']) && 'pause' != $_COOKIE['auto-play-audio'] ):
		$js_output .='
                document.addEventListener("DOMContentLoaded", function () {
                    window.alimo_cookie.setCookie("auto-play-audio", "play", "1");
                });
            ';
	endif;

	wp_add_inline_script('ALIMO_MainJS', $js_output);

	if ( !empty($alimo_data['reading-progress-bar-color']) ) {
		$output = ".reading-position { background: ". esc_attr($alimo_data['reading-progress-bar-color']) ." none repeat scroll 0% 0% !important; }";
		wp_add_inline_style('style', $output);
	}
}
add_action('wp_enqueue_scripts' , 'alimo_post_inline_styles_and_scripts');