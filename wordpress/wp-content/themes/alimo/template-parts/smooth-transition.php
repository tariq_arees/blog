<?php
/**
 * Template part for smooth page transition.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package alimo
 */


function alimo_smooth_transition_style() {

	$alimo_data          = get_option( 'alimo_data' );
	$transition_message  = ( ! empty( $alimo_data['transition-message'] ) && $alimo_data['transition-message-enabled'] ) ? $alimo_data['transition-message'] : NULL;
	$transition_bg_color = ( ! empty ( $alimo_data['transition-bg-color'] ) ) ? $alimo_data['transition-bg-color']['color'] : '';
	$transition_bg_alpha = ( ! empty ( $alimo_data['transition-bg-color'] ) ) ? $alimo_data['transition-bg-color']['alpha'] : '';
	$transition_bg_rgba  = ( ! empty( $transition_bg_color ) && ! empty( $transition_bg_alpha ) ) ? hex2rgba( $transition_bg_color, $transition_bg_alpha ) : FALSE;
	$transition_el_color = ( ! empty ( $alimo_data['transition-el-color'] ) ) ? $alimo_data['transition-el-color']['color'] : '';
	$transition_el_alpha = ( ! empty ( $alimo_data['transition-el-color'] ) ) ? $alimo_data['transition-el-color']['alpha'] : '';
	$transition_el_rgba  = ( ! empty( $transition_el_color ) && ! empty( $transition_el_alpha ) ) ? hex2rgba( $transition_el_color, $transition_el_alpha ) : FALSE;


	$output = "div.smooth-transition{
        background: " . ( $transition_bg_rgba ? esc_attr( $transition_bg_rgba ) : 'white' ) . " !important;
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        opacity: 1;
        transition: all 0.5s ease-in-out;
        z-index: 10;
    }
    body.content-loaded .smooth-transition{
	    opacity:0;
	    visibility: hidden;
    }
    @keyframes spin {
	    0% {
	        transform: rotate(0deg);
	    }
	    100% {
	        transform: rotate(360deg);
	    }
    }
    .spinner {
	    content: '';
	    position: fixed;
	    display: block;
	    width: 60px;
	    height: 60px;
	    top: calc(35%);
	    left: calc(50% - 30px);
	    z-index:1;
	    border-radius: 80px;
	    box-shadow: 0 6px 0 0 " . ( $transition_el_rgba ? esc_attr( $transition_el_rgba ) : 'black' ). ";
	    animation: spin 1s linear infinite;
    }
    html.content-loaded .spinner{
        animation-play-state: paused;
    }";
	if ( $transition_message ) :
		$output .= "
        .message{
        position: fixed;
        font-family: '" .( esc_attr( $alimo_data["transition-message-font"]['font-family'] ) ). "',serif;
        font-size: " .( esc_attr( $alimo_data["transition-message-font"]["font-size"] ) ). ";
        color: " . ( ( $transition_el_rgba ) ? esc_attr( $transition_el_rgba ) : 'black' ). ";
        width: 100%;
        text-align: center;
        top: 50%;
        line-height: 1.25em;
        }";
	endif;

	wp_add_inline_style( 'style', $output );

	$js_output = "document.addEventListener('DOMContentLoaded', function () {
	    // Animate in
	    document.querySelector('body').classList.add('content-loaded');
	    document.querySelector('html').classList.add('content-loaded');
	  });";
	wp_add_inline_script( 'ALIMO_MainJS', $js_output );
}

add_action( 'wp_enqueue_scripts', 'alimo_smooth_transition_style' );

?>

