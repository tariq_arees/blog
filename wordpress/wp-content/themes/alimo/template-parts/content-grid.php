<?php
/**
 * Template part for displaying grid posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package alimo
 */

$alimo_data = get_option('alimo_data');

$sidebar_enabled     = ( $alimo_data['enable-sidebar'] == true &&  is_active_sidebar('sidebar-1')  ) ? true : false;
$sidebar_always_open = ( $alimo_data['sidebar-always-open'] == true ) ? true : false;
$sidebar_position = ($alimo_data['sidebar-position'] == 'left') ? 'left-sidebar':'right-sidebar';


$prefix              = Alimo_Meta_Boxes::get_instance()->prefix;
$featured_post       = (function_exists('rwmb_meta')) ?rwmb_meta( "{$prefix}featured_post" ) : null;
$post_video_provider = (function_exists('rwmb_meta')) ?rwmb_meta( "{$prefix}video_provider" ) : null;
$post_video_id       = (function_exists('rwmb_meta')) ?rwmb_meta( "{$prefix}video_id" ) : null;

$featured_image_url             = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
$featured_image_replacement     = (function_exists('rwmb_meta')) ? is_array(rwmb_meta( "{$prefix}featured_image_replacement" )) ? current( rwmb_meta( "{$prefix}featured_image_replacement" ) ) : null : null;
$featured_image_replacement_url = $featured_image_replacement['full_url'];

$featured_image = ( ! empty( $featured_image_replacement_url ) ) ? $featured_image_replacement_url : $featured_image_url;
if ( empty( $featured_image ) && $post_video_id ) {
	$featured_image = alimo_get_video_thumbnail( $post_video_id, $post_video_provider );
}

$content_class = ( $sidebar_enabled ) ? ( $sidebar_always_open ) ? 'col-sm-6 col-xs-12' : 'col-sm-4 col-xs-12' : 'col-sm-4 col-xs-12';
if(!empty($alimo_data['list-animation']) && true == $alimo_data['list-animation']){
    $content_class .= ' animate-in';
}

?>
<article <?php post_class($content_class); ?> >
    <div class="article-wrapper">
        <header>
            <figure>
                <a class="featured-image" href="<?php echo get_the_permalink(); ?>" <?php if ( $featured_image ) { echo 'style="background-image: url(' . esc_url($featured_image ). ')"'; } ?>></a>
                <figcaption>
                    <div class="categories-list clearfix">
                      <?php if (is_sticky()): ?>
                          <span class="sticky-post" data-toggle="tooltip" data-placement="top" title="Sticky">
                                  <i class="fa fa-thumb-tack" aria-hidden="true"></i>
                              </span>
                      <?php endif; ?>
						<?php if ( $featured_post == '1' && $alimo_data['list-categories'] ) : ?>
                            <span class="featured" data-toggle="tooltip" data-placement="top" title="Featured">
                                <i class="fa fa-bookmark"></i>
                            </span>
						<?php endif; ?>
						<?php echo ( $alimo_data['list-categories'] ) ? get_the_category_list() : null; ?>
                    </div>
                </figcaption>
            </figure>
            <h2>
              <?php if (is_sticky() && !$alimo_data['list-categories']): ?>
                  <span class="sticky-post" data-toggle="tooltip" data-placement="top" title="Sticky">
                          <i class="fa fa-thumb-tack" aria-hidden="true"></i>
                      </span>
              <?php endif; ?>
				<?php if ( $featured_post == '1' && !$alimo_data['list-categories'] ) : ?>
                    <span class="featured" data-toggle="tooltip" data-placement="top" title="Featured">
                        <i class="fa fa-bookmark"></i>
                    </span>
				<?php endif; ?>
                <a href="<?php echo get_the_permalink(); ?>">
					<?php echo get_the_title(); ?>
                </a>
            </h2>
        </header>
    </div>
</article>
	

