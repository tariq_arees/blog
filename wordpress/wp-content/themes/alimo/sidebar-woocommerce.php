<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package alimo
 */
global $woocommerce;
$alimo_data = get_option('alimo_data');
$woo_sidebar_enabled  = ( $alimo_data['woo-enable-sidebar'] == 1 && is_active_sidebar('woocommerce-sidebar') ) ? true : false;
$woo_sidebar_position = ( $alimo_data['woo-sidebar-position'] == 'left' ) ? 'left-sidebar' : 'right-sidebar';
?>
<?php //check if WooCommerce is installed
if ( $woocommerce ):
	/* Is the page being displayed a WooCommerce template ?
	 * (cart and checkout are standard pages with shortcodes and thus are not included).
	 */
	if ( $woo_sidebar_enabled ): ?>
        <aside class="main-sidebar fixed-sidebar col-sm-3 <?php echo esc_attr($woo_sidebar_position); ?>">
            <div class="sidebar-content">
				<?php dynamic_sidebar( 'woocommerce-sidebar' ); ?>
            </div>
        </aside>

	<?php endif;
endif; ?>
