<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package alimo
 */

get_header();

$alimo_data = get_option('alimo_data');
$sidebar_enabled     = ( $alimo_data['enable-sidebar'] == true &&  is_active_sidebar('sidebar-1')  ) ? true : false;
$sidebar_always_open = ( $alimo_data['sidebar-always-open'] == true ) ? true : false;
$sidebar_position = ($alimo_data['sidebar-position'] == 'left') ? 'left-sidebar':'right-sidebar';

$content_class = ( $sidebar_enabled ) ? ( $sidebar_always_open ) ? "col-sm-9 {$sidebar_position}" : "col-sm-12" : "col-sm-12";
$layout        = $alimo_data['search-layout'];
?>

<div class="container-fluid">
    <div class="row">
        <div class="archive-page">
            <div class="container">
                <h3>
					<?php printf( esc_html__( 'Search Results for: %s', 'alimo' ), get_search_query() ); ?>
                </h3>
            </div>
        </div>
    </div>
</div>

<div class="main-content">
    <div class="container">
        <div class="row">
			<?php
			/* Set articles wrapper class based on selected layout */
			$articles_wrapper_class = 'articles-container';
			switch ( $layout ) {
				case 'standard' :
					$articles_wrapper_class = 'articles-container';
					$content_class = ( $sidebar_enabled ) ? ( $sidebar_always_open ) ? "col-sm-9 {$sidebar_position}" : "col-sm-12 col-md-9 center-block" : "col-sm-12 col-md-9 center-block";
					break;
				case 'masonry' :
					$articles_wrapper_class = 'articles-container-masonry no-padding';
					break;
				case 'grid' :
					$articles_wrapper_class = 'articles-container-grid no-padding';
					break;
				case 'interlaced-full' :
					$articles_wrapper_class = 'articles-container-interlaced full-width no-padding';
					$content_class = ( $sidebar_enabled ) ? ( $sidebar_always_open ) ? "col-sm-9 {$sidebar_position}" : "col-sm-12 col-md-9 center-block" : "col-sm-12 col-md-9 center-block";
					break;
				case 'interlaced-half' :
					$articles_wrapper_class = 'articles-container-interlaced no-padding';
					$content_class = ( $sidebar_enabled ) ? ( $sidebar_always_open ) ? "col-sm-9 {$sidebar_position}" : "col-sm-12 col-md-9 center-block" : "col-sm-12 col-md-9 center-block";
					break;
			}
			?>

			<?php if ( have_posts() ) : ?>
                <div class="<?php echo esc_attr($articles_wrapper_class) . ' ' . esc_attr($content_class); ?>">
                    <div id="content">
					<?php
                    /* Start the Loop */
					while ( have_posts() ) : the_post();
						/*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						if ( $layout == 'standard' ) {  // Standard Layout Listing
							get_template_part( 'template-parts/content', get_post_format() );
						}
						if ( $layout == 'masonry' ) { // Masonry Layout Listing
							get_template_part( 'template-parts/content', 'masonry' );
						}
						if ( $layout == 'grid' ) { // Grid Layout Listing
							get_template_part( 'template-parts/content', 'grid' );
						}
						if ( $layout == 'interlaced-full' ) { // Interlaced Full width Layout Listing
							get_template_part( 'template-parts/content', 'interlaced' );
						}
						if ( $layout == 'interlaced-half' ) { // Interlaced Half Layout Listing
							get_template_part( 'template-parts/content', 'interlaced' );
						}

					endwhile; ?>
                    </div>
                </div><!--article container-->
				<?php
			else :
				/* No results available */
				get_template_part( 'template-parts/content', 'none' );

			endif; ?>


			<?php ( $sidebar_enabled && $sidebar_always_open ) ? get_sidebar() : null; ?>
        </div><!--row-->
        <div class="row">
            <div class="col-sm-12"> <!-- pagination -->
                <div class="posts-pagination">
					<?php
					$args = array(
						'mid_size'  => 2,
						'prev_text' => _x( 'Newer', 'previous set of posts', 'alimo' ),
						'next_text' => _x( 'Older', 'next set of posts', 'alimo' ),
					);
					the_posts_pagination( $args ); ?>
                </div>
            </div>
        </div>
    </div><!--container-->
</div>
<?php get_footer(); ?>
