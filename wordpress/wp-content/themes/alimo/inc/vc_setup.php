<?php

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

class VC_Setup {

    protected static $instance = null;

    public $animations = array();
    public $bg_colors = array();

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function get_instance() {

        // If the single instance hasn't been set, set it now.
        if ( null == self::$instance ) {
            self::$instance = new self;
        }

        return self::$instance;
    }


    private function __construct() {

        if (class_exists('Vc_Manager')) {

            // Revome Elements
            vc_remove_element('vc_tour');
            vc_remove_element('vc_toggle');
            vc_remove_element('vc_button');
            //vc_remove_element('vc_button2');
            //vc_remove_element('vc_pie');
            vc_remove_element('vc_gallery');
            //vc_remove_element('vc_images_carousel');
            vc_remove_element('vc_posts_grid');
            vc_remove_element('vc_carousel');
            //vc_remove_element('vc_posts_slider');
            vc_remove_element('vc_cta_button');
            vc_remove_element('vc_cta_button2');
            //vc_remove_element('vc_text_separator');


			$this->posts_layout_1();
			//$this->posts_layout_2();
			//$this->posts_layout_3();
			
		}

    }

	
	public function posts_layout_1() {
	
	  $pages = get_pages(); 
	  foreach ( $pages as $page ) {
		$pages_array[$page->ID] = $page->post_title;
	  }
  
		// get blog categories
		$blog_cat_category = array();
		$cats = get_terms( 'category', 'orderby=name');
		foreach($cats as $cat) {
			$blog_cat_category[] = array(
				'label' => $cat->name,
				'value' => $cat->term_id,
				'group' => 'category'
			);
			//$blog_cat_category[$cat->term_id] = $cat->name;
		}

		
        vc_map( array(
            "name" => esc_html__('Alimo Blog Posts', 'alimo'),
            "base" => "alimo_blog_listing",
            'icon' => 'icon-wpb-wp',
            "category" => esc_html__('LayerBlend', 'alimo'),
            "params" => array(
	            array(
		            "type" => "dropdown",
		            "heading" => esc_html__('Listing type', 'alimo'),
		            "param_name" => "listing_type",
		            "value" => array(
			            esc_html__("Standard", 'alimo') => 'standard',
			            esc_html__("Masonry", 'alimo') => 'masonry',
			            esc_html__("Grid", 'alimo') => 'grid',
			            esc_html__("Interlaced", 'alimo') => 'interlaced',
			            esc_html__("Interlaced full width Images", 'alimo') => 'interlaced-full',
		            ),
		            "std"         => 'standard'
	            ),
	            array(
		            'type' => 'autocomplete',
		            'heading' => __( 'Select categories', 'alimo' ),
		            'param_name' => 'category',
		            'settings' => array(
			            'multiple' => true,
			            'min_length' => 1,
			            'groups' => true,
			            // In UI show results grouped by groups, default false
			            'unique_values' => true,
			            // In UI show results except selected. NB! You should manually check values in backend, default false
			            'display_inline' => true,
			            // In UI show results inline view, default false (each value in own line)
			            'delay' => 500,
			            // delay for search. default 500
			            'auto_focus' => true,
			            // auto focus input, default true
			            'values' => $blog_cat_category
		            ),
		            'description' => __( 'Enter one or more categories, to get posts from. Leave empty to use all categories', 'alimo' ),
	            ),
				array(
                    "type" => "textfield",
                    "heading" => esc_html__('Posts number', 'alimo'),
                    "param_name" => "posts_per_page",
                    "value" => '12'
                ),
				array(
                    "type" => "dropdown",
                    "heading" => esc_html__('Order by', 'alimo'),
                    "param_name" => "orderby",
					"group"=> esc_html__('Data Settings', 'alimo'),
					"value" => array(
                        esc_html__("Date", 'alimo') => 'date',
                        esc_html__("Title", 'alimo') => 'title',
                    ),
                    "std"         => 'date'
                ),
	            array(
		            "type" => "dropdown",
		            "heading" => esc_html__('Sort order', 'alimo'),
		            "param_name" => "order",
		            "group"=> esc_html__('Data Settings', 'alimo'),
		            "value" => array(
			            esc_html__("Ascending", 'alimo') => 'ASC',
			            esc_html__("Descending", 'alimo') => 'DESC'
		            ),
		            "std"         => 'ASC'
	            ),
            )
        ) );
    }


}

VC_Setup::get_instance();

