<?php
/**
 * Jetpack Compatibility File.
 *
 * @link https://jetpack.me/
 *
 * @package alimo
 */

/**
 * Add theme support for Infinite Scroll.
 * See: https://jetpack.me/support/infinite-scroll/
 */

function alimo_jetpack_setup() {
	add_theme_support( 'infinite-scroll', array(
		'container' => 'content',
		'footer' => 'main-footer',
		'render' => 'alimo_jp_is_render',
		'wrapper' => TRUE
	) );
} // end function alimo_jetpack_setup
add_action( 'init', 'alimo_jetpack_setup' );

/**
 * JetPack Infinite Scroll supported page types
 */

function alimo_is_support(){
    $supported =  current_theme_supports( 'infinite-scroll' ) && ( is_home() || is_archive() || is_search() || is_page_template() );
    return $supported;
}
add_filter('infinite_scroll_archive_supported', 'alimo_is_support');

/**
 * Custom render function for Infinite Scroll.
 */
function alimo_jp_is_render(){
	$alimo_data = get_option('alimo_data');
	$layout = 'standard';
	if(is_home()){
		$layout = $alimo_data['index-layout'];
	}
	elseif(is_search()){
		$layout = $alimo_data['search-layout'];
	}
	elseif(is_category() ){
		$layout = $alimo_data['category-layout'];
	}
	elseif(is_tag()){
		$layout = $alimo_data['tag-layout'];
	}
	elseif(is_author()){
		$layout = $alimo_data['author-layout'];
	}
	elseif(is_year() || is_month() || is_day()){
		$layout = $alimo_data['date-layout'];
	}
	elseif(is_post_type_archive()){
		$layout = $alimo_data['archive-layout'];
	}
	if(have_posts()):
	    while (have_posts()): the_post();
		    if ( $layout == 'standard' ) {  // Standard Layout Listing
			    get_template_part( 'template-parts/content', get_post_format() );
		    }
		    if ( $layout == 'masonry' ) { // Masonry Layout Listing
			    get_template_part( 'template-parts/content', 'masonry' );
		    }
		    if ( $layout == 'grid' ) { // Grid Layout Listing
			    get_template_part( 'template-parts/content', 'grid' );
		    }
		    if ( $layout == 'interlaced-full' ) { // Interlaced Full width Layout Listing
			    get_template_part( 'template-parts/content', 'interlaced' );
		    }
		    if ( $layout == 'interlaced-half' ) { // Interlaced Half Layout Listing
			    get_template_part( 'template-parts/content', 'interlaced' );
		    }
	    endwhile;
	endif;

}