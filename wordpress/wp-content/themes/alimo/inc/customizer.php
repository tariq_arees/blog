<?php
/**
 * alimo Theme Customizer.
 *
 * @package alimo
 */

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function alimo_customize_preview_js() {
	wp_enqueue_script( 'alimo_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'alimo_customize_preview_js' );

