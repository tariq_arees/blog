<?php

// File Security Check
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


class Alimo_Meta_Boxes {

	protected static $instance = null;

	public $prefix = 'alimo_meta_';


	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	protected function __construct() {

		add_filter( 'rwmb_meta_boxes', array( &$this, 'alimo_register_meta_boxes' ) );

	}


	/**
	 * @param $meta_boxes
	 *
	 * @return array
	 */
	public function alimo_register_meta_boxes( $meta_boxes ) {
		$prefix = $this->prefix;

		/*
		 * PAGE META BOXES
		 */
		$blog_cat = array( 0 => 'All categories' );
		$cats     = get_terms( 'category', 'orderby=count' );
		foreach ( $cats as $cat ) {
			$blog_cat[ $cat->term_id ] = $cat->name;
		}
		ksort( $blog_cat );
		$meta_boxes[] = array(
			'title'    => esc_html__( 'Blog Page Options', 'alimo' ),
			'pages'    => array( 'page' ),
			'context'  => 'side',
			'priority' => 'default',
			'show'     => array(
				'relation' => 'OR',
				'template' => array(
					'page-templates/template-blog-grid.php',
					'page-templates/template-blog-interlaced.php',
					'page-templates/template-blog-interlaced-full.php',
					'page-templates/template-blog-masonry.php',
					'page-templates/template-blog-standard.php',
				)
			),
			'fields'   => array(
				array(
					'name'    => esc_html__( 'Categories:', 'alimo' ),
					'id'      => "{$prefix}category",
					'type'    => 'select',
					'options' => $blog_cat
				),
			),
		);

		$meta_boxes[] = array(
			'title'   => esc_html__( 'Grayscale', 'alimo' ),
			'pages'   => array( 'page' ),
			'context' => 'normal',
			'fields'  => array(
				array(
					'name'    => esc_html__( 'Grayscale', 'alimo' ),
					'desc'    => esc_html__( 'Enable/Disable the grayscale feature', 'alimo' ),
					'id'      => $prefix . 'page_grayscale',
					'type'    => 'select',
					'options' => array(
						'enable'  => esc_html__( 'Enabled', 'alimo' ),
						'disable' => esc_html__( 'Disabled', 'alimo' ),
					),
					'std'     => 'disable',
				),

			),
		);

		/*
		 * POST META BOXES
		 */
		$meta_boxes[] = array(
			'title'    => esc_html__( 'Featured Image Replacement', 'alimo' ),
			'pages'    => array( 'post' ),
			'priority' => 'low',
			'context'  => 'side',
			'fields'   => array(
				array(
					'id'               => $prefix . 'featured_image_replacement',
					'name'             => esc_html__( 'Image Upload', 'alimo' ),
					'type'             => 'image_advanced',
					'force_delete'     => false,
					'max_file_uploads' => 1,
					'max_status'       => false,
					'desc' => esc_html__('Optional: You can upload an image here and it will be used instead of the featured image outisde the post in places like listings, search results, etc.', 'alimo'),
				),
			),
		);


		$meta_boxes[] = array(
			'title'    => esc_html__( 'Featured Video', 'alimo' ),
			'pages'    => array( 'post' ),
			'priority' => 'low',
			'context'  => 'side',
			'fields'   => array(
				array(
					'id'      => $prefix . 'video_provider',
					'name'    => esc_html__( 'Select video source', 'alimo' ),
					'type'    => 'select',
					'options' => array(
						'youtube' => esc_html__( 'Youtube', 'alimo' ),
						'vimeo'   => esc_html__( 'Vimeo', 'alimo' ),
					),
					'default' => 'youtube',
					'desc'    => wp_kses(__( '<b>Please make sure to select the correct video source, otherwise it won\'t work</b>', 'alimo' ),array('b'=> array()) ),
				),
				array(
					'name'    => esc_html__( 'Video ID', 'alimo' ),
					'id'      => $prefix . 'video_id',
					'type'    => 'text',
					'default' => 'sVzvRsl4rEM',
					'desc'    => wp_kses(__( 'If you add a video ID, the featured image will be overwritten with the video thumbnail<br>
                                       You can get the video ID from the URL of the video you want to add:<br>
                                       on Youtube : https://www.youtube.com/watch?v=<b style="color:darkred">XXXXXXXXXXX</b> (11 characters) <br>
                                       on Vimeo : https://vimeo.com/<b style="color: darkblue;">XXXXXXXXX</b> (9 characters)', 'alimo' ), array('br' => array(), 'b' => array('style' => array() ) ) ),
				),
			),
		);

		$meta_boxes[] = array(
			'title'    => esc_html__( 'Background Music', 'alimo' ),
			'pages'    => array( 'post' ),
			'priority' => 'low',
			'context'  => 'side',
			'fields'   => array(
				array(
					'name'             => esc_html__( 'Mp3 file', 'alimo' ),
					'id'               => $prefix . 'background_music',
					'type'             => 'file_advanced',
					'mime_type'        => 'audio/mpeg',
					'max_file_uploads' => 1,
					'max_status'       => false,
				),
				array(
					'desc'    => esc_html__( 'If enabled, the song will start playing automatically.', 'alimo' ),
					'name'    => esc_html__( 'Autoplay', 'alimo' ),
					'id'      => $prefix . 'background_music_autoplay',
					'type'    => 'select',
					'options' => array(
						'enable'  => esc_html__( 'Enabled', 'alimo' ),
						'disable' => esc_html__( 'Disabled', 'alimo' ),
					),
					'std'     => 'enable',
				),

			),
		);
		$alimo_data = get_option('alimo_data');
		$selected_accent = ( !empty($alimo_data['accent_color']) ? $alimo_data['accent_color'] : 'accent_one' );
		$accent_color = '#00EFCA';
		switch ( $selected_accent ) {
			case 'accent_one':
				break;
			case 'accent_two':
				$accent_color = '#2700FF';
				break;
			case 'accent_three':
				$accent_color = '#FF4B00';
				break;
			case 'accent_custom':
				$accent_color = ( ! empty( $alimo_data['custom_accent_color'] ) ) ? $alimo_data['custom_accent_color'] : '#F0C';
				break;
		}

		$meta_boxes[] = array(
			'title'    => esc_html__( 'Post Custom Options', 'alimo' ),
			'pages'    => array( 'post' ),
			'priority' => 'high',
			'context'  => 'normal',
			'fields'   => array(
				array(
					'id'      => $prefix . 'enable_overlay',
					'name'    => 'Custom Header Overlay',
					'desc'    => esc_html__( 'If enabled, it will override the settings from the admin panel.', 'alimo' ),
					'type'    => 'select',
					'options' => array(
						'enable'  => esc_html__( 'Enabled', 'alimo' ),
						'disable' => esc_html__( 'Disabled', 'alimo' ),
					),
					'std'     => 'disable',

				),
				array(
					'id'         => $prefix . 'post_header_overlay_color',
					'name'       => esc_html__( 'Color Overlay', 'alimo' ),
					'desc'       => esc_html__( 'Use the color picker to select the desired color overlay.', 'alimo' ),
					'type'       => 'color',
					'js_options' => array(
						'palettes' => [ '#125', '#459', '#78b', '#ab0', '#de3', '#f0f' ],
					),
					'std'        => $accent_color,
				),
				array(
					'id'         => $prefix . 'post_header_overlay_alpha',
					'name'       => esc_html__( 'Overlay Opacity', 'alimo' ),
					'desc'       => esc_html__( 'Remember that an opacity of 100% will hide the featured image.', 'alimo' ),
					'type'       => 'slider',
					'suffix'     => esc_html__( ' %', 'alimo' ),
					'js_options' => array(
						'min'  => 0,
						'max'  => 100,
						'step' => 1,
					),
					'std'        => 80,
				),
				array(
					'id'   => $prefix . 'featured_post',
					'name' => esc_html__( 'Featured post', 'alimo' ),
					'desc' => esc_html__( 'Highlight post as featured', 'alimo' ),
					'type' => 'checkbox',
					'std'  => false,
				),
				array(
					'id'   => $prefix . 'post_grayscale',
					'name' => esc_html__( 'Grayscale', 'alimo' ),
					'desc' => esc_html__( 'Enable the grayscale feature', 'alimo' ),
					'type' => 'checkbox',
					'std'  => false,
				),
			),
		);

		return $meta_boxes;
	}

}

Alimo_Meta_Boxes::get_instance();