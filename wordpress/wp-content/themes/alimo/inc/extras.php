<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package alimo
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function alimo_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	return $classes;
}
add_filter( 'body_class', 'alimo_body_classes' );


/**
 * Convert hex color string to rgb(a) string
 * @param $color
 * @param bool $opacity
 *
 * @return string
 */
function hex2rgba($color, $opacity = false) {

	$default = 'rgb(0,0,0)';

	//Return default if no color provided
	if(empty($color))
		return $default;

	//Sanitize $color if "#" is provided
	if ($color[0] == '#' ) {
		$color = substr( $color, 1 );
	}

	//Check if color has 6 or 3 characters and get values
	if (strlen($color) == 6) {
		$hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
	} elseif ( strlen( $color ) == 3 ) {
		$hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
	} else {
		return $default;
	}

	//Convert hexadec to rgb
	$rgb =  array_map('hexdec', $hex);

	//Check if opacity is set(rgba or rgb)
	if($opacity){
		if(abs($opacity) > 1)
			$opacity = 1.0;
		$output = 'rgba('.implode(",",$rgb).','.$opacity.')';
	} else {
		$output = 'rgb('.implode(",",$rgb).')';
	}

	//Return rgb(a) color string
	return $output;
}

/**
 * Core widgets styling changes
 */

// Categories list
function alimo_categories_postcount_filter ($variable) {
	$variable = str_replace('(', '<span class="post_count"> ', $variable);
	$variable = str_replace(')', ' </span>', $variable);
	return $variable;
}
add_filter('wp_list_categories','alimo_categories_postcount_filter');

/**
 * Class Alimo_Arrow_Walker_Nav_Menu
 */
class Alimo_Arrow_Walker_Nav_Menu extends Walker_Nav_Menu {
	function start_lvl(&$output, $depth = 0, $args = Array()) {
		$indent = str_repeat("\t", $depth);
		if('header_nav' == $args->theme_location ){
			$output .='<span class="toggle-submenu fa fa-angle-down"></span>';
		}
		$output .= "\n$indent<ul class=\"sub-menu\">\n";
	}
}