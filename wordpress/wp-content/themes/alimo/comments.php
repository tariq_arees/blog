<?php
/**
 * The template for displaying comments.
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package alimo
 */

	/***********************************************************************************************/
	/* If the post is password protected then display text and return */
	/***********************************************************************************************/
		if (post_password_required()) : ?>

			<p class="text-center">
				<?php
				esc_html_e( 'This post is password protected. Enter the password to view the comments.', 'alimo');
				return;
				?>
			</p>

		<?php endif;

	/***********************************************************************************************/
	/* If we have comments to display, we display them */
	/***********************************************************************************************/
			if (have_comments()) :  ?>

				<!-- ============== COMMENTS CONTAINER ============= -->
				<div class="comment-container">
					<div class="row">
                        <div class="col-xs-12">
                            <div class="comments-header clearfix">
                                <div class="col-xs-6 no-padding">
                                    <h2 id="comments" class="title-comments"><?php comments_number(esc_html__('No Comments', 'alimo'), esc_html__('1 Comment', 'alimo'), esc_html__('% Comments', 'alimo')); ?></h2>
                                </div>
                                <?php if(comments_open()): ?>
                                <div class="col-xs-6 no-padding text-right leave-reply">
                                    <a href="#the-comment" data-easing="easeInOutQuint" data-scroll="" data-speed="600" data-url="false"><?php esc_html_e('Leave a reply', 'alimo')?></a>
                                </div>
                                <?php endif; ?>
                            </div>

                            <ul class="comments">
                                <?php wp_list_comments('callback=alimo_comments'); ?>
                            </ul>

                            <?php if (get_comment_pages_count() > 1 && get_option('page_comments')) : ?>

                                <div class="comment-nav-section clearfix">

                                    <p class="fl"><?php previous_comments_link(__( '&larr; Older Comments', 'alimo')); ?></p>
                                    <p class="fr"><?php next_comments_link(__( 'Newer Comments &rarr;', 'alimo')); ?></p>

                                </div> <!-- end comment-nav-section -->

                            <?php endif; ?>
                        </div>
					</div>
				</div>

		<?php
		/***********************************************************************************************/
		/* If we don't have comments and the comments are closed, display a text */
		/***********************************************************************************************/

		elseif (!comments_open() && !is_page() && post_type_supports(get_post_type(), 'comments')) : ?>
            <p><?php esc_html__('Comments are closed','alimo');?></p>
		<?php endif;

	/***********************************************************************************************/
	/* Display the comment form */
	/***********************************************************************************************/
	?>

	<!-- ============== COMMENT RESPOND ============= -->
		<?php if (comments_open()) : ?>
			<div class="comment-form-theme" id="the-comment">
				<?php comment_form(); ?>
			</div>
		<?php endif; ?>
