<?php
/**
 * Alimo functions and definitions.
 *
 * @link https://codex.wordpress.org/Functions_File_Explained
 *
 * @package alimo
 */

/************************************************************/
/* Define Theme's Constants */
/************************************************************/
if ( ! defined( 'ALIMO_THEMEROOT' ) ) {
	define( 'ALIMO_THEMEROOT', get_template_directory_uri() );
}

if ( ! defined( 'ALIMO_THEMEDIR' ) ) {
	define( 'ALIMO_THEMEDIR', get_stylesheet_directory() );
}

if ( ! defined( 'ALIMO_IMAGES' ) ) {
	define( 'ALIMO_IMAGES', ALIMO_THEMEROOT . '/assets/img' );
}

if ( ! defined( 'LAYERBLEND_FRAMEWORK_DIR' ) ) {
	define( 'LAYERBLEND_FRAMEWORK_DIR', get_template_directory_uri() . '/admin/layerblend-redux/' );
}

if ( ! defined( 'LAYERBLEND_FRAMEWORK' ) ) {
	define( 'LAYERBLEND_FRAMEWORK', get_template_directory() . '/admin/layerblend-redux/' );
}

if ( ! function_exists( 'alimo_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function alimo_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on alimo, use a find and replace
		 * to change 'alimo' to the name of your theme in all the template files
		 */
		load_theme_textdomain( 'alimo', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'header_nav' => esc_html__( 'Header Navigation', 'alimo' )
		) );
		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );
	}
endif; // alimo_setup
add_action( 'after_setup_theme', 'alimo_setup' );


/**
 * Alimo custom featured image sizes
 */

add_image_size( 'alimo-featured-image-xl', 2000, 1125, true );
add_image_size( 'alimo-featured-image-l', 1536, 864, true );
add_image_size( 'alimo-featured-image', 1280, 720, true );
function alimo_max_srcset_image_width( $max_width ) {
	return 2000;
}
add_filter( 'max_srcset_image_width', 'alimo_max_srcset_image_width' );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails.
 */
function alimo_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
    $attr['sizes'] = '(max-width: 767px) 100vw, (max-width: 1920px) 50vw, 2000px';
	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'alimo_post_thumbnail_sizes_attr', 10, 3 );


/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function alimo_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'alimo' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here to appear in your sidebar', 'alimo' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	global $woocommerce;
	$alimo_data = get_option('alimo_data');
	$enable_footer = ( ! empty( $alimo_data['enable_footer'] ) ) ? $alimo_data['enable_footer'] : false;
	if ( $enable_footer ):
		register_sidebar( array(
				'name'          => esc_html__('Footer Column 1','alimo'),
				'id'            => 'footer-area-1',
				'description'   => esc_html__( 'Add widgets here to appear in the first footer column', 'alimo' ),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h4 class="widget-title">',
				'after_title'   => '</h4>'
			)
		);

		register_sidebar( array(
				'name'          => esc_html__('Footer Column 2','alimo'),
				'id'            => 'footer-area-2',
				'description'   => esc_html__( 'Add widgets here to appear in the second footer column', 'alimo' ),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h4 class="widget-title">',
				'after_title'   => '</h4>'
			)
		);


		$footer_columns = ( ! empty( $alimo_data['footer_columns'] ) ) ? $alimo_data['footer_columns'] : '4';

		if ( $footer_columns == '3' || $footer_columns == '4' ) {
			register_sidebar( array(
					'name'          => esc_html__('Footer Column 3','alimo'),
					'id'            => 'footer-area-3',
					'description'   => esc_html__( 'Add widgets here to appear in the third footer column', 'alimo' ),
					'before_widget' => '<section id="%1$s" class="widget %2$s">',
					'after_widget'  => '</section>',
					'before_title'  => '<h4 class="widget-title">',
					'after_title'   => '</h4>'
				)
			);
		}
		if ( $footer_columns == '4' ) {
			register_sidebar( array(
				'name'          => esc_html__('Footer Column 4','alimo'),
				'id'            => 'footer-area-4',
				'description'   => esc_html__( 'Add widgets here to appear in the fourth footer column', 'alimo' ),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h4 class="widget-title">',
				'after_title'   => '</h4>'
			) );
		}
		if ( $alimo_data['enable_widget_above_footer'] ) {
			register_sidebar( array(
					'name'          => esc_html__('Above Footer Area','alimo'),
					'id'            => 'above-footer-area',
					'description'   => esc_html__( 'Add widgets here to appear above the main footer. Recommended for Instagram widget', 'alimo' ),
					'before_widget' => '<section id="%1$s" class="widget %2$s">',
					'after_widget'  => '</section>',
					'before_title'  => '<h3>',
					'after_title'   => '</h3>'
				)
			);
		}
	endif;
	if ( $woocommerce ):
		register_sidebar( array(
				'name'          => esc_html__('WooCommerce Sidebar','alimo'),
				'id'            => 'woocommerce-sidebar',
				'description'   => esc_html__( 'Add widgets here to appear in the Shop Sidebar', 'alimo' ),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h4 class="widget-title">',
				'after_title'   => '</h4>'
			)
		);
	endif;
}

add_action( 'widgets_init', 'alimo_widgets_init' );


/**
 * Register Default Fonts
 */
function alimo_default_fonts_url() {
	$font_url = '';

	/*
	Translators: If there are characters in your language that are not supported
	by chosen font(s), translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Google font: on or off', 'alimo' ) ) {
		$font_url = add_query_arg( 'family', urlencode( 'Muli|Poppins:400,400italic,700italic,700&subset=latin,latin-ext' ), "//fonts.googleapis.com/css" );
	}

	return $font_url;
}


/**
 * Enqueue scripts and styles.
 */
function alimo_scripts() {

	$alimo_data = get_option('alimo_data');

	wp_enqueue_style( 'ALIMO_Fonts', alimo_default_fonts_url(), array(), '1.0.0' );
	wp_enqueue_style( 'style', get_stylesheet_uri() );

	if ( $alimo_data['accent_color'] == 'accent_one' ) {
	} else if ( $alimo_data['accent_color'] == 'accent_two' ) {
		wp_enqueue_style( 'ALIMO_Accent2', ALIMO_THEMEROOT . '/assets/css/colors-02.css' );
	} else if ( $alimo_data['accent_color'] == 'accent_three' ) {
		wp_enqueue_style( 'ALIMO_Accent3', ALIMO_THEMEROOT . '/assets/css/colors-03.css' );
	}


	/*
     * Alimo JS Plugins Bundle.
     * Uncomment this line to load a single file with all the JavaScript libraries
	 * !!! CAUTION !!! If you want to use a plugins.main.js, remember co COMMENT OUT the plugins loaded below to avoid double loading.
     * */

    //wp_enqueue_script( 'ALIMO_PluginsJS', ALIMO_THEMEROOT . '/assets/js/plugins.min.js', array('jquery'), '1.0', true );

    /*
     * Alimo JS Plugins Source
     * JavaScript Libraries used in the theme. Declared individually for easy dequeue.
     * !!! CAUTION !!! If you want to use above plugins.main.js, remember co COMMENT OUT the plugins loaded HERE to avoid double loading.
     * */

    wp_enqueue_script( 'headroom', ALIMO_THEMEROOT . '/assets/js_plugins/headroom.min.js', array('jquery'), '0.9.4', true );
    wp_enqueue_script( 'jquery-fancybox', ALIMO_THEMEROOT . '/assets/js_plugins/jquery.fancybox.min.js', array('jquery'), '3.2.5', true );
    wp_enqueue_script( 'jquery-fitvids', ALIMO_THEMEROOT . '/assets/js_plugins/jquery.fitvids.js', array('jquery'), '1.0', true );
    wp_enqueue_script( 'jquery-smoothscroll', ALIMO_THEMEROOT . '/assets/js_plugins/jquery.smooth-scroll.js', array('jquery'), '1.0', true );
    wp_enqueue_script( 'inview', ALIMO_THEMEROOT . '/assets/js_plugins/in-view.min.js', array('jquery'), '1.0', true );
    wp_enqueue_script( 'masonry', ALIMO_THEMEROOT . '/assets/js_plugins/masonry.pkgd.min.js', array('jquery'), '1.0', true );
    wp_enqueue_script( 'bootstrap-tooltip', ALIMO_THEMEROOT . '/assets/js_plugins/tooltip.js', array('jquery'), '1.0', true );
    /*
     * End Alimo JS Plugins Source
     * */

    /*
     * Initialise JS Plugins
     * This component is used for third party plugin initialisation.
     * Here you'll find the selectors and options/settings used with the various jQuery Libraries in Alimo
     * */
    wp_enqueue_script( 'ALIMO_InitPluginsJS', ALIMO_THEMEROOT . '/assets/js/init_plugins.js', array('jquery'), '1.1.0', true );


    /*
     * Alimo Main JS Components
     *
     * This is the unminified version of main.min.js and below are all the custom Alimo JavaScript Components included in it.
     *
     * These are not loaded by default and instead are bundled with main.js in one single file loaded below as main.min.js
     *
     * You have a gulpfile included with the theme that can be used to rebuild the main.min.js file if you make changes to any of the components.
     * In case you can't use gulp for some reason, you can edit the component you need and load ALL the files below, including main.js
     * All components are required for the theme to function properly.
     *
     * !!! Remember to comment out the  wp_enqueue_script( 'ALIMO_MainJS', ... ); after `End Alimo JS Components` to avoid double loading
     *
     * */
    // wp_enqueue_script('ALIMO_MODULE_background_audio',ALIMO_THEMEROOT.'/assets/js_modules/background_audio.js',array('jquery'),'1.0',true);
    // wp_enqueue_script('ALIMO_MODULE_cookie',ALIMO_THEMEROOT.'/assets/js_modules/cookie.js',array('jquery'),'1.0',true);
    // wp_enqueue_script('ALIMO_MODULE_mobile_nav',ALIMO_THEMEROOT.'/assets/js_modules/mobile_nav.js',array('jquery'),'1.0',true);
    // wp_enqueue_script('ALIMO_MODULE_reading_mode',ALIMO_THEMEROOT.'/assets/js_modules/reading_mode.js',array('jquery'),'1.0',true);
    // wp_enqueue_script('ALIMO_MODULE_reading_progress',ALIMO_THEMEROOT.'/assets/js_modules/reading_progress.js',array('jquery'),'1.0',true);
    // wp_enqueue_script('ALIMO_MODULE_share_widget',ALIMO_THEMEROOT.'/assets/js_modules/share_widget.js',array('jquery'),'1.0',true);
    // wp_enqueue_script('ALIMO_MODULE_sidebar',ALIMO_THEMEROOT.'/assets/js_modules/sidebar.js',array('jquery'),'1.0',true);
    // wp_enqueue_script('ALIMO_MODULE_woocommerce',ALIMO_THEMEROOT.'/assets/js_modules/woocommerce.js',array('jquery'),'1.0',true);
	// wp_enqueue_script('ALIMO_MainJS', ALIMO_THEMEROOT . '/assets/js/main.js', array('jquery'), '1.0', true );
    /*
     * End Alimo JS Components
     * */

    /*
     * Alimo main.min.js - main theme custom javascript file. If using Alimo components above, comment out the line below
     * */
    wp_enqueue_script( 'ALIMO_MainJS', ALIMO_THEMEROOT . '/assets/js/main.min.js', array('jquery'), '1.1.0', true );


    /*
     * Slick Slider dependencies are loaded separately only if the above the footer widget area is enabled
     * */
	if ( $alimo_data['enable_widget_above_footer'] ) {
		wp_enqueue_script( 'SlickJS', ALIMO_THEMEROOT . '/assets/js_plugins/slick.min.js', array(), '1.0', true );
		wp_enqueue_style( 'SlickCSS', ALIMO_THEMEROOT . '/assets/css/slick.css', array(), '1.6' );
	}
	/*
	 * Selection sharer plugin dependencies are loaded only on the post page and only if the Quote sharer is enabled
	 * (Admin > Posts > Social sharing)
	 * */
	if ( is_singular( 'post' && $alimo_data['social-quote-sharer']) ) {
		wp_enqueue_script( 'ALIMO_SelectionSharerJS', ALIMO_THEMEROOT . '/assets/js_plugins/selection-sharer.js', array(), '1.0', true );
		wp_enqueue_style( 'ALIMO_SelectionSharerCSS', ALIMO_THEMEROOT . '/assets/css/selection-sharer.css' );
	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

if ( ! is_admin() ) {
	add_action( 'wp_enqueue_scripts', 'alimo_scripts' );
}


/**
 * Require meta-box custom fields.
 *
 * Meta-box plugin is used for the custom content boxes from the posts, pages and other places
 * in the admin dashboard.
 */
require_once( 'inc/Alimo_Meta_Boxes.php' );

/**
 * Include Redux Framework
 *
 * This is used for the Options Panel of the theme.
 */

if(!get_option('alimo_data')){
    require (LAYERBLEND_FRAMEWORK.'default-options.php');
}

if ( ! isset( $redux_demo ) && file_exists( LAYERBLEND_FRAMEWORK . 'options-init.php' ) ) {
	require_once( LAYERBLEND_FRAMEWORK . 'options-init.php' );
}


if ( is_admin() ) {
    /**
    * Removes the redux-framework plugin info page
    */
    function alimo_remove_redux_menu() {
		remove_submenu_page( 'tools.php', 'redux-about' );
	}
	add_action( 'admin_menu', 'alimo_remove_redux_menu', 12 );

    function alimo_add_admin_css(){
        wp_enqueue_style('alimo-admin-css', LAYERBLEND_FRAMEWORK_DIR.'css/wp-admin-layerblend.css');
    }
    add_action('admin_enqueue_scripts','alimo_add_admin_css');

	/**
	 * Remove plugin flag from redux. Get rid of redirect
	 *
	 * @since 1.1.0
	 */
	function alimo_remove_as_plugin_flag() {
		if ( class_exists('ReduxFrameworkPlugin') ) {
			ReduxFramework::$_as_plugin = FALSE;
		}
	}
	add_action( 'redux/construct', 'alimo_remove_as_plugin_flag' );

	function alimo_removeDemoModeLink() { // Be sure to rename this function to something more unique
		if ( class_exists('ReduxFrameworkPlugin') ) {
			remove_filter( 'plugin_row_meta', array( ReduxFrameworkPlugin::get_instance(), 'plugin_metalinks'), null, 2 );
		}
		if ( class_exists('ReduxFrameworkPlugin') ) {
			remove_action('admin_notices', array( ReduxFrameworkPlugin::get_instance(), 'admin_notices' ) );
		}
	}
	add_action('init', 'alimo_removeDemoModeLink');


  function alimo_post_metabox_scripts() {
      if(!empty($_GET['post'])){
        wp_register_script(
          'layerblend-admin-custom-js',
          LAYERBLEND_FRAMEWORK_DIR . 'js/custom-admin-panel.js'
        );
        wp_enqueue_script( 'layerblend-admin-custom-js' );
      }
  }
  add_action( 'admin_enqueue_scripts', 'alimo_post_metabox_scripts' );
}


/**
 * TGM Activation Plugin
 *
 * This plugin helps to call all the required plugins for the theme.
 */
require_once( get_template_directory() . '/admin/tgm/tgm-init.php' );


/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


if ( class_exists( 'null_instagram_widget' ) ) {

	function alimo_wp_insta_widget_class() {
		$class = 'instagram-widget-item';

		return $class;
	}

	add_filter( 'wpiw_item_class', 'alimo_wp_insta_widget_class' );

}



/**
 * Customise the comments section
 *
 * @param $comment
 * @param $args
 * @param $depth
 */
function alimo_comments( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
    $depth_escaped = intval($depth);
	if ( get_comment_type() == 'pingback' || get_comment_type() == 'trackback' ) : ?>

        <li id="comment-<?php comment_ID(); ?>" class="row">
            <div <?php comment_class( 'clearfix' ); ?>>
                <div class="right-section col-xs-10">
                    <span class="comment-author"><?php esc_html_e( 'Pingback:', 'alimo' ); ?></span>
                    <span class="comment-time"><?php comment_date(); ?> at <?php comment_time(); ?></span>
                    <div class="comment-text">
                        <p><?php comment_author_link(); ?></p>
                    </div>
                </div>
            </div>
        </li>
	<?php endif; ?>

	<?php if ( get_comment_type() == 'comment' ) : ?>
        <li id="comment-<?php comment_ID(); ?>">
        <div <?php comment_class( 'row' ); ?>>
            <div class="left-section col-xs-3 col-sm-2">
				<?php
				$avatar_size = 80;
				echo get_avatar( $comment, $avatar_size );
				?>
            </div>

            <div class="right-section col-xs-9 col-sm-10">
                <div class="reply">
					<?php comment_reply_link( array_merge( $args, array(
						'depth'      => $depth_escaped,
						'max_depth'  => $args['max_depth'],
						'reply_text' => 'reply'
					) ) );
					?>
                </div>
                <span class="comment-author"><?php comment_author_link(); ?></span>
                <div class="comment-time"><?php comment_date(); ?> at <?php comment_time(); ?> </div>
                <div class="comment-text">
					<?php if ( $comment->comment_approved == '0' ) : ?>
                        <p class="awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'alimo' ); ?></p>
					<?php endif; ?>
					<?php comment_text(); ?>
                </div>
            </div>
        </div>
	<?php endif;
}

function alimo_custom_comment_form( $defaults ) {

	$defaults['cancel_reply_before']  = '';
	$defaults['cancel_reply_after']   = '';
	$defaults['comment_notes_before'] = '';
	$defaults['comment_notes_after']  = '';
	$defaults['id_form']              = 'comment-form';
	$defaults['comment_field']        = '<textarea name="comment" id="comment" placeholder="' . esc_html__( 'Comment', 'alimo' ) . '" rows="7"></textarea>';

	return $defaults;
}

function alimo_custom_comment_fields() {
	$commenter = wp_get_current_commenter();
	$req       = get_option( 'require_name_email' );
	$aria_req  = ( $req ? " required " : '' );

	$fields = array(
		'author' => '<ul class="comment-form-inputs"><li>' .
		            '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" ' . $aria_req .
		            ' placeholder="' . esc_html__( 'Name ', 'alimo' ) . ( ! $req ? esc_html__( '(optional)', 'alimo' ) : '' ) . '" /></li>',
		'email'  => '<li>' .
		            '<input id="email" name="email" type="text" value="' . esc_attr( $commenter['comment_author_email'] ) . '" ' . $aria_req .
		            ' placeholder="' . esc_html__( 'Email ', 'alimo' ) . ( ! $req ? esc_html__( '(optional)', 'alimo' ) : '' ) . '"/>' .
		            '</li>',
		'url'    => '<li>' .
		            '<input id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) .
		            '" placeholder="' . esc_html__( 'Website (optional)', 'alimo' ) . '" />' .
		            '</li></ul>'
	);

	return $fields;
}

function alimo_custom_comment_form_submit_button( $button ) {
	$button = '<button name="submit" class="submit-comment" type="submit">'.esc_html__('Post Comment','alimo').'</button>';

	return $button;
}

// Comments filters
add_filter( 'comment_form_defaults', 'alimo_custom_comment_form' );
add_filter( 'comment_form_default_fields', 'alimo_custom_comment_fields' );
add_filter( 'comment_form_submit_button', 'alimo_custom_comment_form_submit_button' );

function alimo_custom_query_vars_filter( $vars ) {
	$vars[] = 'reading_mode';

	return $vars;
}

add_filter( 'query_vars', 'alimo_custom_query_vars_filter' );

/**
 * Change excerpt length
 * @param int $length
 *
 * @return int
 */
function alimo_excerpt_length( $length ) {
    $alimo_data = get_option('alimo_data');
	$user_len = intval( $alimo_data['excerpt_length'] );
	$length   = ( $user_len ) ? $user_len : 30;

	return $length;
}

add_filter( 'excerpt_length', 'alimo_excerpt_length', 999 );

/**
 * Filter the excerpt "read more" string.
 *
 * @param string $more "Read more" excerpt string.
 *
 * @return string (Maybe) modified "read more" excerpt string.
 */
function alimo_excerpt_more( $more ) {
    $alimo_data = get_option('alimo_data');
	$more = ( $alimo_data['excerpt_more'] ) ? ' ' . sanitize_text_field( $alimo_data['excerpt_more'] ) : '...';

	return $more;
}

add_filter( 'excerpt_more', 'alimo_excerpt_more' );

function alimo_relative_time() {
	$post_date = get_the_time( 'U' );
	$delta     = current_time( 'timestamp' ) - $post_date;
	if ( $delta < 60 ) {
		$formatted_time = esc_html__('Less than a minute ago','alimo');
	} elseif ( $delta > 60 && $delta < 120 ) {
		$formatted_time = esc_html__('About a minute ago','alimo');
	} elseif ( $delta > 120 && $delta < ( 60 * 60 ) ) {
		$formatted_time = sprintf(esc_html__('%s minutes ago','alimo'),strval( round( ( $delta / 60 ), 0 ) ) );
	} elseif ( $delta > ( 60 * 60 ) && $delta < ( 120 * 60 ) ) {
		$formatted_time = esc_html__('About an hour ago','alimo');
	} elseif ( $delta > ( 120 * 60 ) && $delta < ( 24 * 60 * 60 ) ) {
		$formatted_time = sprintf(esc_html__('%s hours ago','alimo'),strval( round( ( $delta / 3600 ), 0 ) ));
	} else {
		$formatted_time = date_i18n( 'j\<\s\u\p\>S\<\/\s\u\p\> M y g:i a' );
	}

	return $formatted_time;
}


/**
 * @param $comment_ID
 *
 * @return false|int|string
 */
function alimo_relative_comment_time( $comment_ID ) {
	$post_date = get_comment_date( 'U', $comment_ID );
	$delta     = current_time( 'timestamp' ) - $post_date;
	if ( $delta < 60 ) {
		$formatted_time = esc_html__('Less than a minute ago','alimo');
	} elseif ( $delta > 60 && $delta < 120 ) {
		$formatted_time = esc_html__('About a minute ago','alimo');
	} elseif ( $delta > 120 && $delta < ( 60 * 60 ) ) {
		$formatted_time = sprintf(esc_html__('%s minutes ago','alimo'),strval( round( ( $delta / 60 ), 0 ) ) );
	} elseif ( $delta > ( 60 * 60 ) && $delta < ( 120 * 60 ) ) {
		$formatted_time = esc_html__('About an hour ago','alimo');
	} elseif ( $delta > ( 120 * 60 ) && $delta < ( 24 * 60 * 60 ) ) {
		$formatted_time = sprintf(esc_html__('%s hours ago','alimo'),strval( round( ( $delta / 3600 ), 0 ) ));
	} else {
		$formatted_time = date_i18n( 'j\<\s\u\p\>S\<\/\s\u\p\> M y g:i a' );
	}

	return $formatted_time;
}


/**
 * @param $post
 *
 * @return float|string
 */
function alimo_reading_time( $post ) {
	$avg_wpm      = 275;
	$content      = get_post_field( 'post_content', $post->ID );
	$word_count   = str_word_count( strip_tags( $content ) );
	$reading_time = floor( $word_count / $avg_wpm );
	if ( $reading_time <= 0 ) {
		return '1';
	} else {
		return $reading_time;
	}

}

/**
 * Max content width for oEmbeds and images added to posts.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 870;
}

/**
 * Helper function to retrieve the thumbnail for posts with video instead of featured image.
 *
 * @param $videoID
 * @param $videoProvider
 * @param string $quality
 *
 * @return string
 */
function alimo_get_video_thumbnail( $videoID, $videoProvider, $thumbQuality = 'maxres' ) {
	$url           = $backup_url = $thumbnail = '';
	$video_ID       = esc_html( $videoID );
	$video_provider = esc_html( $videoProvider );
	$thumb_quality       = esc_html( $thumbQuality );

	if ('youtube' == $video_provider){
	    $available_qualities = array('maxres','hq','mq','sd');
	    foreach ($available_qualities as $quality){
		    $url        = "https://img.youtube.com/vi/{$video_ID}/{$quality}default.jpg";
		    $response = wp_remote_get( $url );
		    if(is_wp_error($response)){
			    //request can't be performed
			    break;
		    }
		    if('200' == wp_remote_retrieve_response_code($response)){
			    //request succeeded
			    $thumbnail = $url;
			    break;
		    }
		    if('404' == wp_remote_retrieve_response_code($response)){
			    //request succeeded and link not found
			    continue;
		    }
        }
	    return $thumbnail;
    }
    elseif ('vimeo' == $video_provider){
		$url = "https://vimeo.com/api/oembed.json?url=https%3A//vimeo.com/" . $video_ID;
		$response = wp_remote_get( $url );
		if(is_wp_error($response)){
			//request can't be performed
			return false;
		}
	    if('200' == wp_remote_retrieve_response_code($response)){
		    //request succeeded and link not found
		    $r_body = json_decode($response['body']);
		    $thumbnail = $r_body->thumbnail_url;
	    }
		if('404' == wp_remote_retrieve_response_code($response)){
			//request succeeded and link not found
			return false;
		}
	    return $thumbnail;
    }

	return $thumbnail;
}

/*
 * Enable WooCommerce support
 */
add_action( 'after_setup_theme', 'alimo_woocommerce_support' );
function alimo_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}

/*
 * Wrap the default WooCommerce elements in Alimo's containers
 */
function alimo_woocommerce_wrapper_start() {
	$alimo_data = get_option('alimo_data');
	$woo_sidebar_enabled  = FALSE;
	if(!empty($alimo_data['woo-enable-sidebar'])){
	    $woo_sidebar_enabled = $alimo_data['woo-enable-sidebar'];
	    if(is_product() && $woo_sidebar_enabled ){
	        $woo_sidebar_enabled = $alimo_data['woo-enable-sidebar-product'];
        }
    }
	$woo_sidebar_position = ( !empty($alimo_data['woo-sidebar-position']) && $alimo_data['woo-sidebar-position'] == 'left' ) ? 'left-sidebar' : 'right-sidebar';
    $content_class =  ($woo_sidebar_enabled) ? "col-sm-9 {$woo_sidebar_position}" : "col-sm-12 default-page no-sidebar";
	if(is_product() && !$woo_sidebar_enabled ){
		$content_class =  "col-sm-12 no-sidebar";
	}
    echo '<div class="main-content"><div class="container"><div class="row">';
	($woo_sidebar_enabled) ? get_sidebar('woocommerce'): NULL;
    echo '<div class="' . esc_attr($content_class) . '">';
}

function alimo_woocommerce_wrapper_end() {
	echo '</div></div></div></div>';
}

remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );

add_action( 'woocommerce_before_main_content', 'alimo_woocommerce_wrapper_start', 10 );
add_action( 'woocommerce_after_main_content', 'alimo_woocommerce_wrapper_end', 10 );



/*
 * Enable the WooCommerce mini-cart quantity update when a product is added to cart.
 */
add_filter( 'woocommerce_add_to_cart_fragments', 'alimo_woocommerce_header_add_to_cart_fragment' );

function alimo_woocommerce_header_add_to_cart_fragment( $fragments ) {
	global $woocommerce;
	$products_in_cart = $woocommerce->cart->cart_contents_count;
	ob_start(); ?>
    <a class="cart-contents <?php echo ( $products_in_cart > 0 ) ? 'cart-full' : 'cart-empty'; ?>"
       href="<?php echo esc_url(wc_get_cart_url()); ?>">
        <div class="cart-icon-wrap">
            <i class="lbli lbli-cart-o">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16.782 16.34" width="16.34">
                    <path id="cart"
                          d="M12.581 16.34H4.202a1.254 1.254 0 0 1-.8-.509C1.96 14.246-.171 9.125.009 7.123c.208-2.266 1.1-2.514 2.955-2.514a4.876 4.876 0 0 1 1.733-3.271 5.775 5.775 0 0 1 7.392 0 4.872 4.872 0 0 1 1.73 3.271c1.852 0 2.747.248 2.953 2.514.182 2-1.948 7.123-3.394 8.707a1.255 1.255 0 0 1-.797.51zM4.192 6.289c-1.434 0-2.127.177-2.286 1.8a7.783 7.783 0 0 0 .786 3.084A11.784 11.784 0 0 0 4.437 14.2c.28.309.519.465.712.465h6.486c.192 0 .432-.156.711-.465a11.777 11.777 0 0 0 1.746-3.032 7.786 7.786 0 0 0 .785-3.085c-.158-1.618-.85-1.8-2.285-1.8zm4.2-4.613a3.821 3.821 0 0 0-3.824 2.933h7.649a3.817 3.817 0 0 0-3.824-2.933z"/>
                </svg>
            </i>
            <div class="product-count-badge"><span><?php echo esc_html($woocommerce->cart->cart_contents_count); ?> </span></div>
        </div>
    </a>
	<?php

	$fragments['a.cart-contents'] = ob_get_clean();

	return $fragments;
}

/* show minicart in checkout */
add_filter( 'woocommerce_widget_cart_is_hidden', '__return_false', 40, 0 );


/*
 * Change WooCommerce default elements order on the product page
 */
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 25 );

/*
 * Change default WooCommerce products per page.
 * @param $number_of_products
 */
function alimo_new_loop_shop_per_page( $number_of_products = 12 ) {

	return $number_of_products;
}
add_filter( 'loop_shop_per_page', 'alimo_new_loop_shop_per_page', 20 );


/* Visual Composer : Enable bundled with theme. Required for updating */
if ( defined( 'WPB_VC_VERSION' ) ) {
  vc_set_as_theme();
}
add_filter('widget_text','do_shortcode');
add_filter('newsletter_enqueue_style', '__return_false');