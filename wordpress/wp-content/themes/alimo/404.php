<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package alimo
 */

get_header();

$alimo_data = get_option('alimo_data');
$sidebar_enabled     = ( $alimo_data['enable-sidebar'] == true &&  is_active_sidebar('sidebar-1')  ) ? true : false;
$sidebar_always_open = ( $alimo_data['sidebar-always-open'] == true ) ? true : false;
$sidebar_position = ($alimo_data['sidebar-position'] == 'left') ? 'left-sidebar':'right-sidebar';

$content_class =  ( $sidebar_enabled ) ? ( $sidebar_always_open ) ? "col-sm-9 {$sidebar_position}" : "col-sm-12 " : "col-sm-12";
?>

	<div class="main-content error-404 not-found">
        <div class="container">
            <div class="row">
                <div class="default-page <?php echo esc_attr($content_class); ?>">
                    <h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'alimo' ); ?></h1>
                    <div class="page-content">
                        <p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try a search?', 'alimo' ); ?></p>
                        <div class="row">
                            <div class="text-center col-sm-12">
                                <div class="glitch-fof" data-text="404"><?php esc_html_e( '404', 'alimo');?></div>
                            </div>
                        </div>
                    </div><!-- .page-content -->
                </div><!-- .error-404 -->
	            <?php ( $sidebar_enabled && $sidebar_always_open ) ? get_sidebar() : null; ?>
            </div>
		</div><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
