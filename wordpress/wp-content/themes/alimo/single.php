<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package alimo
 */

get_header();
$alimo_data = get_option('alimo_data');
$prefix = Alimo_Meta_Boxes::get_instance()->prefix;

?>

<section class="main-content">

	<?php
	$background_music_autoplay = (function_exists('rwmb_meta')) ? rwmb_meta( "{$prefix}background_music_autoplay", $args = array(), get_the_ID() ) : null;
	$background_music_arr          = (function_exists('rwmb_meta')) ? is_array(current(rwmb_meta( "{$prefix}background_music", "type=file" ))) ? current(rwmb_meta( "{$prefix}background_music", "type=file" )) : null : null;
	$background_music = (!empty($background_music_arr)) ? $background_music_arr['url'] : null;
	$file_mime_type   = wp_check_filetype( $background_music );
	$enable_autoplay = FALSE; // This gets set to true if it's enabled in the post and if the user hasnt previously paused a song
	/* check if wrong file type was added and show a warning to the admin */
	if ( ! empty( $background_music ) && 'audio/mpeg' != $file_mime_type['type'] && current_user_can('edit_posts') ):
		echo "<div class=\"alert alert-info\">"
		     . esc_html__( "Please make sure to upload an MP3 file. If you don't see a file in the background music metabox, just click update post one more time and this message will disappear.", 'alimo' )
		     . "</div>";
	endif;

	/* Check if background music is present and set to auto play. If it wasn't previously paused by the user, enable auto play */
	if ( ! empty( $background_music ) && 'audio/mpeg' == $file_mime_type['type'] && $background_music_autoplay == 'enable' && isset($_COOKIE['auto-play-audio']) && 'pause' != $_COOKIE['auto-play-audio'] ):
      $enable_autoplay = TRUE;
    endif;
	/* Add background audio and playback controls */
	if ( ! empty( $background_music ) && 'audio/mpeg' == $file_mime_type['type'] ) : ?>
        <div class="single-article-audio-bg">
            <span id="count_num">3</span>
            <i class="fa fa-volume-up"></i>
            <audio src="<?php echo esc_url($background_music); ?>" loop <?php if(TRUE == $enable_autoplay) echo esc_attr('autoplay'); ?>></audio>
        </div>
	<?php endif; ?>

	<?php
	/* If reading progress bar is enabled, add it to the page */
	if ( $alimo_data['reading-progress-bar'] ): ?>
        <div class="reading-position"></div>
	<?php endif; ?>
	<?php
	/* If Reading Mode is enabled, show the controls */
	if ( $alimo_data['reading-mode'] == true ): ?>
        <div class="reading-mode">
            <svg id="rm-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32">
                <path id="upper-dash" d="M18.29 10h5.273a1 1 0 0 1 0 2H18.29a1 1 0 0 1 0-2z"/>
                <path id="middle-dash" d="M18.29 16h7.273a1 1 0 0 1 0 2H18.29a1 1 0 0 1 0-2z"/>
                <path id="lower-dash" d="M10.29 22h11.7a1 1 0 0 1 0 2h-11.7a1 1 0 0 1 0-2z"/>
                <path id="letter"
                      d="M9.688 13.304v-2.41c.915 0 1.455-.005 2.155 0 .671.028 1.219.276 1.2 1.226 0 1.155-.9 1.18-1.323 1.184zm-1.883-3.149v7.838h1.883v-3.344h1.882a1.487 1.487 0 0 1 .985.279c.558.482.481 1.792.571 2.311a2.466 2.466 0 0 0 .171.756h1.883c-.418-.527-.266-1.788-.435-2.6a1.666 1.666 0 0 0-1.172-1.425 2.128 2.128 0 0 0 1.355-2.171 2.35 2.35 0 0 0-2.481-2.369H8.535c-.919-.008-.704.374-.73.725z"/>
                <path id="ring"
                      d="M16 32a16 16 0 1 1 16-16 16.018 16.018 0 0 1-16 16zm0-30.5A14.5 14.5 0 1 0 30.5 16 14.516 14.516 0 0 0 16 1.5z"/>
            </svg>
        </div>
        <div class="reading-mode-container">
            <div class="reading-options">
                <div class="reading-options-trigger">
                    <svg id="rm-settings" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32">
                        <rect id="right-top-bar" width="2.15" height="4.31" rx="1.075" transform="translate(18.69 8)"/>
                        <rect id="right-bottom-bar" width="2.15" height="7.54" rx="1.075"
                              transform="translate(18.69 16.62) rotate(180 1.075 3.77)"/>
                        <path id="right-control"
                              d="M19.77 11.23a3.231 3.231 0 1 1-3.231 3.232 3.235 3.235 0 0 1 3.231-3.232zm0 5.385a2.154 2.154 0 1 0-2.154-2.153 2.156 2.156 0 0 0 2.154 2.153z"/>
                        <rect id="left-top-bar" width="2.15" height="7.54" rx="1.075" transform="translate(11.25 8)"/>
                        <rect id="left-bottom-bar" width="2.15" height="4.31" rx="1.075"
                              transform="translate(11.25 19.85) rotate(180 1.075 2.155)"/>
                        <path id="left-control"
                              d="M12.231 20.924A3.235 3.235 0 0 1 9 17.692a3.234 3.234 0 0 1 3.23-3.23 3.234 3.234 0 0 1 3.23 3.23 3.235 3.235 0 0 1-3.229 3.232zm0-5.385a2.156 2.156 0 0 0-2.154 2.154 2.157 2.157 0 0 0 2.154 2.155 2.157 2.157 0 0 0 2.154-2.155 2.156 2.156 0 0 0-2.154-2.154z"/>
                        <path id="ring"
                              d="M16 32a16 16 0 1 1 16-16 16.018 16.018 0 0 1-16 16zm0-30.5A14.5 14.5 0 1 0 30.5 16 14.516 14.516 0 0 0 16 1.5z"/>
                    </svg>
                </div>
                <div class="reading-options-container">
                    <div class="ff-options clearfix">
                        <div class="font-box active sans-serif">
                            <p class="control-title">Sans-serif</p>
                            <p class="typeface">Aa</p>
                        </div>
                        <div class="font-box serif">
                            <p class="control-title">Serif</p>
                            <p class="typeface">Aa</p>
                        </div>
                    </div>
                    <div class="fz-options clearfix">
                        <div class="font-size">
                            <p class="control-title">Font size</p>
                            <span class="control increase">+</span>
                            <span class="control decrease">-</span>
                        </div>
                        <div class="line-height">
                            <p class="control-title">Line height</p>
                            <span class="control increase">+</span>
                            <span class="control decrease">-</span>
                        </div>
                    </div>
                    <div class="cs-options clearfix">
                        <div class="cs-option active light"><?php esc_html_e( 'Light', 'alimo' ); ?></div>
                        <div class="cs-option dark"><?php esc_html_e( 'Dark', 'alimo' ); ?></div>
                        <div class="cs-option sepia"><?php esc_html_e( 'Sepia', 'alimo' ); ?></div>
                    </div>
                </div>
            </div>
            <div class="container-fluid reading-mode-content-container sans-serif size-2 lineHeight-2" data-size="2" data-line-height="2">
                <div class="row">
                    <div class="col-md-9 center-block col-sm-12">
                        <div id="reading-mode-article-wrapper">
                            <h2 class="the-title"></h2>
                            <div class="the-content"></div>
                            <div class="posts-navigation clearfix">
                              <?php $prev_post = get_adjacent_post(); ?>
                              <?php if ( is_a( $prev_post, 'WP_Post' ) ) :
                                $prev_featured_image = get_the_post_thumbnail_url( $prev_post->ID, 'thumbnail' );
                                ?>
                                  <div class="col-md-6 pull-left">
                                      <a class="prev-post" href="<?php echo get_permalink( $prev_post->ID ); ?>?reading_mode=enabled">
                                          <div class="post prev <?php echo (empty($prev_featured_image)) ?'no-thumb':'thumb'?>">
                                            <?php if(!empty($prev_featured_image)): ?>
                                                <span class="next-prev-thumb" style="background-image: url(<?php echo esc_url($prev_featured_image); ?>)"></span>
                                            <?php endif; ?>
                                              <span class="nav-label"><?php esc_html_e( 'prev post', 'alimo' ); ?></span>
                                              <h4 class="post-title"><?php echo get_the_title( $prev_post->ID ); ?></h4>
                                          </div>
                                      </a>
                                  </div>
                              <?php endif; ?>
                              <?php $next_post = get_adjacent_post( false, '', false ); ?>
                              <?php if ( is_a( $next_post, 'WP_Post' ) ) :
                                $next_featured_image = get_the_post_thumbnail_url( $next_post->ID, 'thumbnail' );
                                ?>
                                  <div class="col-md-6 pull-right">
                                      <a class="next-post" href="<?php echo get_permalink( $next_post->ID ); ?>?reading_mode=enabled">
                                          <div class="post next <?php echo (empty($next_featured_image)) ?'no-thumb':'thumb'?>">
                                              <span class="nav-label"><?php esc_html_e( 'next post', 'alimo' ); ?></span>
                                              <h4 class="post-title"><?php echo get_the_title( $next_post->ID ); ?></h4>
                                            <?php if(!empty($next_featured_image)): ?>
                                                <span class="next-prev-thumb" style="background-image: url(<?php echo esc_url($next_featured_image); ?>)"></span>
                                            <?php endif; ?>
                                          </div>
                                      </a>
                                  </div>
                              <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	<?php endif; ?>

	<?php while ( have_posts() ) : the_post(); ?>
		<?php get_template_part( 'template-parts/content', 'single' ); ?>
        <div class="container article-footer" id="alimo-article-footer">
            <div class="row">
                <div class="col-md-12 posts-navigation clearfix">
					<?php $prev_post = get_adjacent_post(); ?>
					<?php if ( is_a( $prev_post, 'WP_Post' ) ) :
					    $prev_featured_image = get_the_post_thumbnail_url( $prev_post->ID, 'thumbnail' );
					    ?>
                        <div class="col-md-6 pull-left">
                            <a class="prev-post" href="<?php echo get_permalink( $prev_post->ID ); ?>">
                                <div class="post prev <?php echo (empty($prev_featured_image)) ?'no-thumb':'thumb'?>">
						            <?php if(!empty($prev_featured_image)): ?>
                                    <span class="next-prev-thumb" style="background-image: url(<?php echo esc_url($prev_featured_image); ?>)"></span>
						            <?php endif; ?>
                                    <span class="nav-label"><?php esc_html_e( 'prev post', 'alimo' ); ?></span>
                                    <h4 class="post-title"><?php echo get_the_title( $prev_post->ID ); ?></h4>
                                </div>
                            </a>
                        </div>
					<?php endif; ?>

					<?php $next_post = get_adjacent_post( false, '', false ); ?>
					<?php if ( is_a( $next_post, 'WP_Post' ) ) :
					    $next_featured_image = get_the_post_thumbnail_url( $next_post->ID, 'thumbnail' );
					    ?>
                        <div class="col-md-6 pull-right">
                            <a class="next-post" href="<?php echo get_permalink( $next_post->ID ); ?>">
                                <div class="post next <?php echo (empty($next_featured_image)) ?'no-thumb':'thumb'?>">
                                    <span class="nav-label"><?php esc_html_e( 'next post', 'alimo' ); ?></span>
                                    <h4 class="post-title"><?php echo get_the_title( $next_post->ID ); ?></h4>
                                    <?php if(!empty($next_featured_image)): ?>
                                    <span class="next-prev-thumb" style="background-image: url(<?php echo esc_url($next_featured_image); ?>)"></span>
                                    <?php endif; ?>
                                </div>
                            </a>
                        </div>
					<?php endif; ?>
                </div>
            </div>

	        <?php if ( 1 == $alimo_data['comments-display']) : ?>
                <div class="row comments-section">

                <div class="center-block col-md-8">
						<?php
						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) {
							if ( ! empty( $alimo_data['use-facebook-comments'] ) && TRUE == $alimo_data['use-facebook-comments'] ) {
								echo '<div class="fb-comments" data-href="' . get_the_permalink() . '" data-numposts="5"></div>';
							} else {
								comments_template();
							}

						};
						?>
                </div>
            </div>
            <?php endif; ?>

        </div>
	<?php endwhile; // End of the loop. ?>
</section>
<?php get_footer(); ?>