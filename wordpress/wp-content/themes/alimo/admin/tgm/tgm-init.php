<?php

/**
 * TGM Init Class
 */
require_once get_template_directory() . '/admin/tgm/class-tgm-plugin-activation.php';

function alimo_register_required_plugins() {

	$plugins = array(
		array(
			'name'               => 'Alimo Options Panel',
			'slug'               => 'redux-framework',
			'required'           => TRUE,
			'force_activation'   => TRUE,
			'force_deactivation' => TRUE,
		),
		array(
			'name'               => 'Alimo Shortcodes',
			'slug'               => 'alimo-shortcodes',
			'source'             => get_template_directory() . '/inc/plugins/alimo-shortcodes.zip',
			'version'            => '1.0',
			'required'           => TRUE,
			'force_activation'   => FALSE,
			'force_deactivation' => FALSE,
		),
		array(
			'name'               => 'Alimo Widgets',
			'slug'               => 'alimo-widgets',
			'source'             => get_template_directory() . '/inc/plugins/alimo-widgets.zip',
			'version'            => '1.1.0',
			'required'           => TRUE,
			'force_activation'   => FALSE,
			'force_deactivation' => FALSE,
		),
		array(
			'name'               => 'Alimo Demos',
			'slug'               => 'alimo-demos',
			'source'             => get_template_directory() . '/inc/plugins/alimo-demos.zip',
			'version'            => '1.0',
			'required'           => FALSE,
			'force_activation'   => FALSE,
			'force_deactivation' => FALSE,
		),
		array(
			'name'               => 'Meta Box',
			'slug'               => 'meta-box',
			'required'           => TRUE,
			'force_activation'   => FALSE,
			'force_deactivation' => FALSE,
			'version'            => '4.10.1',
		),
		array(
			'name'               => 'Meta Box Show Hide',
			'slug'               => 'meta-box-show-hide',
			'source'             => get_template_directory() . '/inc/plugins/meta-box-show-hide.zip',
			'required'           => TRUE,
			'force_activation'   => FALSE,
			'force_deactivation' => FALSE,
			'version'            => '0.1.0',
		),
		array(
			'name'               => 'Visual Composer',
			'slug'               => 'js_composer',
			'source'             => get_template_directory() . '/inc/plugins/js_composer.zip',
			'required'           => FALSE,
			'force_activation'   => FALSE,
			'force_deactivation' => FALSE,
		),

		array(
			'name'               => 'Revolution Slider',
			'slug'               => 'revslider',
			'source'             => get_template_directory() . '/inc/plugins/revslider.zip',
			'required'           => FALSE,
			'force_activation'   => FALSE,
			'force_deactivation' => FALSE,
		),

		array(
			'name'               => 'Zilla Likes',
			'slug'               => 'zilla-likes',
			'source'             => get_template_directory() . '/inc/plugins/zilla-likes.zip',
			'required'           => FALSE,
			'force_activation'   => FALSE,
			'force_deactivation' => FALSE,
		),
		array(
			'name'     => 'WP Instagram Widget',
			'slug'     => 'wp-instagram-widget',
			'required' => FALSE,
		),
		array(
			'name'     => 'Contact Form 7',
			'slug'     => 'contact-form-7',
			'required' => FALSE,
		),
		array(
			'name'     => 'WooCommerce',
			'slug'     => 'woocommerce',
			'required' => FALSE,
		),


	);

	$config = array(
		'id'           => 'alimo',
		// Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',
		// Default absolute path to bundled plugins.
		'menu'         => 'alimo-install-plugins',
		// Menu slug.
		'parent_slug'  => 'themes.php',
		// Parent menu slug.
		'capability'   => 'edit_theme_options',
		// Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => TRUE,
		// Show admin notices or not.
		'dismissable'  => TRUE,
		// If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',
		// If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => FALSE,
		// Automatically activate plugins after installation or not.
		'message'      => '',
		// Message to output right before the plugins table.
	);

	tgmpa( $plugins, $config );

}

add_action( 'tgmpa_register', 'alimo_register_required_plugins' );