jQuery(document).ready(function () {

    // enable post color overlay
    var enable_overlay = jQuery('#alimo_meta_enable_overlay');
    if (enable_overlay.find(':selected').val() !== 'enable') {
        jQuery('#alimo_meta_post_header_overlay_color').closest('.rwmb-field').hide();
        jQuery('#alimo_meta_post_header_overlay_alpha').closest('.rwmb-field').hide();
    }
    enable_overlay.change(function () {
        if (enable_overlay.find(':selected').val() === 'enable') {
            jQuery('#alimo_meta_post_header_overlay_color').closest('.rwmb-field').show();
            jQuery('#alimo_meta_post_header_overlay_alpha').closest('.rwmb-field').show();
        }
        if (enable_overlay.find(':selected').val() === 'disable') {
            jQuery('#alimo_meta_post_header_overlay_color').closest('.rwmb-field').hide();
            jQuery('#alimo_meta_post_header_overlay_alpha').closest('.rwmb-field').hide();
        }
    });

    var post_thumb = jQuery('#_thumbnail_id');
    var thumb_replacement = jQuery('#featured-image-replacement');
    if (post_thumb.val() === '-1') {
        thumb_replacement.hide();
    }
    var featuredImage = wp.media.featuredImage.frame();
    featuredImage.on('select', function () {
        thumb_replacement.show();
    });
    jQuery('#postimagediv').on( 'click', '#remove-post-thumbnail', function() {
        thumb_replacement.hide();
    });
});
