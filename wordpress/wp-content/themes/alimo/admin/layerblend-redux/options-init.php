<?php
/**
 * Alimo Panel Config File
 * For full documentation, please visit: http://docs.reduxframework.com/
 */

if ( ! class_exists( 'Redux' ) ) {
	return;
}


// This is your option name where all the Redux data is stored.
$opt_name = "alimo_data";

// This line is only for altering the demo. Can be easily removed.
//$opt_name = apply_filters( 'redux_demo/opt_name', $opt_name );

/**
 * ---> SET ARGUMENTS
 * All the possible arguments for Redux.
 * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
 * */

$theme = wp_get_theme(); // For use with some settings. Not necessary.

$args = array(
	// TYPICAL -> Change these values as you need/desire
	'opt_name'             => $opt_name,
	'allow_tracking'       => false,
	'templates_path'       => LAYERBLEND_FRAMEWORK . 'templates/panel/',
	// This is where your data is stored in the database and also becomes your global variable name.
	'display_name'         => $theme->get( 'Name' ),
	// Name that appears at the top of your panel
	'display_version'      => $theme->get( 'Version' ),
	// Version that appears at the top of your panel
	'menu_type'            => 'menu',
	//Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
	'allow_sub_menu'       => true,
	// Show the sections below the admin menu item or not
	'menu_title'           => esc_html__( 'Alimo', 'alimo' ),
	'page_title'           => esc_html__( 'Alimo Options', 'alimo' ),
	// You will need to generate a Google API key to use this feature.
	// Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
	'google_api_key'       => '',
	// Set it you want google fonts to update weekly. A google_api_key value is required.
	'google_update_weekly' => false,
	// Must be defined to add google fonts to the typography module
	'async_typography'     => true,
	// Use a asynchronous font on the front end or font string
	//'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
	'admin_bar'            => false,
	// Show the panel pages on the admin bar
	'admin_bar_icon'       => 'dashicons-portfolio',
	// Choose an icon for the admin bar menu
	'admin_bar_priority'   => 50,
	// Choose an priority for the admin bar menu
	'global_variable'      => '',
	// Set a different name for your global variable other than the opt_name
	'dev_mode'             => false,
	// Show the time the page took to load, etc
	'update_notice'        => false,
	// If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
	'customizer'           => true,
	// Enable basic customizer support
	//'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
	//'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

	// OPTIONAL -> Give you extra features
	'page_priority'        => 53,
	// Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
	'page_parent'          => 'themes.php',
	// For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
	'page_permissions'     => 'manage_options',
	// Permissions needed to access the options panel.
	'menu_icon'            => LAYERBLEND_FRAMEWORK_DIR . 'img/alimo_icon.svg',
	// Specify a custom URL to an icon
	'last_tab'             => '',
	// Force your panel to always open to a specific tab (by id)
	'page_icon'            => 'icon-themes',
	// Icon displayed in the admin panel next to your menu_title
	'page_slug'            => '',
	// Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
	'save_defaults'        => true,
	// On load save the defaults to DB before user clicks save or not
	'default_show'         => false,
	// If true, shows the default value next to each field that is not the default value.
	'default_mark'         => '',
	// What to print by the field's title if the value shown is default. Suggested: *
	'show_import_export'   => true,
	// Shows the Import/Export panel when not used as a field.

	// CAREFUL -> These options are for advanced use only
	'transient_time'       => 60 * MINUTE_IN_SECONDS,
	'output'               => true,
	// Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
	'output_tag'           => true,
	// Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
	'footer_credit'        => '',
	// Disable the footer credit of Redux. Please leave if you can help it.

	// FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
	'database'             => '',
	// possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
	'use_cdn'              => true,
	// If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

	// HINTS
	'hints'                => array(
		'icon'          => 'el el-question-sign',
		'icon_position' => 'right',
		'icon_color'    => 'lightgray',
		'icon_size'     => 'normal',
		'tip_style'     => array(
			'color'   => 'red',
			'shadow'  => true,
			'rounded' => false,
			'style'   => '',
		),
		'tip_position'  => array(
			'my' => 'top left',
			'at' => 'bottom right',
		),
		'tip_effect'    => array(
			'show' => array(
				'effect'   => 'slide',
				'duration' => '500',
				'event'    => 'mouseover',
			),
			'hide' => array(
				'effect'   => 'slide',
				'duration' => '500',
				'event'    => 'click mouseleave',
			),
		),
	)
);


// SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.

$args['share_icons'][] = array(
	'url'   => 'https://www.facebook.com/LayerBlend',
	'title' => esc_html__('Like us on Facebook','alimo'),
	'icon'  => 'el el-facebook'
);
$args['share_icons'][] = array(
	'url'   => 'http://twitter.com/LayerBlend',
	'title' => esc_html__('Follow us on Twitter','alimo'),
	'icon'  => 'el el-twitter'
);
// Panel Intro text -> before the form
if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
	if ( ! empty( $args['global_variable'] ) ) {
		$v = $args['global_variable'];
	} else {
		$v = str_replace( '-', '_', $args['opt_name'] );
	}
	$args['intro_text'] = '';
} else {
	$args['intro_text'] = '';
}

Redux::setArgs( $opt_name, $args );

/*
 * ---> END ARGUMENTS
 */


// Theme info section HTML
ob_start();

$ct         = wp_get_theme();
$item_name  = $ct->get( 'Name' );
$tags       = $ct->Tags;
$screenshot = $ct->get_screenshot();
$class      = $screenshot ? 'has-screenshot' : '';

$customize_title = sprintf( esc_html__( 'Customize &#8220;%s&#8221;', 'alimo' ), $ct->display( 'Name' ) );

?>
    <div id="current-theme" class="<?php echo esc_attr( $class ); ?>">
		<?php if ( $screenshot ) : ?>
			<?php if ( current_user_can( 'edit_theme_options' ) ) : ?>
                <a href="<?php echo wp_customize_url(); ?>" class="load-customize hide-if-no-customize"
                   title="<?php echo esc_attr( $customize_title ); ?>">
                    <img src="<?php echo esc_url( $screenshot ); ?>"
                         alt="<?php esc_attr_e( 'Current theme preview', 'alimo' ); ?>"/>
                </a>
			<?php endif; ?>
            <img class="hide-if-customize" src="<?php echo esc_url( $screenshot ); ?>"
                 alt="<?php esc_attr_e( 'Current theme preview', 'alimo' ); ?>"/>
		<?php endif; ?>

        <h4><?php echo $ct->display( 'Name' ); ?></h4>

        <div>
            <ul class="theme-info">
                <li><?php printf( esc_html__( 'By %s', 'alimo' ), $ct->display( 'Author' ) ); ?></li>
                <li><?php printf( esc_html__( 'Version %s', 'alimo' ), $ct->display( 'Version' ) ); ?></li>
                <li><?php echo '<strong>' . esc_html__( 'Tags', 'alimo' ) . ':</strong> '; ?><?php printf( $ct->display( 'Tags' ) ); ?></li>
            </ul>
            <p class="theme-description"><?php echo $ct->display( 'Description' ); ?></p>
			<?php
			if ( $ct->parent() ) {
				printf( ' <p class="howto">' . wp_kses(__( 'This <a href="%1$s">child theme</a> requires its parent theme, %2$s.', 'alimo' ), array('a'=>array('href'=>array()))) . '</p>', esc_html__( 'http://codex.wordpress.org/Child_Themes', 'alimo' ), $ct->parent()->display( 'Name' ) );
			}
			?>

        </div>
    </div>

<?php
$item_info = ob_get_contents();

ob_end_clean();
// End Theme info HTML


function help_section( $url_path = false, $alert_type = 'info' ) {
	$html = '<div class="help-section alert alert-' . $alert_type . '">
                            <div class="pull-left">    
                                <h4 class="alert-heading">Need help?</h4>
                                <p>You can find out more information about this section in our help desk</p>
                            </div>
                            <a href="http://help.layerblend.com/alimo' . $url_path . '" target="_blank" class="btn btn-outline-' . $alert_type . ' pull-right">Help with this section</a>
                       </div>';

	return $html;
}


/*******************************************************************************************************
 *
 *                              S E C T I O N S    D E C L A R A T I O N
 *
 *******************************************************************************************************/

/*
 * GENERAL OPTIONS SECTION
 */
Redux::setSection( $opt_name, array(
	'icon_type' => 'image',
	'icon'      => LAYERBLEND_FRAMEWORK_DIR . 'img/icon-general.svg',
	'title'     => esc_html__( 'General', 'alimo' ),
	'help_area' => help_section( '#general' ),
	'fields'    => array(
		// Search box placeholder
		array(
			'id'       => 'search-placeholder',
			'type'     => 'text',
			'title'    => esc_html__( 'Search placeholder.', 'alimo' ),
			'subtitle' => esc_html__( 'Set a custom placeholder for the search in the header.', 'alimo' ),
			'default'  => esc_html__( 'Search and press Enter', 'alimo' ),
		),
	)
) );

/*
 * DESIGN SECTION
 */
Redux::setSection( $opt_name, array(
	'id'        => 'design_section',
	'icon_type' => 'image',
	'icon'      => LAYERBLEND_FRAMEWORK_DIR . 'img/icon-layout.svg',
	'title'     => esc_html__( 'Design', 'alimo' ),
	'fields'    => array()
) );

/*
 * DESIGN > LOGO SECTION
 */
Redux::setSection( $opt_name, array(
	'icon_type'  => 'image',
	'icon'       => LAYERBLEND_FRAMEWORK_DIR . 'img/icon-chevron-right.svg',
	'title'      => esc_html__( 'Logo', 'alimo' ),
	'help_area'  => help_section( '#design-logo' ),
	'subsection' => true,
	'fields'     => array(
		// Logo upload
		array(
			'id'       => 'logo-alimo',
			'type'     => 'media',
			'url'      => false,
			'title'    => esc_html__( 'Logo', 'alimo' ),
			'compiler' => 'true',
			'subtitle' => esc_html__( 'Upload your logo image here.', 'alimo' ),
            'default'  => ''
		),
		// Logo size
		array(
			'id'       => 'logo-size',
			'type'     => 'radio',
			'title'    => esc_html__( 'Logo Size', 'alimo' ),
			'subtitle' => esc_html__( 'Please select the display size of your logo', 'alimo' ),
			'desc'     => esc_html__( 'Selecting "Small", the logo will have more top and bottom padding. Selecing "Large", it will have less.', 'alimo' ),
			'options'  => array(
				'small'  => 'Small',
				'medium' => 'Medium',
				'large'  => 'Large'
			),
			'default'  => 'medium',
		),
		// Logo Alt text
		array(
			'id'       => 'logo-text',
			'type'     => 'text',
			'title'    => esc_html__( 'Logo Alt Text', 'alimo' ),
			'subtitle' => esc_html__( 'If you have an image logo, this text will be added to the alt tag', 'alimo' ),
			'validate' => 'no_special_chars',
            'default'  => ''
		),
		// Logo font
		array(
			'id'          => 'logo-font',
			'type'        => 'typography',
			'title'       => esc_html__( 'Logo Font', 'alimo' ),
			'google'      => true,
			'font-backup' => false,
			'text-align'  => false,
			'subsets'     => false,
			'font-size'   => false,
			'font-style'  => false,
			'font-weight' => false,
			'color'       => false,
			'line-height' => false,
			'output'      => array( '.main-logo .site-title' ),
			'units'       => 'px',
			'subtitle'    => esc_html__( 'This font will be used in case no logo image is provided', 'alimo' ),
			'default'     => array(
				'font-family' => 'Poppins',
				'google'      => true,
			),
		),
		// Favicon
		array(
			'id'       => 'favicon',
			'type'     => 'media',
			'url'      => false,
			'title'    => esc_html__( 'Favicon', 'alimo' ),
			'compiler' => 'true',
			'desc'     => esc_html__( 'A favicon is the image that gets displayed in the address bar of every browser. Use an image that makes your site stand out from the other tabs your visitors might have open. Keep it small and square (min 16x16px - max 128x128px).', 'alimo' ),
			'subtitle' => esc_html__( 'Upload a favicon image here', 'alimo' ),
			'default'  => ''
		),
	)
) );

/*
 * DESIGN > HEADER LAYOUT SECTION
 */


if ( ! function_exists( 'alimo_available_social_profiles' ) ) {
	function alimo_available_social_profiles( $field ) {
		global $alimo_data;
		$networks           = array(
			'facebook'      => 'Facebook',
			'google-plus'   => 'Google+',
			'instagram'     => 'Instagram',
			'twitter'       => 'Twitter',
			'youtube'       => 'Youtube',
			'vimeo'         => 'Vimeo',
			'pinterest'     => 'Pinterest',
			'tumblr'        => 'Tumblr',
			'dribbble'      => 'Dribbble',
			'behance'       => 'Behance',
			'linkedin'      => 'LinkedIn',
			'vine'          => 'Vine',
			'rss'           => 'RSS',
			'flickr'        => 'Flickr',
			'spotify'       => 'Spotify',
			'github'        => 'GitHub',
			'stackexchange' => 'StackExchange',
			'soundcloud'    => 'SoundCloud',
			'vk'            => 'VK',
		);
		$available_networks = array();
		foreach ( $networks as $network => $network_name ) {
			$profile_url =  ( !empty($alimo_data[ $network . '-url' ]) ) ? $alimo_data[ $network . '-url' ] : null;
			if ( $profile_url ) {
				$available_networks[ $network ] = $network_name;
			}
		}

		$field = array(
			// Social icons
			'id'       => 'header-social',
			'type'     => 'sortable',
			'title'    => esc_html__( 'Social profiles', 'alimo' ),
			'subtitle' => wp_kses(__( 'Enable the social profiles you want in the header and use the <i class="el el-move icon-large"> drag icon</i> on the right to set the order in which they appear. <br><br> Make sure you add the profile URL\'s in Social Media > <a class="redux-group-tab-link-a" href="#" data-rel="19">Profiles</a>, <b style="color:red">save changes and refresh the page</b> otherwise they won\'t appear', 'alimo' ),array('a' => array('class'=>array(),'data-rel'=>array(),'href'=>array()) ,'i' => array() , 'b' =>array()  , 'br'=> array() )),
			'mode'     => 'checkbox',
			'options'  => $available_networks,
			'default'  => array(
				'facebook'  => true,
				'instagram' => true,
				'twitter'   => true,
				'youtube'   => true,
			),
		);

		return $field;
	}
}
add_filter( 'redux/options/' . $opt_name . '/field/header-social/register', 'alimo_available_social_profiles' );

Redux::setSection( $opt_name, array(
	'id'         => 'header_layout_section',
	'icon_type'  => 'image',
	'icon'       => LAYERBLEND_FRAMEWORK_DIR . 'img/icon-chevron-right.svg',
	'title'      => esc_html__( 'Header layout', 'alimo' ),
	'help_area'  => help_section( '#design-header' ),
	'subsection' => true,
	'fields'     => array(
		// Header layout
		array(
			'id'       => 'header_format',
			'type'     => 'image_select',
			'title'    => esc_html__( 'Header Layout', 'alimo' ),
			'subtitle' => esc_html__( 'Please select the layout you desire', 'alimo' ),
			'options'  => array(
				'default_header' => array(
					'title' => esc_html__('Default Layout','alimo'),
					'img'   => LAYERBLEND_FRAMEWORK_DIR . 'img/header1.png'
				),
				'centred_header' => array(
					'title' => esc_html__('Centred Menu','alimo'),
					'img'   => LAYERBLEND_FRAMEWORK_DIR . 'img/header2.png'
				),
				'banner_header'  => array(
					'title' => esc_html__('Banner Logo','alimo'),
					'img'   => LAYERBLEND_FRAMEWORK_DIR . 'img/header3.png'
				)
			),
			'default'  => 'default_header'
		),
		// Header boxed
		array(
			'id'       => 'header_boxed',
			'type'     => 'switch',
			'title'    => esc_html__( 'Boxed header.', 'alimo' ),
			'subtitle' => esc_html__( 'Use this switch to show or hide the search.', 'alimo' ),
			'default'  => true,
			'on'       => esc_html__('Boxed', 'alimo'),
			'off'      => esc_html__('Full-width', 'alimo'),
		),
		// Header background color
		array(
			'id'       => 'banner_header_overlay',
			'type'     => 'color_rgba',
			'title'    => esc_html__( 'Banner Header Background Color', 'alimo' ),
			'subtitle' => esc_html__( 'Pick a background color for the header (default: rgba(207, 52, 74, 0.8) ).', 'alimo' ),
			'default'  => array(
				'color' => '#0081ff',
				'alpha' => 0.67
			),
			'required' => array( 'header_format', '=', 'banner_header' )
		),
		// Header background image
		array(
			'id'       => 'banner_header_bg_image',
			'type'     => 'media',
			'url'      => false,
			'title'    => esc_html__( 'Banner Header Background Image', 'alimo' ),
			'compiler' => 'true',
			'subtitle' => esc_html__( 'If you would like to use a backgorund image for the header area, upload it here.', 'alimo' ),
			'desc'     => esc_html__( 'Note: the selected Header Background Color will be used as an overlay. Please make sure the selected color has a minimum transparency setting.', 'alimo' ),
			'required' => array( 'header_format', '=', 'banner_header' ),
            'default'  => ''
		),
		// Banner Logo Upload
		array(
			'id'       => 'banner_logo',
			'type'     => 'media',
			'url'      => false,
			'title'    => esc_html__( 'Banner Logo', 'alimo' ),
			'compiler' => 'true',
			'subtitle' => esc_html__( 'Upload here the logo image used with the Banner Header Layout.', 'alimo' ),
			'required' => array( 'header_format', '=', 'banner_header' ),
            'default'  => ''
		),
		// Banner logo font
		array(
			'id'          => 'banner-font',
			'type'        => 'typography',
			'title'       => esc_html__( 'Banner Logo Font', 'alimo' ),
			'google'      => true,
			'font-backup' => false,
			'text-align'  => false,
			'output'      => array( '.site-title' ),
			'units'       => 'px',
			'subtitle'    => esc_html__( 'This font will be used in case no logo or banner logo image is provided', 'alimo' ),
			'default'     => array(
				'color'       => '#000',
				'font-family' => 'Poppins',
				'google'      => true,
				'font-size'   => '89px',
				'line-height' => '133px'
			),
			'required' => array( 'header_format', '=', 'banner_header' ),
		),
        array(
            'id'       => 'header_gradient',
            'type'     => 'color_gradient',
            'title'    => esc_html__('Header Background Color', 'alimo'),
            'desc'     => esc_html__('Use this option to set a background color to the header menu.', 'alimo'),
            'validate' => 'color',
            'default'  => array(
                'from' => '',
                'to'   => '',
            ),
        ),
        array(
            'id'            => 'header_gradient_angle',
            'type'          => 'slider',
            'title'         => esc_html__('Header Background Color Angle', 'alimo'),
            'subtitle'      => esc_html__('This slider displays the value as a label.', 'alimo'),
            'desc'          => esc_html__('Set the gradient angle', 'alimo'),
            "default"       => 0,
            "min"           => -1,
            "step"          => 1,
            "max"           => 90,
            'display_value' => 'Angle',
        ),


		// Social icons
		array(
			'id'       => 'header-social',
			'type'     => 'sortable',
			'title'    => esc_html__( 'Social profiles', 'alimo' ),
			'subtitle' => wp_kses(__( 'Enable the social profiles you want to appear in the header and use the <i class="el el-move icon-large"> drag icon</i> on the right to set the order in which they appear. <br><br> Make sure you add the profile URL\'s in Social Media > <a class="redux-group-tab-link-a" href="#" data-rel="19">Profiles</a>, otherwise they won\'t appear', 'alimo' ), array('a' => array('class'=>array(),'data-rel'=>array(),'href'=>array()) ,'i' => array() , 'b' =>array()  , 'br'=> array() )),
			'mode'     => 'checkbox',
			'options'  => array(
				'facebook'      => 'Facebook',
				'google'        => 'Google+',
				'instagram'     => 'Instagram',
				'twitter'       => 'Twitter',
				'youtube'       => 'Youtube',
				'vimeo'         => 'Vimeo',
				'pinterest'     => 'Pinterest',
				'tumblr'        => 'Tumblr',
				'dribbble'      => 'Dribbble',
				'behance'       => 'Behance',
				'linkedin'      => 'LinkedIn',
				'vine'          => 'Vine',
				'rss'           => 'RSS',
				'flickr'        => 'Flickr',
				'spotify'       => 'Spotify',
				'github'        => 'GitHub',
				'stackexchange' => 'StackExchange',
				'soundcloud'    => 'SoundCloud',
				'vk'            => 'VK',
			),
			'default'  => array(
				'facebook'  => true,
				'instagram' => true,
				'twitter'   => true,
				'youtube'   => true,
			),

		),
		// Search box show / hide
		array(
			'id'       => 'display_search',
			'type'     => 'switch',
			'title'    => esc_html__( 'Display search.', 'alimo' ),
			'subtitle' => esc_html__( 'Use this switch to show or hide the search.', 'alimo' ),
			'default'  => true
		),

	)
) );

/*
 * DESIGN > TYPOGRAPHY SECTION
 */
Redux::setSection( $opt_name, array(
	'icon_type'  => 'image',
	'icon'       => LAYERBLEND_FRAMEWORK_DIR . 'img/icon-chevron-right.svg',
	'title'      => esc_html__( 'Typography', 'alimo' ),
	'help_area'  => help_section( '#design-typography' ),
	'desc'       => esc_html__( 'Change the font-family of different sections. Leave empty for default font families', 'alimo' ),
	'subsection' => true,
	'fields'     => array(
		// Menu font
		array(
			'id'          => 'menu-font',
			'type'        => 'typography',
			'title'       => esc_html__( 'Menu', 'alimo' ),
			'google'      => true,
			'font-backup' => false,
			'text-align'  => false,
			'line-height' => false,
			'output'      => array( '.main-nav .header-menu .main-menu li a' ),
			'units'       => 'px',
			'subtitle'    => esc_html__( 'Specify the Main Menu font properties.', 'alimo' ),
			'default'     => array(
				'color'       => '#000',
				'font-style'  => '300',
				'font-family' => 'Montserrat',
				'google'      => true,
				'font-size'   => '13px',
			),
		),
		// Heading font
		array(
			'id'          => 'heading-font',
			'type'        => 'typography',
			'title'       => esc_html__( 'Heading Font', 'alimo' ),
			'google'      => true,
			'font-backup' => false,
			'text-align'  => false,
            'all_styles'  => true,
			'output'      => array( '.articles-container article header h2, .articles-container-interlaced article header h2, .articles-container-masonry article header h2, .articles-container-grid article header h2, .post-content-wrapper h1, .page-content-wrapper h1' ),
			'units'       => 'px',
			'subtitle'    => esc_html__( 'Specify the Page Heading font properties.', 'alimo' ),
			'default'     => array(
				'color'       => '#000',
				'font-weight' => '700',
				'font-family' => 'Poppins',
				'google'      => true,
				'font-size'   => '34px',
				'line-height' => '42px'
			),
		),
		// Content font
		array(
			'id'          => 'content-font',
			'type'        => 'typography',
			'title'       => esc_html__( 'Content Font', 'alimo' ),
			'google'      => true,
			'font-backup' => false,
			'text-align'  => false,
			'all_styles'  => true,
			'output'      => array( '.post-content-wrapper, .page-content-wrapper','.article-wrapper' ),
			'units'       => 'px',
			'subtitle'    => esc_html__( 'Specify the Page Content font properties.', 'alimo' ),
			'default'     => array(
				'color'       => '#000',
				'font-weight' => '300',
				'font-family' => 'Muli',
				'google'      => true,
				'font-size'   => '16px',
				'line-height' => '27px',
				'subsets'     => 'latin-ext'
			),
		),
		// Footer font
		array(
			'id'          => 'footer-font',
			'type'        => 'typography',
			'title'       => esc_html__( 'Footer Content', 'alimo' ),
			'google'      => true,
			'font-backup' => false,
			'text-align'  => false,
            'line-height' => false,
			'subsets'     => false,
			'output'      => array( '.main-footer, .main-footer .widget ul li' ),
			'units'       => 'px',
			'subtitle'    => esc_html__( 'Specify the Post Content font properties.', 'alimo' ),
			'default'     => array(
				'color'       => '#000',
				'font-weight' => '300',
				'font-family' => 'Muli',
				'google'      => true,
				'font-size'   => '12px',
			),
		),
	),
) );

/*
 * DESIGN > COLOR SCHEMES SECTION
 */
Redux::setSection( $opt_name, array(
	'icon_type'  => 'image',
	'icon'       => LAYERBLEND_FRAMEWORK_DIR . 'img/icon-chevron-right.svg',
	'title'      => esc_html__( 'Color schemes', 'alimo' ),
	'id'         => 'color-palette',
	'help_area'  => help_section( '#design-color-schemes' ),
	'subsection' => true,
	'fields'     => array(
		// Select desired color palette
		array(
			'id'      => 'accent_color',
			'type'    => 'image_select',
			'title'   => esc_html__( 'Select one of the predefined accent colors or choose your own by selecting the \'Custom Accent\' option.', 'alimo' ),
			'options' => array(
				'accent_one'    => array(
					'title' => esc_html__('Accent One','alimo'),
					'img'   => LAYERBLEND_FRAMEWORK_DIR . 'img/accent1.png'
				),
				'accent_two'    => array(
					'title' => esc_html__('Accent Two','alimo'),
					'img'   => LAYERBLEND_FRAMEWORK_DIR . 'img/accent2.png'
				),
				'accent_three'  => array(
					'title' => esc_html__('Accent Three','alimo'),
					'img'   => LAYERBLEND_FRAMEWORK_DIR . 'img/accent3.png'
				),
				'accent_custom' => array(
					'title' => esc_html__('Custom Accent','alimo'),
					'img'   => LAYERBLEND_FRAMEWORK_DIR . 'img/accent_custom.png'
				)
			),
			'default' => 'accent_one',
		),
		// Custom Accent
		array(
			'id'          => 'custom_accent_color',
			'type'        => 'color',
			'title'       => esc_html__( 'Custom Accent Color', 'alimo' ),
			'transparent' => false,
			'default'     => '#0081ff',
			'validate'    => 'color',
			'required'    => array( 'accent_color', '=', 'accent_custom' ),
		),

	)
) );

/*
 * DESIGN > LISTING LAYOUTS SECTION
 */
Redux::setSection($opt_name, array(
  'icon_type'  => 'image',
  'icon'       => LAYERBLEND_FRAMEWORK_DIR . 'img/icon-chevron-right.svg',
  'title'      => esc_html__('Listing layouts', 'alimo'),
  'desc'       => esc_html__('This section allows you to set the desired layout for the various pages that list posts.', 'alimo'),
  'help_area'  => help_section('#design-layouts'),
  'subsection' => TRUE,
  'fields'     => array(
    // >> Start Listing layouts section
    array(
      'id'     => 'section-listing-layouts-start',
      'type'   => 'section',
      'indent' => TRUE,
    ),
    // Index layout
    array(
      'id'       => 'index-layout',
      'type'     => 'select',
      'title'    => esc_html__('Index page', 'alimo'),
      'subtitle' => esc_html__('Select the index page listing layout', 'alimo'),
      'default'  => esc_html__('standard', 'alimo'),
      'options'  => array(
        'standard'        => 'Standard',
        'masonry'         => 'Masonry',
        'grid'            => 'Grid',
        'interlaced-full' => 'Interlaced Fullwidth',
        'interlaced-half' => 'Interlaced Half',
      ),
    ),
    // Category page layout
    array(
      'id'       => 'category-layout',
      'type'     => 'select',
      'title'    => esc_html__('Category page', 'alimo'),
      'subtitle' => esc_html__('Select the category page listing layout', 'alimo'),
      'default'  => esc_html__('standard', 'alimo'),
      'options'  => array(
        'standard'        => 'Standard',
        'masonry'         => 'Masonry',
        'grid'            => 'Grid',
        'interlaced-full' => 'Interlaced Fullwidth',
        'interlaced-half' => 'Interlaced Half',
      ),
    ),
    // Search page layout
    array(
      'id'       => 'search-layout',
      'type'     => 'select',
      'title'    => esc_html__('Search page', 'alimo'),
      'subtitle' => esc_html__('Select the search page listing layout', 'alimo'),
      'default'  => esc_html__('standard', 'alimo'),
      'options'  => array(
        'standard'        => 'Standard',
        'masonry'         => 'Masonry',
        'grid'            => 'Grid',
        'interlaced-full' => 'Interlaced Fullwidth',
        'interlaced-half' => 'Interlaced Half',
      ),
    ),
    // Author page layout
    array(
      'id'       => 'author-layout',
      'type'     => 'select',
      'title'    => esc_html__('Author page', 'alimo'),
      'subtitle' => esc_html__('Select the author page listing layout', 'alimo'),
      'default'  => esc_html__('standard', 'alimo'),
      'options'  => array(
        'standard'        => 'Standard',
        'masonry'         => 'Masonry',
        'grid'            => 'Grid',
        'interlaced-full' => 'Interlaced Fullwidth',
        'interlaced-half' => 'Interlaced Half',
      ),
    ),
    // Date page layout
    array(
      'id'       => 'date-layout',
      'type'     => 'select',
      'title'    => esc_html__('Date archive page', 'alimo'),
      'subtitle' => esc_html__('Select the date archive page listing layout', 'alimo'),
      'default'  => esc_html__('standard', 'alimo'),
      'options'  => array(
        'standard'        => 'Standard',
        'masonry'         => 'Masonry',
        'grid'            => 'Grid',
        'interlaced-full' => 'Interlaced Fullwidth',
        'interlaced-half' => 'Interlaced Half',
      ),
    ),
    // Tag page layout
    array(
      'id'       => 'tag-layout',
      'type'     => 'select',
      'title'    => esc_html__('Tag page', 'alimo'),
      'subtitle' => esc_html__('Select the tag page listing layout', 'alimo'),
      'default'  => esc_html__('standard', 'alimo'),
      'options'  => array(
        'standard'        => 'Standard',
        'masonry'         => 'Masonry',
        'grid'            => 'Grid',
        'interlaced-full' => 'Interlaced Fullwidth',
        'interlaced-half' => 'Interlaced Half',
      ),
    ),
    // Archive page layout
    array(
      'id'       => 'archive-layout',
      'type'     => 'select',
      'title'    => esc_html__('Archive page', 'alimo'),
      'subtitle' => esc_html__('Select the archive page listing layout', 'alimo'),
      'default'  => esc_html__('standard', 'alimo'),
      'options'  => array(
        'standard'        => 'Standard',
        'masonry'         => 'Masonry',
        'grid'            => 'Grid',
        'interlaced-full' => 'Interlaced Fullwidth',
        'interlaced-half' => 'Interlaced Half',
      ),
    ),
    // << End section
    array(
      'id'     => 'section-listing-layouts-end',
      'type'   => 'section',
      'indent' => FALSE,
    ),

    // >> Start Post meta section
    array(
      'id'       => 'section-post-meta-start',
      'title'    => esc_html__('Post meta', 'alimo'),
      'subtitle' => esc_html__('This section allows you to enable or disable the display of post information regarding author, publish date, category, tags, comments, social sharing, likes and reading time.', 'alimo'),
      'type'     => 'section',
      'indent'   => TRUE,
    ),
    // Author details
    array(
      'id'       => 'list-author',
      'type'     => 'switch',
      'title'    => esc_html__('Author details', 'alimo'),
      'subtitle' => esc_html__('Display the author name and profile image.', 'alimo'),
      'default'  => TRUE,
      'on'       => esc_html__('Show', 'alimo'),
      'off'      => esc_html__('Hide', 'alimo'),
    ),
    // Date toggle
    array(
      'id'       => 'list-date',
      'type'     => 'switch',
      'title'    => esc_html__('Publish date', 'alimo'),
      'subtitle' => esc_html__('Display the post publish date.', 'alimo'),
      'default'  => TRUE,
      'on'       => esc_html__('Show', 'alimo'),
      'off'      => esc_html__('Hide', 'alimo'),
    ),
    // Category toggle
    array(
      'id'       => 'list-categories',
      'type'     => 'switch',
      'title'    => esc_html__('Categories', 'alimo'),
      'default'  => TRUE,
      'on'       => esc_html__('Show', 'alimo'),
      'off'      => esc_html__('Hide', 'alimo'),
      'subtitle' => esc_html__('Display the categories', 'alimo'),
    ),
    // Tags toggle
    array(
      'id'       => 'list-tags',
      'type'     => 'switch',
      'title'    => esc_html__('Tags', 'alimo'),
      'default'  => TRUE,
      'on'       => esc_html__('Show', 'alimo'),
      'off'      => esc_html__('Hide', 'alimo'),
      'subtitle' => esc_html__('Display the tags', 'alimo'),
    ),
    // Comment count
    array(
      'id'       => 'list-comments',
      'type'     => 'switch',
      'title'    => esc_html__('Comments', 'alimo'),
      'default'  => TRUE,
      'on'       => esc_html__('Show', 'alimo'),
      'off'      => esc_html__('Hide', 'alimo'),
      'subtitle' => esc_html__('Display the number of comments', 'alimo'),
    ),
    // Social media share
    array(
      'id'       => 'list-social',
      'type'     => 'switch',
      'title'    => esc_html__('Social sharing', 'alimo'),
      'default'  => TRUE,
      'on'       => esc_html__('Show', 'alimo'),
      'off'      => esc_html__('Hide', 'alimo'),
      'subtitle' => esc_html__('Display the social media sharing options', 'alimo'),
    ),
    // Post likes info
    array(
      'id'       => 'list-likes',
      'type'     => 'switch',
      'title'    => esc_html__('Likes', 'alimo'),
      'default'  => TRUE,
      'on'       => esc_html__('Show', 'alimo'),
      'off'      => esc_html__('Hide', 'alimo'),
      'subtitle' => esc_html__('Display the post likes feature', 'alimo'),
      'desc'     => wp_kses(__('If you are using the standard listing, you can customise the text next to the heart icon by going to Posts > <a href="#" class="redux-group-tab-link-a" data-rel="16">Post likes</a>.', 'alimo'),array(  'a' => array('class'=>array(),'data-rel'=>array(),'href'=>array()) )),
    ),
    // Reading time
    array(
      'id'       => 'list-reading-time',
      'type'     => 'switch',
      'title'    => esc_html__('Reading time', 'alimo'),
      'default'  => TRUE,
      'on'       => esc_html__('Show', 'alimo'),
      'off'      => esc_html__('Hide', 'alimo'),
      'subtitle' => esc_html__('Display the article reading time', 'alimo'),
    ),
    // << End section
    array(
      'id'     => 'section-post-meta-end',
      'type'   => 'section',
      'indent' => FALSE,
    ),
    // >> Start Post meta section
    array(
      'id'       => 'section-post-animation',
      'title'    => esc_html__('Post list animation', 'alimo'),
      'subtitle' => esc_html__('This section allows you to enable or disable the display of post information regarding author, publish date, category, tags, comments, social sharing, likes and reading time.', 'alimo'),
      'type'     => 'section',
      'indent'   => TRUE,
    ),
    array(
      'id'       => 'list-animation',
      'type'     => 'switch',
      'title'    => esc_html__('Animation', 'alimo'),
      'default'  => TRUE,
      'on'       => esc_html__('Enable', 'alimo'),
      'off'      => esc_html__('Disable', 'alimo'),
      'subtitle' => esc_html__('Enable the animation for posts in listings.', 'alimo'),
    ),
    // << End section
    array(
      'id'     => 'section-post-meta-end',
      'type'   => 'section',
      'indent' => FALSE,
    ),
  ),
));

/*
 * DESIGN > SIDEBAR SECTION
 */
Redux::setSection( $opt_name, array(
	'icon_type'  => 'image',
	'icon'       => LAYERBLEND_FRAMEWORK_DIR . 'img/icon-chevron-right.svg',
	'title'      => esc_html__( 'Sidebar', 'alimo' ),
	'help_area'  => help_section( '#design-sidebar' ),
	'subsection' => true,
	'desc'       => esc_html__( 'Enable/Disable the sidebar in different areas and configure the position', 'alimo' ),
	'fields'     => array(
		// Enable sidebar
		array(
			'id'       => 'enable-sidebar',
			'type'     => 'switch',
			'title'    => esc_html__( 'Enable sidebar', 'alimo' ),
			'default'  => true,
		),
		// Enable always open
		array(

			'id'       => 'sidebar-always-open',
			'type'     => 'switch',
			'title'    => esc_html__( 'Always open', 'alimo' ),
			'subtitle' => esc_html__( 'Enable always open option for the sidebar', 'alimo' ),
			'default'  => true,
			'required' => array( 'enable-sidebar', 'equals', true ),
		),
		// Sidebar position
		array(
			'id'       => 'sidebar-position',
			'type'     => 'radio',
			'title'    => esc_html__( 'Sidebar position', 'alimo' ),
			'options'  => array(
				'left'  => 'Left',
				'right' => 'Right',
			),
			'default'  => 'right',
			'required' => array( 'enable-sidebar', 'equals', true ),
		),
	)
) );

/*
 * DESIGN > FOOTER OPTIONS SECTION
 */
Redux::setSection( $opt_name, array(
	'icon_type'  => 'image',
	'icon'       => LAYERBLEND_FRAMEWORK_DIR . 'img/icon-chevron-right.svg',
	'title'      => esc_html__( 'Footer options', 'alimo' ),
	'help_area'  => help_section( '#design-footer' ),
	'subsection' => true,
	'fields'     => array(
		// Enable footer widget areas
		array(
			'id'       => 'enable_footer',
			'type'     => 'switch',
			'title'    => esc_html__( 'Enable footer widget areas', 'alimo' ),
			'subtitle' => esc_html__( 'Do you want use the main footer that contains all the widgets areas?', 'alimo' ),
			'desc'     => esc_html__('','alimo'),
			'default'  => true
		),

		// Footer columns
		array(
			'id'       => 'footer_columns',
			'type'     => 'image_select',
			'title'    => esc_html__( 'Footer Columns', 'alimo' ),
			'subtitle' => esc_html__( 'Please select the number of columns you would like for your footer.', 'alimo' ),
			'desc'     => sprintf( wp_kses( __( 'You can add or remove widgets by going to the <a href="%s">Widget Area</a>.', 'alimo' ), array(  'a' => array( 'href' => array() ) ) ), esc_url( admin_url('widgets.php') ) ),
			'options'  => array(
				'2' => array( 'title' => '2 Columns', 'img' => LAYERBLEND_FRAMEWORK_DIR . 'img/2col.png' ),
				'3' => array( 'title' => '3 Columns', 'img' => LAYERBLEND_FRAMEWORK_DIR . 'img/3col.png' ),
				'4' => array( 'title' => '4 Columns', 'img' => LAYERBLEND_FRAMEWORK_DIR . 'img/4col.png' )
			),
			'default'  => '4',
			'required' => array( 'enable_footer', '=', true )
		),
		array(
			'id'       => 'footer_container',
			'type'     => 'radio',
			'title'    => esc_html__( 'Footer layout', 'alimo' ),
			'subtitle' => esc_html__( 'Make the footer layout boxed or full width (default: boxed).', 'alimo' ),
			'options'  => array(
				'1' => 'Boxed',
				'2' => 'Full Width'
			),
			'default'  => '1',
			'required' => array( 'enable_footer', '=', true ),
		),
		// Footer background color
		array(
			'id'       => 'footer-bg-color',
			'type'     => 'color_rgba',
			'title'    => esc_html__( 'Footer Background Color', 'alimo' ),
			'subtitle' => esc_html__( 'Pick a background color for the footer (default: rgba(0,129,255,0.67) ).', 'alimo' ),
			'default'  => array(
				'color' => '#F3F3F3',
				'alpha' => 0.8,
			),
			'required' => array( 'enable_footer', '=', true )
		),
		// Footer background image
		array(
			'id'       => 'footer-bg-image',
			'type'     => 'media',
			'url'      => false,
			'title'    => esc_html__( 'Footer Background Image', 'alimo' ),
			'compiler' => 'true',
			'subtitle' => esc_html__( 'If you would like to use a backgorund image for the footer area, upload it here.', 'alimo' ),
			'desc'     => wp_kses(__( 'Note: the selected Footer Background Color will be used as an overlay. <br> <span style="color:red">Please make sure the selected color has a minimum transparency setting or the image won\'t be visible.</span>', 'alimo' ), array('br' =>array(), 'span' =>array())),
			'required' => array( 'enable_footer', '=', true ),
			'default'  => ''
		),
		// Above footer widget area
		array(
			'id'       => 'enable_widget_above_footer',
			'type'     => 'switch',
			'title'    => esc_html__( 'Widget area above footer', 'alimo' ),
			'subtitle' => esc_html__( 'Add an extra widget area above the footer.', 'alimo' ),
			'desc'     => sprintf( wp_kses( __( 'You can add or remove widgets by going to the <a href="%s">Widget Area</a>.', 'alimo' ), array(  'a' => array( 'href' => array() ) ) ), esc_url( admin_url('widgets.php') ) ),
			'default'  => false,
			'on'       => 'Enable',
			'off'      => 'Disable',
			'required' => array( 'enable_footer', '=', true )
		),
	)
) );

/*
 * DESIGN > PAGE TRANSITION SECTION
 */
Redux::setSection( $opt_name, array(
	'icon_type'  => 'image',
	'icon'       => LAYERBLEND_FRAMEWORK_DIR . 'img/icon-chevron-right.svg',
	'title'      => esc_html__( 'Page transition', 'alimo' ),
	'help_area'  => help_section( '#design-page-transition' ),
	'subsection' => true,
	'fields'     => array(
		// Enable smooth page transition
		array(
			'id'      => 'smooth-transition',
			'type'    => 'switch',
			'title'   => esc_html__( 'Enable smooth page transition', 'alimo' ),
			'default' => true
		),
		// Header background color
		array(
			'id'       => 'transition-bg-color',
			'type'     => 'color_rgba',
			'title'    => esc_html__( 'Background Color', 'alimo' ),
			'subtitle' => esc_html__( 'Pick a background color for the transition background.', 'alimo' ),
			'default'  => array(
				'color' => '#ffffff',
				'alpha' => 1
			),
			'required' => array( 'smooth-transition', '=', true )
		),
		// Header background color
		array(
			'id'       => 'transition-el-color',
			'type'     => 'color_rgba',
			'title'    => esc_html__( 'Spinner and Message Color', 'alimo' ),
			'subtitle' => esc_html__( 'Pick a color for the transition spinner and message.', 'alimo' ),
			'default'  => array(
				'color' => '#000000',
				'alpha' => 1
			),
			'required' => array( 'smooth-transition', '=', true )
		),
		// Enable preload message
		array(
			'id'       => 'transition-message-enabled',
			'type'     => 'switch',
			'title'    => esc_html__( 'Enable Transition Message', 'alimo' ),
			'subtitle' => esc_html__( 'Enable or disable the message that appears on the transition.', 'alimo' ),
			'default'  => true,
			'required' => array( 'smooth-transition', '=', true )
		),
		// Custom preload message
		array(
			'id'       => 'transition-message',
			'type'     => 'text',
			'title'    => esc_html__( 'Transition Message', 'alimo' ),
			'subtitle' => esc_html__( 'Customise the Transition message. By default it uses the blog description.', 'alimo' ),
			'default'  => get_bloginfo( 'description' ),
			'required' => array( 'transition-message-enabled', '=', true )
		),
		// Message font
		array(
			'id'          => 'transition-message-font',
			'type'        => 'typography',
			'title'       => esc_html__( 'Font', 'alimo' ),
			'google'      => true,
			'font-backup' => false,
			'text-align'  => false,
			'subsets'     => false,
			'font-size'   => true,
			'font-style'  => false,
			'font-weight' => false,
			'color'       => false,
			'line-height' => false,
			'units'       => 'px',
			'subtitle'    => esc_html__( 'This font will be used in case no logo or banner logo image is provided', 'alimo' ),
			'default'     => array(
				'font-family' => 'Poppins',
				'google'      => true,
				'font-size'   => '55px',
			),
		),
	)
) );

/*
 * POSTS SECTION
 */
Redux::setSection( $opt_name, array(
	'icon_type' => 'image',
	'icon'      => LAYERBLEND_FRAMEWORK_DIR . 'img/icon-posts.svg',
	'title'     => esc_html__( 'Posts', 'alimo' ),
	'fields'    => array()
) );

/*
 * POSTS > HEADER SECTION
 */
Redux::setSection( $opt_name, array(
	'icon_type'  => 'image',
	'icon'       => LAYERBLEND_FRAMEWORK_DIR . 'img/icon-chevron-right.svg',
	'title'      => esc_html__( 'Header', 'alimo' ),
	'desc'       => esc_html__( 'Settings for the header section of the single post pages', 'alimo' ),
	'help_area'  => help_section( '#posts-header' ),
	'subsection' => true,
	'fields'     => array(
		// Header overlay color
		array(
			'id'       => 'post_header_overlay',
			'type'     => 'color_rgba',
			'title'    => esc_html__( 'Header overlay color', 'alimo' ),
			'subtitle' => esc_html__( 'Pick an overlay color for the header', 'alimo' ),
			'default'  => array(
				'color' => '#000000',
				'alpha' => 0.5
			),
		),
		// Author details
		array(
			'id'      => 'author-display',
			'type'    => 'switch',
			'title'   => esc_html__( 'Author details', 'alimo' ),
			'default' => true,
			'on'      => esc_html__( 'Show', 'alimo' ),
			'off'     => esc_html__( 'Hide', 'alimo' ),
		),
		/*// Date toggle
		array(
			'id'      => 'calendar-display',
			'type'    => 'switch',
			'title'   => esc_html__( 'Publish date', 'alimo' ),
			'default' => true,
			'on'      => esc_html__( 'Show', 'alimo' ),
			'off'     => esc_html__( 'Hide', 'alimo' ),
		),*/
		// Category toggle
		array(
			'id'       => 'category-display',
			'type'     => 'switch',
			'title'    => esc_html__( 'Category', 'alimo' ),
			'default'  => true,
			'on'       => esc_html__( 'Show', 'alimo' ),
			'off'      => esc_html__( 'Hide', 'alimo' ),
			'subtitle' => esc_html__( 'Display the category(ies)', 'alimo' ),
		),
		// Comment count
		array(
			'id'       => 'comments-number-display',
			'type'     => 'switch',
			'title'    => esc_html__( 'Comment count', 'alimo' ),
			'default'  => true,
			'on'       => esc_html__( 'Show', 'alimo' ),
			'off'      => esc_html__( 'Hide', 'alimo' ),
			'subtitle' => esc_html__( 'Display the number of comments', 'alimo' ),
		),
		// Use facebook comments
		array(
			'id'      => 'use-facebook-comments',
			'type'    => 'switch',
			'title'   => esc_html__( 'Use Facebook comments', 'alimo' ),
			'subtitle' => esc_html__( 'Enable this option to replace the default WordPress comments with the Facebook comments plugin', 'alimo' ),
			'default' => false,
		),
	),
) );


/*
 * POSTS > FOOTER SECTION
 */
Redux::setSection( $opt_name, array(
	'icon_type'  => 'image',
	'icon'       => LAYERBLEND_FRAMEWORK_DIR . 'img/icon-chevron-right.svg',
	'title'      => esc_html__( 'Footer', 'alimo' ),
	'desc'       => esc_html__( 'Settings for the footer section of the single post pages', 'alimo' ),
	'help_area'  => help_section( '#posts-footer' ),
	'subsection' => true,
	'fields'     => array(
		/*// Author details
		array(
			'id'      => 'author-footer-display',
			'type'    => 'switch',
			'title'   => esc_html__( 'Author details', 'alimo' ),
			'default' => true,
			'on'      => esc_html__( 'Show', 'alimo' ),
			'off'     => esc_html__( 'Hide', 'alimo' ),
		),
		// Pagination
		array(
			'id'      => 'pagination-display',
			'type'    => 'switch',
			'title'   => esc_html__( 'Pagination', 'alimo' ),
			'default' => true,
			'on'      => esc_html__( 'Show', 'alimo' ),
			'off'     => esc_html__( 'Hide', 'alimo' ),
		),*/
		// Comments
		array(
			'id'       => 'comments-display',
			'type'     => 'switch',
			'title'    => esc_html__( 'Comments', 'alimo' ),
			'subtitle' => esc_html__( 'Show the comments and the comment form', 'alimo' ),
			'default'  => true,
			'on'       => esc_html__( 'Show', 'alimo' ),
			'off'      => esc_html__( 'Hide', 'alimo' ),
		),
	),
) );

/*
 * POSTS > READING FEATURES SECTION
 */
Redux::setSection( $opt_name, array(
	'icon_type'  => 'image',
	'icon'       => LAYERBLEND_FRAMEWORK_DIR . 'img/icon-chevron-right.svg',
	'title'      => esc_html__( 'Reading features', 'alimo' ),
	'desc'       => esc_html__( 'Configure the reading features of the single posts pages', 'alimo' ),
	'help_area'  => help_section( '#posts-reading-features' ),
	'subsection' => true,
	'fields'     => array(
		// Reading mode
		array(
			'id'       => 'reading-mode',
			'type'     => 'switch',
			'title'    => esc_html__( 'Reading mode', 'alimo' ),
			'default'  => true,
			'subtitle' => esc_html__( 'Enable the reading mode feature in single posts. ', 'alimo' ),
		),
		// Reading mode Serif Font
		array(
			'id'          => 'reading-mode-serif',
			'type'        => 'typography',
			'title'       => esc_html__( 'Reading Mode Serif Font', 'alimo' ),
			'google'      => true,
			'font-backup' => false,
			'text-align'  => false,
			'subsets'     => false,
			'font-size'   => false,
			'font-style'  => false,
			'font-weight' => false,
			'color'       => false,
			'line-height' => false,
			'output'      => array( '.reading-mode-container .reading-options .reading-options-container .ff-options .font-box.serif .typeface, .reading-mode-container .reading-mode-content-container.serif *' ),
			'units'       => 'px',
			'subtitle'    => esc_html__( 'Select the Reading Mode Serif font', 'alimo' ),
			'default'     => array(
				'font-family' => 'Poppins',
				'google'      => true,
			),
			'required' => array( 'reading-mode', '=', true ),
		),
		// Reading mode Sans Serif Font
		array(
			'id'          => 'reading-mode-sans-serif',
			'type'        => 'typography',
			'title'       => esc_html__( 'Reading Mode Sans Serif Font', 'alimo' ),
			'google'      => true,
			'font-backup' => false,
			'text-align'  => false,
			'subsets'     => false,
			'font-size'   => false,
			'font-style'  => false,
			'font-weight' => false,
			'color'       => false,
			'line-height' => false,
			'output'      => array( '.reading-mode-container .reading-options .reading-options-container .ff-options .font-box.sans-serif .typeface, .reading-mode-container .reading-mode-content-container.sans-serif *' ),
			'units'       => 'px',
			'subtitle'    => esc_html__( 'Select the Reading Mode Sans Serif font', 'alimo' ),
			'default'     => array(
				'font-family' => 'Muli',
				'google'      => true,
			),
			'required' => array( 'reading-mode', '=', true ),
		),
		// Reading progress bar
		array(
			'id'       => 'reading-progress-bar',
			'type'     => 'switch',
			'title'    => esc_html__( 'Reading progress bar', 'alimo' ),
			'default'  => true,
			'subtitle' => esc_html__( 'Enable the reading progress bar feature in single posts. ', 'alimo' ),
			'required' => array( 'reading-mode', '=', true ),
		),
		// Progress bar color
		array(
			'id'          => 'reading-progress-bar-color',
			'type'        => 'color',
			'title'       => esc_html__( 'Reading progress bar color', 'alimo' ),
			'subtitle'    => esc_html__( 'Pick an color for the reading progress bar', 'alimo' ),
			'default'     => '',
			'transparent' => false,
			'required'    => array(
				array( 'reading-mode', '=', true ),
				array( 'reading-progress-bar', '=', true ),
			),
		),
	),
) );

/*
 * POSTS > SOCIAL SHARING SECTION
 */
Redux::setSection( $opt_name, array(
	'icon_type'  => 'image',
	'icon'       => LAYERBLEND_FRAMEWORK_DIR . 'img/icon-chevron-right.svg',
	'title'      => esc_html__( 'Social sharing', 'alimo' ),
	'subtitle'   => wp_kses(__( 'Configure the <b>social quote sharer</b> feature of the single posts pages and social media networks in which to share the posts.', 'alimo' ), array('b' => array()) ),
	'help_area'  => help_section( '#posts-sharing' ),
	'subsection' => true,
	'fields'     => array(
		// Enable social media sharing
		array(
			'id'      => 'social-media-sharing',
			'type'    => 'switch',
			'title'   => esc_html__( 'Social media sharing', 'alimo' ),
			'desc'    => wp_kses(__( 'Enable social media sharing for posts. Available networks can be configured in Social Media > <a class="redux-group-tab-link-a" data-rel="20" href="#">Sharing networks</a>', 'alimo' ), array('a' => array('class'=>array(),'data-rel'=>array(),'href'=>array() ) ) ),
			'default' => true,

		),
		// Enable social quote sharer
		array(
			'id'      => 'social-quote-sharer',
			'type'    => 'switch',
			'title'   => esc_html__( 'Quote sharer', 'alimo' ),
			'default' => true,
			'desc'    => wp_kses(__( 'Popover menu to share any text selected on the page. Configure settings in Social Media > <a  class="redux-group-tab-link-a" data-rel="21" href="#">Quote Sharer</a>', 'alimo' ),array('a' => array('class'=>array(),'data-rel'=>array(),'href'=>array()) )),
		),
		// Enable post likes
		array(
			'id'      => 'single-post-likes',
			'type'    => 'switch',
			'title'   => esc_html__( 'Post likes', 'alimo' ),
			'desc'    => esc_html__( 'Show likes (hearts) on single posts.', 'alimo' ),
			'default' => true,
            'required' => array('enable_post_likes', '=', true),
			'on'       => esc_html__( 'Show', 'alimo' ),
			'off'      => esc_html__( 'Hide', 'alimo' ),

		),
	),
) );

/*
 * POSTS > POST LIKES SECTION
 */
Redux::setSection( $opt_name, array(
	'icon_type'  => 'image',
	'icon'       => LAYERBLEND_FRAMEWORK_DIR . 'img/icon-chevron-right.svg',
	'title'      => esc_html__('Post likes','alimo'),
	'id'         => 'post_likes_settings',
	'subsection' => true,
	'help_area'  => help_section( '#posts-likes' ),
	'fields'     => array(
		array(
			'id'      => 'enable_post_likes',
			'type'    => 'switch',
			'title'   => esc_html__('Enable post likes feature','alimo'),
			'desc'    => wp_kses(__('Choose whether to enable or disable the post likes (hearts) system.<br><span style="color:blue">Notice: if disabled, posts likes will be disabled completely and will not show up in listings, single post pages or widgets. If you just want to disable it from the single post page, go to Posts > <a href="#" data-rel="15" class="redux-group-tab-link-a">Social sharing</a> and disable it there.</span>','alimo'), array('br' => array(), 'span' => array(), 'a' => array('class'=>array(),'data-rel'=>array(),'href'=>array()) ) ),
			'on'      => 'Enable',
			'off'     => 'Disable',
			'default' => true,
		),
		array(
			'id'       => 'likes_message',
			'title'    => esc_html__('Likes config','alimo'),
			'type'     => 'section',
			'subtitle' => esc_html__( 'Use this section to modify the message next to the post likes in the standard blog listing', 'alimo' ),
			'indent'   => true,
			'required' => array( 'enable_post_likes', '=', true )
		),
		array(
			'id'       => 'likes_zero',
			'type'     => 'text',
			'title'    => esc_html__( 'No post likes', 'alimo' ),
			'desc'     => esc_html__( 'The text after the count when no one has liked a post. Leave blank for no text after the count. Default: Show some love', 'alimo' ),
			'default'  => 'Show some love',
			'required' => array( 'enable_post_likes', '=', true )
		),
		array(
			'id'       => 'likes_one',
			'type'     => 'text',
			'title'    => esc_html__( 'One post like', 'alimo' ),
			'desc'     => esc_html__( 'The text after the count when one person has liked a post. Leave blank for no text after the count. Default: Person loves this', 'alimo' ),
			'default'  => 'Person loves this',
			'required' => array( 'enable_post_likes', '=', true )
		),
		array(
			'id'       => 'likes_more',
			'type'     => 'text',
			'title'    => esc_html__( 'Multiple post likes', 'alimo' ),
			'desc'     => esc_html__( 'The text after the count when more than one person has liked a post. Leave blank for no text after the count. Default: People love this', 'alimo' ),
			'default'  => 'People love this',
			'required' => array( 'enable_post_likes', '=', true )
		),
		array(
			'type'   => 'section',
			'indent' => false
		),
	)
) );

/*
 * POSTS > EXCERPT SECTION
 */
Redux::setSection( $opt_name, array(
	'icon_type'  => 'image',
	'icon'       => LAYERBLEND_FRAMEWORK_DIR . 'img/icon-chevron-right.svg',
	'title'      => esc_html__('Excerpt','alimo'),
	'id'         => 'post_excerpt',
	'subsection' => true,
	'help_area'  => help_section( '#posts-excerpt' ),
	'fields'     => array(
		array(
			'id'      => 'excerpt_length',
			'type'    => 'text',
			'title'   => esc_html__('Excerpt length','alimo'),
			'desc'    => esc_html__('Enter a custom excerpt length. Default is 30 words','alimo'),
			'default' => '30'
		),
		array(
			'id'      => 'excerpt_more',
			'type'    => 'text',
			'title'   => esc_html__('Excerpt \'read more\' trim','alimo'),
			'desc'    => esc_html__('Enter a custom \'read more\' trim. Default is \'...\'','alimo'),
			'default' => '...'
		),
	)
) );

/*
 * SOCIAL MEDIA SECTION
 */
Redux::setSection( $opt_name, array(
	'icon_type' => 'image',
	'icon'      => LAYERBLEND_FRAMEWORK_DIR . 'img/icon-social.svg',
	'title'     => esc_html__( 'Social Media', 'alimo' ),
) );

/*
 * SOCIAL MEDIA > PROFILES SECTION
 */
Redux::setSection( $opt_name, array(
	'icon_type'  => 'image',
	'icon'       => LAYERBLEND_FRAMEWORK_DIR . 'img/icon-chevron-right.svg',
	'title'      => esc_html__( 'Profiles', 'alimo' ),
	'desc'       => wp_kses(__( 'Enter in your social media locations here and then activate which ones you would like to display on your site. 
                            For the header, go to the <a class="redux-group-tab-link-a" data-rel="4" href="#">header layout</a> tab. 
                            <br/>
                            You can also display them elsewhere (in widgets for example) using the <a href="http://help.layerblend.com/#shortcodes" target="_blank">[alimo_social_profile] shortcode <i class="fa fa-external-link"></i></a>.
                            <br/>
                            <br/>
                            <strong>Remember to include the "http://" or "https://" in all URLs!</strong>', 'alimo' ), array('a' => array('class'=>array(),'data-rel'=>array(),'href'=>array()),'br' => array(),'i' => array(),'strong' => array()) ),
	'help_area'  => help_section( '#social-media-profiles' ),
	'subsection' => true,
	'fields'     => array(

		array(
			'id'       => 'facebook-url',
			'type'     => 'text',
			'title'    => esc_html__( 'Facebook URL', 'alimo' ),
			'subtitle' => esc_html__( 'Please enter in your Facebook URL.', 'alimo' ),
            'default'  => ''
		),
		array(
			'id'       => 'twitter-url',
			'type'     => 'text',
			'title'    => esc_html__( 'Twitter URL', 'alimo' ),
			'subtitle' => esc_html__( 'Please enter in your Twitter URL.', 'alimo' ),
            'default'  => ''
		),
		array(
			'id'       => 'google-plus-url',
			'type'     => 'text',
			'title'    => esc_html__( 'Google+ URL', 'alimo' ),
			'subtitle' => esc_html__( 'Please enter in your Google+ URL.', 'alimo' ),
            'default'  => ''
		),
		array(
			'id'       => 'vimeo-url',
			'type'     => 'text',
			'title'    => esc_html__( 'Vimeo URL', 'alimo' ),
			'subtitle' => esc_html__( 'Please enter in your Vimeo URL.', 'alimo' ),
            'default'  => ''
		),
		array(
			'id'       => 'dribbble-url',
			'type'     => 'text',
			'title'    => esc_html__( 'Dribbble URL', 'alimo' ),
			'subtitle' => esc_html__( 'Please enter in your Dribbble URL.', 'alimo' ),
            'default'  => ''
		),
		array(
			'id'       => 'pinterest-url',
			'type'     => 'text',
			'title'    => esc_html__( 'Pinterest URL', 'alimo' ),
			'subtitle' => esc_html__( 'Please enter in your Pinterest URL.', 'alimo' ),
            'default'  => ''
		),
		array(
			'id'       => 'youtube-url',
			'type'     => 'text',
			'title'    => esc_html__( 'Youtube URL', 'alimo' ),
			'subtitle' => esc_html__( 'Please enter in your Youtube URL.', 'alimo' ),
            'default'  => ''
		),
		array(
			'id'       => 'tumblr-url',
			'type'     => 'text',
			'title'    => esc_html__( 'Tumblr URL', 'alimo' ),
			'subtitle' => esc_html__( 'Please enter in your Tumblr URL.', 'alimo' ),
            'default'  => ''
		),
		array(
			'id'       => 'linkedin-url',
			'type'     => 'text',
			'title'    => esc_html__( 'LinkedIn URL', 'alimo' ),
			'subtitle' => esc_html__( 'Please enter in your LinkedIn URL.', 'alimo' ),
            'default'  => ''
		),
		array(
			'id'       => 'rss-url',
			'type'     => 'text',
			'title'    => esc_html__( 'RSS URL', 'alimo' ),
			'subtitle' => esc_html__( 'If you have an external RSS feed such as Feedburner, please enter it here. Will use built in Wordpress feed if left blank.', 'alimo' ),
            'default'  => ''
		),
		array(
			'id'       => 'behance-url',
			'type'     => 'text',
			'title'    => esc_html__( 'Behance URL', 'alimo' ),
			'subtitle' => esc_html__( 'Please enter in your Behance URL.', 'alimo' ),
            'default'  => ''
		),
		array(
			'id'       => 'flickr-url',
			'type'     => 'text',
			'title'    => esc_html__( 'Flickr URL', 'alimo' ),
			'subtitle' => esc_html__( 'Please enter in your Flickr URL.', 'alimo' ),
            'default'  => ''
		),
		array(
			'id'       => 'spotify-url',
			'type'     => 'text',
			'title'    => esc_html__( 'Spotify URL', 'alimo' ),
			'subtitle' => esc_html__( 'Please enter in your Spotify URL.', 'alimo' ),
            'default'  => ''
		),
		array(
			'id'       => 'instagram-url',
			'type'     => 'text',
			'title'    => esc_html__( 'Instagram URL', 'alimo' ),
			'subtitle' => esc_html__( 'Please enter in your Instagram URL.', 'alimo' ),
            'default'  => ''
		),
		array(
			'id'       => 'github-url',
			'type'     => 'text',
			'title'    => esc_html__( 'GitHub URL', 'alimo' ),
			'subtitle' => esc_html__( 'Please enter in your GitHub URL.', 'alimo' ),
            'default'  => ''
		),
		array(
			'id'       => 'stackexchange-url',
			'type'     => 'text',
			'title'    => esc_html__( 'StackExchange URL', 'alimo' ),
			'subtitle' => esc_html__( 'Please enter in your StackExchange URL.', 'alimo' ),
            'default'  => ''
		),
		array(
			'id'       => 'soundcloud-url',
			'type'     => 'text',
			'title'    => esc_html__( 'SoundCloud URL', 'alimo' ),
			'subtitle' => esc_html__( 'Please enter in your SoundCloud URL.', 'alimo' ),
            'default'  => ''
		),
		array(
			'id'       => 'vk-url',
			'type'     => 'text',
			'title'    => esc_html__( 'VK URL', 'alimo' ),
			'subtitle' => esc_html__( 'Please enter in your VK URL.', 'alimo' ),
            'default'  => ''
		),
		array(
			'id'       => 'vine-url',
			'type'     => 'text',
			'title'    => esc_html__( 'Vine URL', 'alimo' ),
			'subtitle' => esc_html__( 'Please enter in your Vine URL.', 'alimo' ),
            'default'  => ''
		)
	),
) );
/*
 * SOCIAL MEDIA > SHARING NETWORKS SECTION
 */
Redux::setSection( $opt_name, array(
	'icon_type'  => 'image',
	'icon'       => LAYERBLEND_FRAMEWORK_DIR . 'img/icon-chevron-right.svg',
	'title'      => esc_html__( 'Sharing Networks', 'alimo' ),
	'desc'       => esc_html__( 'Select which networks your posts can be shared on.', 'alimo' ),
	'help_area'  => help_section( '#social-media-sharing-networks' ),
	'subsection' => true,
	'fields'     => array(
		// Post sharing options
		array(
			'id'      => 'post_sharing_networks',
			'type'    => 'checkbox',
			'title'   => esc_html__( 'Post sharing networks', 'alimo' ),
			'multi'   => true,
			'options' => array(
				'facebook'  => 'Facebook',
				'twitter'   => 'Twitter',
				'gplus'     => 'Google+',
				'pinterest' => 'Pinterest',
				'linkedin'  => 'LinkedIn',
				'reddit'    => 'Reddit',
				'stumble'   => 'Stumbleupon',
				'tumblr'    => 'Tumblr',
				'vk'        => 'VK',
			),
			'default' => array(
				'facebook'  => '0',
				'twitter'   => '0',
				'gplus'     => '0',
				'pinterest' => '0',
				'linkedin'  => '0',
				'reddit'    => '0',
				'stumble'   => '0',
				'tumblr'    => '0',
				'vk'        => '0',
			),
		),
	),
) );

/*
 * SOCIAL MEDIA > QUOTE SHARER SECTION
 */
Redux::setSection( $opt_name, array(
	'icon_type'  => 'image',
	'icon'       => LAYERBLEND_FRAMEWORK_DIR . 'img/icon-chevron-right.svg',
	'title'      => esc_html__( 'Quote Sharer', 'alimo' ),
	'desc'       => wp_kses(__( 'Enable Medium-like quote sharing for your posts. Twitter and Email work by default, to enable facebook share, add your Facebook App ID below and also make sure Quote Sharing is enabled <a class="redux-group-tab-link-a" data-rel="15" href="#">here</a>', 'alimo' ), array('a' => array('class'=>array(),'data-rel'=>array(),'href'=>array())) ),
	'help_area'  => help_section( '#social-media-quote-sharer' ),
	'subsection' => true,
	'fields'     => array(
		// Facebook App ID
		array(
			'id'       => 'social-quote-sharer-facebook-id',
			'type'     => 'text',
			'title'    => esc_html__( 'Facebook Application ID', 'alimo' ),
			'desc'     => wp_kses(__( 'Create a facebook application and add here the ID. Tutorial: <a target="_blank" href="https://developers.facebook.com/docs/apps/register"> How to create an facebook application </a>', 'alimo' ), array('a' => array('class'=>array(),'data-rel'=>array(),'href'=>array())) ),
            'default'  => ''
		),
	),
) );

global $woocommerce;
if($woocommerce):
    /*
     * WOOCOMMERCE SECTION
     */
    Redux::setSection( $opt_name, array(
        'icon_type' => 'image',
        'icon'      => LAYERBLEND_FRAMEWORK_DIR . 'img/icon-shop.svg',
        'title'     => esc_html__( 'WooCommerce', 'alimo' ),
        'fields'     => array(
	        // Enable sidebar
	        array(
		        'id'       => 'woo-enable-sidebar',
		        'type'     => 'switch',
		        'title'    => esc_html__( 'Enable sidebar', 'alimo' ),
		        'default'  => true,
	        ),
	        // Sidebar position
	        array(
		        'id'       => 'woo-sidebar-position',
		        'type'     => 'radio',
		        'title'    => esc_html__( 'Sidebar position', 'alimo' ),
		        'options'  => array(
			        'left'  => 'Left',
			        'right' => 'Right',
		        ),
		        'default'  => 'right',
		        'required' => array( 'woo-enable-sidebar', 'equals', true ),
	        ),
	        // Enable sidebar
	        array(
		        'id'       => 'woo-enable-sidebar-product',
		        'type'     => 'switch',
		        'title'    => esc_html__( 'Enable sidebar on single product page', 'alimo' ),
		        'default'  => false,
		        'required' => array( 'woo-enable-sidebar', 'equals', true ),
	        ),
        )
    ) );


endif;


/*
 * THEME INFORMATION SECTION
 */
Redux::setSection( $opt_name, array(
	'icon_type' => 'image',
	'icon'      => LAYERBLEND_FRAMEWORK_DIR . 'img/icon-layerblend.svg',
	'title'     => esc_html__( 'Theme Information', 'alimo' ),
	'desc'      => wp_kses(__( '<p class="description">alimo is awesome yet fresh.</p>', 'alimo' ), array('p' => array() ) ),
	'fields'    => array(
		array(
			'id'      => 'opt-raw-info',
			'type'    => 'raw',
			'content' => $item_info,
		)
	),
) );

/*
 * IMPORT / EXPORT SECTION
 */
Redux::setSection( $opt_name, array(
	'icon_type' => 'image',
	'icon'      => LAYERBLEND_FRAMEWORK_DIR . 'img/icon-import.svg',
	'title'     => esc_html__( 'Import / Export', 'alimo' ),
	'desc'      => esc_html__( 'Import and Export your Redux Framework settings from file, text or URL.', 'alimo' ),
	'help_area' => help_section( '#import-export', 'danger' ),
	'fields'    => array(
		array(
			'id'         => 'opt-import-export',
			'type'       => 'import_export',
			'title'      => esc_html__( 'Import Export', 'alimo' ),
			'subtitle'   => esc_html__( 'Save and restore your theme settings', 'alimo' ),
			'full_width' => false,
		),
	),
) );


/********************************************************************************************************
 *
 *                          E N D   S E C T I O N S    D E C L A R A T I O N
 *
 ********************************************************************************************************/



/**
 * Helper function for getting and setting plugin values.
 * @param $opt_key
 * @param $opt_val
 * @param $opt_group
 */
function alimo_alt_update_option($opt_key,$opt_val,$opt_group){
	// get options-data as it exists before update
	$options = get_option($opt_group);
	// update it
	$options[$opt_key] = $opt_val;
	// store updated data
	update_option($opt_group,$options);
}

add_action('redux/options/alimo_data/saved','alimo_update_zilla_fields',1,1);
/**
 * Read options set by user for likes feature and set them in the plugin
 * @param $value
 */
function alimo_update_zilla_fields($value){
	global $alimo_data;
	$zero = $alimo_data['likes_zero'];
	$one = $alimo_data['likes_one'];
	$more = $alimo_data['likes_more'];
	alimo_alt_update_option('zero_postfix',$zero,'zilla_likes_settings');
	alimo_alt_update_option('one_postfix',$one,'zilla_likes_settings');
	alimo_alt_update_option('more_postfix',$more,'zilla_likes_settings');

}



/**
 * Admin panel Custom CSS
 */
if ( ! function_exists( 'addPanelCSS' ) ):

	function addPanelCSS() {
		wp_register_style(
			'layerblend-admin-custom-css',
			LAYERBLEND_FRAMEWORK_DIR . 'css/admin-panel.css',
			array( 'redux-admin-css' ), // Be sure to include redux-admin-css so it's appended after the core css is applied
			time(),
			'all'
		);
		wp_register_style(
			'bootstrap-alerts',
			LAYERBLEND_FRAMEWORK_DIR . 'css/bootstrap/alerts.css',
			array( 'redux-admin-css' ),
			'3.3.7',
			'all'
		);

		wp_register_style(
			'font-awesome',
			LAYERBLEND_FRAMEWORK_DIR . 'css/font-awesome.min.css',
			array( 'redux-admin-css' ),
			'4.7.0',
			'all'
		);
		wp_register_style(
			'muli-google-font',
			'https:////fonts.googleapis.com/css?family=Muli',
			array( 'redux-admin-css' ),
			'4.7.0',
			'all'
		);
		wp_enqueue_style( 'layerblend-admin-custom-css' );
		wp_enqueue_style( 'custom-login' );
		wp_enqueue_style( 'bootstrap-alerts' );
		wp_enqueue_style( 'font-awesome' );
		wp_enqueue_style( 'muli-google-font' );
		wp_dequeue_style( array( 'wp-jquery-ui', 'wp-jquery-ui-dialog', 'wp-jquery-ui-core' ) );
		wp_deregister_style( array( 'wp-jquery-ui', 'wp-jquery-ui-dialog', 'wp-jquery-ui-core' ) );
	}

	add_action( 'redux/page/alimo_data/enqueue', 'addPanelCSS', 100 );

endif;
