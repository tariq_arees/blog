<?php
    /**
     * The template for the panel header area.
     * Override this template by specifying the path where it is stored (templates_path) in your Redux config.
     *
     * @author      Redux Framework
     * @package     ReduxFramework/Templates
     * @version:    3.5.4.18
     */

    $tip_title = esc_html__( 'Developer Mode Enabled', 'alimo' );

    if ( $this->parent->dev_mode_forced ) {
        $is_debug     = false;
        $is_localhost = false;

        $debug_bit = '';
        if ( Redux_Helpers::isWpDebug() ) {
            $is_debug  = true;
            $debug_bit = esc_html__( 'WP_DEBUG is enabled', 'alimo' );
        }

        $localhost_bit = '';
        if ( Redux_Helpers::isLocalHost() ) {
            $is_localhost  = true;
            $localhost_bit = esc_html__( 'you are working in a localhost environment', 'alimo' );
        }

        $conjunction_bit = '';
        if ( $is_localhost && $is_debug ) {
            $conjunction_bit = ' ' . esc_html__( 'and', 'alimo' ) . ' ';
        }

        $tip_msg = esc_html__( 'This has been automatically enabled because', 'alimo' ) . ' ' . $debug_bit . $conjunction_bit . $localhost_bit . '.';
    } else {
        $tip_msg = esc_html__( 'If you are not a developer, your theme/plugin author shipped with developer mode enabled. Contact them directly to fix it.', 'alimo' );
    }

?>
<div id="redux-header">
    <?php if ( ! empty( $this->parent->args['display_name'] ) ) { ?>
        <div class="display_header">
            <h2>
                <svg version="1.1" id="alimo_logo" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     width="640px" height="248.569px" viewBox="0 195.234 640 248.569" enable-background="new 0 195.234 640 248.569"
                     xml:space="preserve">
                        <g>
                            <path d="M345.81,277.54c0.676-26.093-45.042-21.604-45.696,3.376C298.786,308.711,345.848,304.65,345.81,277.54z"/>
                            <path d="M300.113,280.916C300.171,278.699,299.869,286.031,300.113,280.916L300.113,280.916z"/>
                            <path d="M398.973,412.479C398.973,411.396,398.97,415.146,398.973,412.479L398.973,412.479z"/>
                            <path d="M626.952,350.115c-12.716-14.108-27.936-26.678-46.594-31.787c-40.611-11.123-75.522,17.794-77.47,58.592
                                c-0.283,5.955,0.244,12.105,1.51,18.092c-10.395,9.289-22.817,16.715-25.888,3.18c-4.177-18.424,3.052-39.694,4.568-58.166
                                c0.797-9.696,2.306-21.376-9.978-23.729c-9.129-1.75-14.801,4.812-19.61,11.578c-9.317,12.739-16.742,26.685-25.457,39.81
                                c-8.58-9.045,22.481-52.386-7.219-51.666c-19.784-0.465-32.643,38.322-41.556,51.666c-3.962-6.278,2.301-24.582,3.072-32.615
                                c1.167-12.129,1.028-19.238-12.812-19.06c-13.467-0.245-14.901,16.207-17.221,26.457c-3.019,13.345-5.257,26.901-7.003,40.468
                                c-0.498,3.799-1.119,7.89-1.643,12.092c-8.81,8.152-18.687,16.341-29.316,16.83c-13.233,1.222-2.691-37.242-1.051-43.652
                                c3.715-14.512,10.702-30.072,11.408-45.098c0.726-14.183-26.157-4.71-30.39,1.955c-3.971,6.254-4.648,16.401-6.527,23.512
                                c-2.356,8.863-4.579,17.77-6.562,26.725c-0.873,3.939-1.928,8.219-2.892,12.651c-6.96,5.173-14.237,9.999-21.972,13.796
                                c-11.298,5.559-16.961,2.615-18.369-10.29c-2.043-18.738,12.846-38.004,21.323-53.466c12.935-23.596,26.289-47.526,34.546-73.247
                                c5.356-16.682,15.035-49.584,0.168-64.278c-18.477-18.262-44.416,16.115-52.509,29.594c-13.486,22.46-22.284,47.555-28.687,72.858
                                c-6.474,25.578-10.652,52.407-7.517,78.791c0.254,2.14,0.59,4.626,1.025,7.326c-9.956,5.918-19.987,11.97-30.857,15.771
                                c-15.078,5.766-10.93-15.935-10.395-25.018c0.821-13.932,6.814-27.477,10.783-40.812c4.441-14.921,8.766-29.907,12.071-45.127
                                c3.099-14.269,5.731-29.239,3.662-43.846c-0.877-6.192-2.745-13.899-9.412-16.259c-5.456-1.799-11.393-1.009-16.791-2.922
                                c-6.279-2.225-0.401-4.713-5.237-8.255c-5.133-3.75-13.525-2.583-19.381-2.122c-15.804,1.246-31.188,6.185-45.194,13.528
                                C42.898,261.586-6.056,334.296,5.937,395.854c6.072,31.165,33.05,51.857,65.026,47.328c30.947-4.383,54.577-30.322,67.633-57.139
                                c-3.465,20.426-5.467,56.273,24.728,53.58c17.761-1.584,36.138-11.653,50.32-23.713c4.55,10.496,11.35,19.04,21.522,19.835
                                c-0.84-0.008-1.279,0.008,0.915,0.076c0.524-0.009,0.724-0.018,0.728-0.026c13.68-0.418,27.67-6.931,40.324-14.714
                                c0.281,0.945,0.603,1.875,0.971,2.786c9.954,24.653,44.411,8.846,65.19-5.055c0.509,3.129,1.313,6.144,2.56,8.95
                                c7.201,16.315,23.055,5.606,28.259-4.925c6.249-12.646,12.243-25.408,18.319-38.138c2.434-5.095,7.462-21.179,13.275-21.903
                                c-2.793,16.104-6.734,33.286-6.734,49.682c-0.009,7.777,0.877,23.92,12.228,23.343c6.214,0.128,14.031-2.639,16.517-8.969
                                c2.449-6.234,4.898-12.471,7.348-18.706c4.311-10.976,8.619-21.949,12.931-32.926c1.362-3.469,2.864-12.537,6.875-12.794
                                c-1.943,18.736-4.509,39.72,0.2,58.242c4.833,19.014,21.677,17.146,36.857,10.021c5.793-2.721,14.999-7.522,21.366-13.379
                                c10.635,16.672,28.758,25.744,51.546,12.806c29.322-16.649,36.909-59.117,22.606-87.786c5.046-4.717,41.155,29.707,45.66,35.479
                                c0.553-4.109,1.105-8.221,1.658-12.332C635.749,358.161,632.004,355.721,626.952,350.115z M145.747,311.201
                                c-7.898,29.355-14.423,58.818-35.202,82.429c-14.084,16.003-45.659,34.921-63.042,12.563c-14.391-22.623-4.913-53.375,4.759-75.986
                                c17.681-41.34,58.541-98.607,110.902-83.74C157.358,268.045,151.553,289.623,145.747,311.201z M236.205,335.965
                                c0.029-21.907,4.154-43.846,10.818-64.674c3.724-11.639,11.753-41.262,26.602-43.159c6.907,0.061-8.542,47.086-9.993,50.841
                                C256.037,298.625,246.523,317.612,236.205,335.965z M549.414,411.795c0.541-0.021,0.773-0.021-0.535,0.059
                                c-0.961,0.03-0.931,0.021-0.582-0.002c-30.462,0.072-18.435-70.865,7.793-71.901C581.359,339.296,572.275,409.312,549.414,411.795z
                                "/>
                        </g>
                </svg>
                <?php // echo wp_kses_post( $this->parent->args['display_name'] ); ?></h2>
            <a class="btn btn-info pull-right" href="//help.layerblend.com/alimo" target="_blank">Alimo Helpdesk</a>
            <?php if ( ! empty( $this->parent->args['display_version'] ) ) { ?>
                <span><?php echo wp_kses_post( $this->parent->args['display_version'] ); ?></span>
            <?php } ?>
        </div>
    <?php } ?>

    <div class="clear"></div>
</div>