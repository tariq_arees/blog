<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package alimo
 */

get_header();

$alimo_data = get_option('alimo_data');
$sidebar_enabled     = ( $alimo_data['enable-sidebar'] == true &&  is_active_sidebar('sidebar-1')  ) ? true : false;
$sidebar_always_open = ( $alimo_data['sidebar-always-open'] == true ) ? true : false;
$sidebar_position = ($alimo_data['sidebar-position'] == 'left') ? 'left-sidebar':'right-sidebar';

$content_class = ( $sidebar_enabled ) ? ( $sidebar_always_open ) ? "col-sm-9 {$sidebar_position}" : "col-sm-12" : "col-sm-12";
$index_layout  = $alimo_data['index-layout'];
?>
<div class="main-content">
    <div class="container">
        <div class="row">
			<?php /* Set articles wrapper class based on selected layout */
			$articles_wrapper_class = 'articles-container';
			switch ( $index_layout ) {
				case 'standard' :
					$articles_wrapper_class = 'articles-container';
                    $content_class = ( $sidebar_enabled ) ? ( $sidebar_always_open ) ? "col-sm-9 {$sidebar_position}" : "col-sm-12 col-md-9 center-block" : "col-sm-12 col-md-9 center-block";
					break;
				case 'masonry' :
					$articles_wrapper_class = 'articles-container-masonry no-padding';
					break;
				case 'grid' :
					$articles_wrapper_class = 'articles-container-grid no-padding';
					break;
				case 'interlaced-full' :
					$articles_wrapper_class = 'articles-container-interlaced full-width no-padding';
                    $content_class = ( $sidebar_enabled ) ? ( $sidebar_always_open ) ? "col-sm-9 {$sidebar_position}" : "col-sm-12 col-md-9 center-block" : "col-sm-12 col-md-9 center-block";
					break;
				case 'interlaced-half' :
					$articles_wrapper_class = 'articles-container-interlaced no-padding';
                    $content_class = ( $sidebar_enabled ) ? ( $sidebar_always_open ) ? "col-sm-9 {$sidebar_position}" : "col-sm-12 col-md-9 center-block" : "col-sm-12 col-md-9 center-block";
					break;
			}
			?>
			<?php if ( have_posts() ) : ?>
                <div class="<?php echo esc_attr($articles_wrapper_class) . ' ' . esc_attr($content_class); ?>">
                    <div id="content">
                    <?php
					/* Start the Loop */
					while ( have_posts() ) : the_post();
						/*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						if ( $index_layout == 'standard' ) {  // Standard Layout Listing
							get_template_part( 'template-parts/content', get_post_format() );
						}
						if ( $index_layout == 'masonry' ) { // Masonry Layout Listing
							get_template_part( 'template-parts/content', 'masonry' );
						}
						if ( $index_layout == 'grid' ) { // Grid Layout Listing
							get_template_part( 'template-parts/content', 'grid' );
						}
						if ( $index_layout == 'interlaced-full' ) { // Interlaced Full width Layout Listing
							get_template_part( 'template-parts/content', 'interlaced' );
						}
						if ( $index_layout == 'interlaced-half' ) { // Interlaced Half Layout Listing
							get_template_part( 'template-parts/content', 'interlaced' );
						}
					endwhile; ?>
                    </div>
                    <!-- pagination -->
                    <div class="col-sm-12">
                        <div class="posts-pagination">
							<?php
							$args = array(
								'mid_size'  => 2,
								'prev_text' => _x( 'Newer', 'previous set of posts', 'alimo' ),
								'next_text' => _x( 'Older', 'next set of posts', 'alimo' ),
							);
							the_posts_pagination( $args ); ?>
                        </div>
                    </div>
                </div>
			<?php else :
				/* No posts available*/  ?>
            <?php
				get_template_part( 'template-parts/content', 'none' );
            endif; ?>

			<?php ( $sidebar_enabled && $sidebar_always_open ) ? get_sidebar() : null; ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>
