<?php
/* Template Name: Standard Blog Listing */

// File Security Check

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


get_header();

$alimo_data = get_option('alimo_data');
$sidebar_enabled     = ( $alimo_data['enable-sidebar'] == true &&  is_active_sidebar('sidebar-1') ) ? true : false;
$sidebar_always_open = ( $alimo_data['sidebar-always-open'] == true ) ? true : false;
$sidebar_position = ($alimo_data['sidebar-position'] == 'left') ? 'left-sidebar':'right-sidebar';

$content_class = ( $sidebar_enabled ) ? ( $sidebar_always_open ) ? "col-sm-9 {$sidebar_position}" : "col-sm-12 col-md-9 center-block" : "col-sm-12 col-md-9 center-block";

$prefix   = Alimo_Meta_Boxes::get_instance()->prefix;
$category = (function_exists('rwmb_meta')) ? rwmb_meta( "{$prefix}category" ) : null;
$paged = 1;
if ( get_query_var('paged') ) {
	$paged = get_query_var('paged');
} else if ( get_query_var('page') ) {
	$paged = get_query_var('page');
} else {
	$paged = 1;
}
?>
<div class="main-content">
    <div class="container">
		<?php if ( have_posts() && $paged == 1 ): ?>
            <div class="row">
                <div class="col-sm-12 blog-listing-content">
					<?php while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
						<?php // If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) : ?>
                            <div class="row">
                                <div class="center-block col-md-8">
		                            <?php comments_template(); ?>
                                </div>
                            </div>
						<?php endif; ?>
					<?php endwhile; ?>
                </div>
            </div>
		<?php endif; ?>
        <div class="row">
            <div class="articles-container <?php echo esc_attr($content_class); ?>">
                <div id="content">
				<?php
				$query_args     = array(
					'post_type'    => 'post',
					'post_status'  => 'publish',
					'paged'        => $paged,
					'category__in' => $category,
				);
				$the_query = new WP_Query( $query_args );
				?>
				<?php if ( $the_query->have_posts() ) : ?>
					<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						<?php get_template_part( 'template-parts/content', get_post_format() ); ?>
					<?php endwhile; ?>
                </div>
                    <!-- pagination -->
                    <div class="col-sm-12">
                        <div class="posts-pagination">
							<?php
							$args_pagination = array(
								'mid_size'  => 2,
                                'prev_text' => _x( 'Newer', 'previous set of posts', 'alimo' ),
                                'next_text' => _x( 'Older', 'next set of posts', 'alimo' ),
							);
							$wp_query = $the_query;
							the_posts_pagination( $args_pagination ); ?>
                        </div>
                    </div>
	            <?php wp_reset_postdata(); ?>
				<?php else : ?>

					<?php
					/* Get the none-content template (error) */
					get_template_part( 'content', 'none' );
					?>
				<?php endif; ?>
            </div>
			<?php ( $sidebar_enabled ) ? get_sidebar() : null; ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>
