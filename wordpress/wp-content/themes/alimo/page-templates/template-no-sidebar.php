<?php
/* Template Name: Default - no sidebar */
/**
 * This template shows only the_content.
 * If the Sidebar > Always open option is Off, the sidebar will load normally, floated left or right
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package alimo
 */

get_header();

$content_class = "col-sm-12 center-block" ;
?>
<div class="main-content">
	<div class="container">
		<div class="row">
			<?php while ( have_posts() ) : the_post(); ?>
            <div class="default-page no-sidebar page-content-wrapper <?php echo esc_attr($content_class);?>">
                <h1 class="page-title"><?php the_title() ?></h1>
                <?php the_content(); ?>
	            <?php // If comments are open or we have at least one comment, load up the comment template.
	            if ( comments_open() || get_comments_number() ) : ?>
                    <div class="row">
                        <div class="center-block col-md-8">
		                    <?php comments_template(); ?>
                        </div>
                    </div>
	            <?php endif; ?>
			</div>
			<?php endwhile; // End of the loop. ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
