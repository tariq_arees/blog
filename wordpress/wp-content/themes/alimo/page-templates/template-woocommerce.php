<?php
/* Template Name: WooCommerce Page */
/**
 * The template for displaying WooCommerce widgets and use the WooCommerce sidebar
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package alimo
 */

get_header();

$alimo_data = get_option('alimo_data');
$woo_sidebar_enabled  = ( $alimo_data['woo-enable-sidebar'] == 1 && is_active_sidebar('woocommerce-sidebar') ) ? true : false;
$woo_sidebar_position = ( $alimo_data['woo-sidebar-position'] == 'left' ) ? 'left-sidebar' : 'right-sidebar';

$content_class =  ( $woo_sidebar_enabled )  ? "col-sm-9 {$woo_sidebar_position}" : "col-sm-12";
?>
<div class="main-content">
    <div class="container">
        <div class="row">
			<?php while ( have_posts() ) : the_post(); ?>
                <div class="woocommerce-page page-content-wrapper <?php echo esc_attr($content_class);?>">
                    <h1 class="page-title"><?php the_title() ?></h1>
					<?php the_content(); ?>
					<?php // If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) : ?>
                        <div class="row">
                            <div class="center-block col-md-8">
								<?php comments_template(); ?>
                            </div>
                        </div>
					<?php endif; ?>
                </div>
			<?php endwhile; // End of the loop. ?>
			<?php ($woo_sidebar_enabled) ? get_sidebar('woocommerce'): NULL; ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
