<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package alimo
 */

$alimo_data = get_option('alimo_data');
$footer_columns  = ( ! empty( $alimo_data['footer_columns'] ) ) ? $alimo_data['footer_columns'] : '4';
$footer_enabled  = ( ! empty( $alimo_data['enable_footer'] ) && $alimo_data['enable_footer'] == true ) ? true : false;
$above_enabled   = ( ! empty( $alimo_data['enable_widget_above_footer'] ) && $alimo_data['enable_widget_above_footer'] == true ) ? true : false;
$footer_bg_image = ( ! empty( $alimo_data['footer-bg-image']['url'] ) ) ? true : false;
$footer_bg_color = null;
if ( ! empty( $alimo_data['footer-bg-color']['rgba'] ) ) {
	$footer_bg_color = $alimo_data['footer-bg-color']['rgba'];
} else {
	if ( ! empty( $alimo_data['footer-bg-color']['color'] ) && ! empty( $alimo_data['footer-bg-color']['alpha'] ) ) {
		$footer_bg_color = hex2rgba($alimo_data['footer-bg-color']['color'],$alimo_data['footer-bg-color']['alpha']);
	}
}

switch ( $footer_columns ) {
	case 2:
		$col = 'col-sm-6 col-xs-12';
		break;
	case 3:
		$col = 'col-sm-4 col-xs-12';
		break;
	case 4:
	default:
		$col = 'col-sm-3 col-xs-12';
}
?>
<?php if ( $footer_enabled ) : ?>
	<?php if ( $above_enabled && function_exists('dynamic_sidebar' ) && is_active_sidebar( 'Above Footer Area' )) : ?>
        <div class="above-footer-widget text-center">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                            <?php dynamic_sidebar( 'Above Footer Area' ); ?>
                    </div>
                </div>
            </div>
        </div>
	<?php endif; ?>
	<?php $footer_layout = ( $alimo_data['footer_container'] == '1' ) ? 'container' : 'container-fluid'; ?>
    <?php if ( function_exists( 'dynamic_sidebar' ) && (is_active_sidebar( 'Footer Area 1' ) OR is_active_sidebar( 'Footer Area 2' ) OR is_active_sidebar( 'Footer Area 3' ) OR is_active_sidebar( 'Footer Area 4' ) )  ) : ?>
    <div id="main-footer"
         class="main-footer" <?php echo ( $footer_bg_image ) ? 'style="background-image: url(' . esc_url($alimo_data['footer-bg-image']['url']) . ')"' : ''; ?>>
        <div class="overlay" <?php echo ( $footer_bg_color ) ? 'style="background-color: ' . esc_attr($footer_bg_color) . '"' : ''; ?>>
            <div class="<?php echo esc_attr($footer_layout); ?> clearfix">
                <div class="row">

                    <div class="<?php echo esc_attr($col); ?>">
	                    <?php if ( function_exists( 'dynamic_sidebar' ) && is_active_sidebar( 'Footer Area 1' ) ) : ?>
		                    <?php dynamic_sidebar( 'Footer Area 1' ); ?>
	                    <?php endif; ?>
                    </div>

                    <div class="<?php echo esc_attr($col); ?>">
	                    <?php if ( function_exists( 'dynamic_sidebar' ) && is_active_sidebar( 'Footer Area 2' ) ) : ?>
		                    <?php dynamic_sidebar( 'Footer Area 2' ); ?>
	                    <?php endif; ?>
                    </div>

					<?php if ( $footer_columns == '3' || $footer_columns == '4' ) : ?>
                        <div class="<?php echo esc_attr($col); ?>">
	                        <?php if ( function_exists( 'dynamic_sidebar' ) && is_active_sidebar( 'Footer Area 3' ) ) : ?>
		                        <?php dynamic_sidebar( 'Footer Area 3' ); ?>
	                        <?php endif; ?>
                        </div>
					<?php endif; ?>

					<?php if ( $footer_columns == '4' ) : ?>
                        <div class="<?php echo esc_attr($col); ?>">
							<?php if ( function_exists( 'dynamic_sidebar' ) && is_active_sidebar( 'Footer Area 4' ) ) : ?>
                                <?php dynamic_sidebar( 'Footer Area 4' ); ?>
							<?php endif; ?>
                        </div>
					<?php endif; ?>

                </div>
            </div>
        </div>
    </div>
    <?php endif;?>
<?php endif;?>

<?php wp_footer(); ?>
</body>
</html>
