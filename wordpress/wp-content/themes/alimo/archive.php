<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package alimo
 */

get_header();

$alimo_data = get_option('alimo_data');
$sidebar_enabled     = ( $alimo_data['enable-sidebar'] == true &&  is_active_sidebar('sidebar-1')  ) ? true : false;
$sidebar_always_open = ( $alimo_data['sidebar-always-open'] == true ) ? true : false;
$sidebar_position = ($alimo_data['sidebar-position'] == 'left') ? 'left-sidebar':'right-sidebar';

$content_class =  ( $sidebar_enabled ) ? ( $sidebar_always_open ) ? "col-sm-9 {$sidebar_position}" : "col-sm-12" : "col-sm-12";
?>
<div class="container-fluid">
    <div class="row">
        <div class="archive-page <?php echo ( is_author() ) ? 'author-page' : '' ?>">
            <div class="container">
				<?php
				$title = '';
				if ( is_category() ) {
					$title        = single_cat_title( '', false );
					$current_page = esc_html__('category', 'alimo');
				} elseif ( is_tag() ) {
					$title        = single_tag_title( '', false );
					$current_page = esc_html__('tag', 'alimo');
				} elseif ( is_author() ) {
					$title        = get_the_author();
					$current_page = esc_html__('author', 'alimo');
				} elseif ( is_year() ) {
					$title        = sprintf( esc_html__( 'Year: %s', 'alimo' ), get_the_date( esc_html__( 'Y', 'alimo' ) ) );
					$current_page = esc_html__('date', 'alimo');
				} elseif ( is_month() ) {
					$title        = sprintf( esc_html__( 'Month: %s', 'alimo' ), get_the_date( esc_html__( 'F Y', 'alimo' ) ) );
					$current_page = esc_html__('date', 'alimo');
				} elseif ( is_day() ) {
					$title        = sprintf( esc_html__( 'Day: %s', 'alimo' ), get_the_date( esc_html__( 'F j, Y', 'alimo' ) ) );
					$current_page = esc_html__('date', 'alimo');
				} elseif ( is_tax( 'post_format' ) ) {
					if ( is_tax( 'post_format', 'alimo' ) ) {
						$title = esc_html__( 'Asides', 'alimo' );
					} elseif ( is_tax( 'post_format', 'alimo' ) ) {
						$title = esc_html__( 'Galleries', 'alimo' );
					} elseif ( is_tax( 'post_format', 'alimo' ) ) {
						$title = esc_html__( 'Images', 'alimo' );
					} elseif ( is_tax( 'post_format', 'alimo' ) ) {
						$title = esc_html__( 'Videos', 'alimo' );
					} elseif ( is_tax( 'post_format', 'alimo' ) ) {
						$title = esc_html__( 'Quotes', 'alimo' );
					} elseif ( is_tax( 'post_format', 'alimo' ) ) {
						$title = esc_html__( 'Links', 'alimo' );
					} elseif ( is_tax( 'post_format', 'alimo' ) ) {
						$title = esc_html__( 'Statuses', 'alimo' );
					} elseif ( is_tax( 'post_format', 'alimo' ) ) {
						$title = esc_html__( 'Audio', 'alimo' );
					} elseif ( is_tax( 'post_format', 'alimo' ) ) {
						$title = esc_html__( 'Chats', 'alimo' );
					}
					$current_page = esc_html__('archive','alimo');
				} elseif ( is_post_type_archive() ) {
					$title        = sprintf( esc_html__( 'Archives: %s', 'alimo' ), post_type_archive_title( '', false ) );
					$current_page = esc_html__('archive','alimo');
				} elseif ( is_tax() ) {
					$tax = get_taxonomy( get_queried_object()->taxonomy );
					/* translators: 1: Taxonomy singular name, 2: Current taxonomy term */
					$title        = sprintf( '%1$s: %2$s', $tax->labels->singular_name, single_term_title( '', false ) );
					$current_page = esc_html__('archive','alimo');
				} else {
					$title        = esc_html__( 'Archives', 'alimo' );
					$current_page = esc_html__('archive','alimo');
				}
				?>
                <span><?php echo esc_html__('Browsing ','alimo') . esc_html($current_page); ?></span>
                <h3><?php echo esc_html($title); ?></h3>
            </div>
        </div>
		<?php if ( is_author() ) { ?>
            <div class="author-profile">
                <img class="author-photo" src="<?php echo get_avatar_url( get_the_author_meta( 'ID' ), 190 ); ?>"
                     alt="">
				<?php
				/* check if at least one profile is available */
				$facebook_escaped    = esc_url( get_the_author_meta( 'facebook' ) );
				$twitter_escaped     = esc_url( get_the_author_meta( 'twitter' ) );
				$linkedin_escaped    = esc_url( get_the_author_meta( 'linkedin' ) );
				$google_plus_escaped = esc_url( get_the_author_meta( 'google-plus' ) );
				if ( ! empty( $facebook_escaped ) || ! empty( $twitter_escaped ) || ! empty( $google_plus_escaped ) || ! empty( $linkedin_escaped ) ):
					?>
                    <div class="contact-methods">
                        <span><?php esc_html_e( 'Find me on:', 'alimo' ); ?></span>
                        <ul class="social-icons">
							<?php
							echo ( $facebook_escaped ) ? "<li><a target='_blank' href='{$facebook_escaped}'><i class='fa fa-facebook'></i></a></li>" : '';
							echo ( $twitter_escaped ) ? "<li><a target='_blank' href='{$twitter_escaped}'><i class='fa fa-twitter'></i></a></li>" : '';
							echo ( $linkedin_escaped ) ? "<li><a target='_blank' href='{$linkedin_escaped}'><i class='fa fa-linkedin'></i></a></li>" : '';
							echo ( $google_plus_escaped ) ? "<li><a target='_blank' href='{$google_plus_escaped}'><i class='fa fa-google-plus'></i></a></li>" : '';
							?>
                        </ul>
                    </div>
				<?php endif; ?>
            </div>
		<?php } ?>
    </div>
</div>

<?php
$layout = 'standard';
if ( $current_page == 'author' ) {
	$layout = $alimo_data['author-layout'];
}
if ( $current_page == 'archive' ) {
	$layout = $alimo_data['archive-layout'];
}
if ( $current_page == 'category' ) {
	$layout = $alimo_data['category-layout'];
}
if ( $current_page == 'tag' ) {
	$layout = $alimo_data['tag-layout'];
}
if ( $current_page == 'date' ) {
	$layout = $alimo_data['date-layout'];
}
?>
<div class="main-content">
    <div class="container">
        <div class="row">
			<?php
			/* Set articles wrapper class based on selected layout */
			$articles_wrapper_class = 'articles-container';
			switch ( $layout ) {
				case 'standard' :
					$articles_wrapper_class = 'articles-container';
					$content_class =  ( $sidebar_enabled ) ? ( $sidebar_always_open ) ? "col-sm-9 {$sidebar_position}" : "col-sm-12 col-md-9 center-block" : "col-sm-12 col-md-9 center-block";
					break;
				case 'masonry' :
					$articles_wrapper_class = 'articles-container-masonry no-padding';
					break;
				case 'grid' :
					$articles_wrapper_class = 'articles-container-grid no-padding';
					break;
				case 'interlaced-full' :
					$articles_wrapper_class = 'articles-container-interlaced full-width no-padding';
					$content_class =  ( $sidebar_enabled ) ? ( $sidebar_always_open ) ? "col-sm-9 {$sidebar_position}" : "col-sm-12 col-md-9 center-block" : "col-sm-12 col-md-9 center-block";
					break;
				case 'interlaced-half' :
					$articles_wrapper_class = 'articles-container-interlaced no-padding';
					$content_class =  ( $sidebar_enabled ) ? ( $sidebar_always_open ) ? "col-sm-9 {$sidebar_position}" : "col-sm-12 col-md-9 center-block" : "col-sm-12 col-md-9 center-block";
					break;
			}
			?>
            <div class="<?php echo esc_attr($articles_wrapper_class) .' '. esc_attr($content_class); ?>">
                <div id="content">
                <?php if ( have_posts() ) :
                    /* Start the Loop*/
                    while ( have_posts() ) : the_post();
                        /*
                         * Include the Post-Format-specific template for the content.
                         * If you want to override this in a child theme, then include a file
                         * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                         */
                        if ( $layout == 'standard' ) {  // Standard Layout Listing
                            get_template_part( 'template-parts/content', get_post_format() );
                        }
                        if ( $layout == 'masonry' ) { // Masonry Layout Listing
                            get_template_part( 'template-parts/content', 'masonry' );
                        }
                        if ( $layout == 'grid' ) { // Grid Layout Listing
                            get_template_part( 'template-parts/content', 'grid' );
                        }
                        if ( $layout == 'interlaced-full' ) { // Interlaced Full width Layout Listing
                            get_template_part( 'template-parts/content', 'interlaced' );
                        }
                        if ( $layout == 'interlaced-half' ) { // Interlaced Half Layout Listing
                            get_template_part( 'template-parts/content', 'interlaced' );
                        }
                    endwhile;?>
                </div>
                <?php else :

                    get_template_part( 'template-parts/content', 'none' );

                endif; ?>
            </div>
			<?php ( $sidebar_enabled && $sidebar_always_open ) ? get_sidebar() : null; ?>
        </div>
        <div class="row">
            <div class="col-sm-12"> <!-- pagination -->
                <div class="posts-pagination">
					<?php
					$args = array(
						'mid_size'  => 2,
						'prev_text' => _x( 'Newer', 'previous set of posts', 'alimo' ),
						'next_text' => _x( 'Older', 'next set of posts', 'alimo' ),
					);
					the_posts_pagination( $args ); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
