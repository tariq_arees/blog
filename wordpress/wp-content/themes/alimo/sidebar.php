<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package alimo
 */

global $woocommerce;
$alimo_data = get_option('alimo_data');
$sidebar_enabled  = ( $alimo_data['enable-sidebar'] == 1 &&  is_active_sidebar('sidebar-1') ) ? true : false;
$sidebar_position = ( $alimo_data['sidebar-position'] == 'left' ) ? 'left-sidebar' : 'right-sidebar';
$sidebar_classes  = ( $alimo_data['sidebar-always-open'] == 1 ) ? "fixed-sidebar col-sm-3 {$sidebar_position}" : "sticky-sidebar {$sidebar_position}";
?>
<?php if ( $sidebar_enabled ) : ?>
	<?php // check if woocommerce is installed and enabled
	if ( $woocommerce ) {
	    // if we're on a woocommerce template don't load the default sidebar.
		if ( is_woocommerce() ) {
			return;
		}
	}
	?>
    <aside class="main-sidebar <?php echo esc_attr($sidebar_classes); ?>">
        <div class="sidebar-open-trigger">
            <span></span>
            <span></span>
            <span></span>
        </div>
        <div class="clearfix"></div>
        <div class="sidebar-content">
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
        </div>
    </aside>
<?php endif; ?>
